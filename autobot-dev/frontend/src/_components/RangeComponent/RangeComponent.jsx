import React from 'react';
import  './RangeComponent.css'
class RangeComponent extends React.Component {
	constructor(props) {
    super(props)
    this.state = {
      value : this.props.value,
      title : this.props.title,
      minValue : this.props.minValue,
      maxValue : this.props.maxValue
    }
    this.changeValue = this.changeValue.bind(this)
  }

  changeValue(e) {
    this.setState({
      value: e.currentTarget.value
    },()=>{
      this.props.onValueChange(this.state.value)
    });
  }
    
  render() {
    return (
      <div>
        <div><b>{this.state.title}  :  {this.state.value}</b></div> 
        <input type="range" className="range-selector" min={this.state.minValue} max={this.state.maxValue} onInput={this.changeValue} value={this.state.value} />
        <div className="range-left-label"> {this.state.minValue} </div> 
        <div className="range-right-label"> {this.state.maxValue} </div>
        <br/>
      </div>
    );
  }
  }
  export default RangeComponent
