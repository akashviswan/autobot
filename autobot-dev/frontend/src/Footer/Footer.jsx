import React from 'react';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';
import { userActions } from '../_actions';

class Footer extends React.Component {
    componentDidMount() {
        //this.props.dispatch(userActions.getAll());
    }
    render() {
        return(  
				<footer className="main-footer">
					<div className="pull-right hidden-xs">
						<b>Version</b> 1.0
					</div>
					<strong>Copyright &copy; 2017 <a href="">Autobot</a>.</strong> All rights reserved.
				</footer>
        );
    }
}

export default Footer;