export function authHeader() {
    // return authorization header with jwt token
    let token = localStorage.getItem('token');

    if (token) {
        return {
            "Accept": "application/json",
            "Content-Type": "application/json",
            'Authorization': 'Token ' + token
        };
    } else {
        return {
            "Accept": "application/json",
            "Content-Type": "application/json"
        };
    }
}

export function handleResponse(response) {
    if (response) {
        if (response.ok) {
            return response.json();
        }
        else if (response.status === 401) {
            // Redirect to login page
            let token = localStorage.getItem('token');
            if (token) {
                logout();
                location.reload(true);
            }
        }
        else {
            throw new Error(response.statusText);
        }
    }
}

function logout() {
    // remove user from local storage to log user out
    localStorage.removeItem('token');
}
