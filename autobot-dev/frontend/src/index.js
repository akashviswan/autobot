import React from 'react';
import { render } from 'react-dom';
import { Provider } from 'react-redux';
import { store } from './_helpers';
import { App } from './App';
import "./css/bootstrap/css/bootstrap.min.css"
import "./css/source/css/AdminLTE.min.css"
import "./css/source/css/skin-black.css"
import $  from "./css/source/js/jquery-2.2.3.min.js"
import "./css/bootstrap/js/bootstrap.min.js" 
import "./css/source/js/app.min.js"
import "./css/source/js/main.js"
render(
    <Provider store={store}>
        <App />
    </Provider>,
    document.getElementById('app')
);