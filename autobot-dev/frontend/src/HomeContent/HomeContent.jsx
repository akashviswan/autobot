import React, {PropTypes} from 'react';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';
import {bindActionCreators} from 'redux';
import * as homeActions from '../_actions/home.actions';
import {Pie} from 'react-chartjs-2';

class HomeContent extends React.Component {

	constructor(props) {
		super(props);
		this.state = {
			dashboard : this.props.dashboard
		}
	}
	componentWillReceiveProps(newProps) {
		this.setState({
			dashboard : newProps.dashboard
		})
	}
	

    render() {
		const randomColors =["#FFC300","#00BCD4","#00B0FF"]
		const options = {
			maintainAspectRatio: false,
			responsive: false,
			legend: {
				display: false
			},
			elements: {
				arc: {
					borderWidth: 0
				}
			}
		  }
		
		const dashboard = this.state.dashboard
		let heathyHostCount = 0
		var healthhostPieChartData = []
		var healthhostPieChartSliceColors = []
		var healthhostPieChartTitles = []
		if (dashboard && dashboard.instance_status_count) {
			for (var index = 0; index < dashboard.instance_status_count.length; index++) { 
				let item = dashboard.instance_status_count[index]
				healthhostPieChartData.push(item.id__count)
				healthhostPieChartTitles.push(item.status)
				if (item.status === 'inservice'){
					heathyHostCount = item.id__count
					healthhostPieChartSliceColors.push('#4CAF50')
				}
				else if (item.status === 'outofservice'){
					healthhostPieChartSliceColors.push('#FF5733')
				}
				else {
					healthhostPieChartSliceColors.push(randomColors[index%3])
				}
			} 
		}
		
		
		const data = {
			labels: healthhostPieChartTitles,
			datasets: [{
				data: healthhostPieChartData,
				backgroundColor: healthhostPieChartSliceColors,
				hoverBackgroundColor: healthhostPieChartSliceColors
			}]
		};

        return (        		
				<div className="content-wrapper">
							<section className="content-header">
								<h1>
									Dashboard
									<small>Control panel</small>
								</h1>
								<ol className="breadcrumb">
									<li><a href="#"><i className="fa fa-dashboard"></i> Home</a></li>
									<li className="active">Dashboard</li>
								</ol>
							</section>
							<section className="content">
								<div className="row">    				
									<div className="col-lg-3 col-xs-6">
										<div className="small-box bg-blue-black">
											<div className="inner">													
												<div align="left" className="col-xs-6">
													<p>Applications</p>
													<h3>{this.props.dashboard.application_count}<sup style={{fontSize: '20px'}}></sup></h3>							
												</div>						
												<div align="right" className="sparkline" data-type="pie" data-offset="90" data-width="100px" data-height="84px"></div>				
											</div>			
											<a href="/applications?search_param=all&x=" className="small-box-footer">More info <i className="fa fa-arrow-circle-right"></i></a>
										</div>
									</div>
									<div className="col-lg-3 col-xs-6">
										<div className="small-box bg-blue-black">
											<div className="inner">												
												<div align="left" className="col-xs-6">
													<p>Metrics</p>
													<h3>{this.props.dashboard.metrics_count}<sup style={{fontSize: '20px'}}></sup></h3>							
												</div>						
												<div align="right" className="sparkline" data-type="pie" data-offset="90" data-width="100px" data-height="84px"></div>
											</div>			
										<a href="/metrics" className="small-box-footer">More info <i className="fa fa-arrow-circle-right"></i></a>
										</div>
									</div>
									<div className="col-lg-3 col-xs-6" >
										<div className="small-box bg-blue-black">
											<div className="inner">						
												<div align="left" className="col-xs-6">
													<p>Instance Managed</p>
													<h3>{this.props.dashboard.instance_count}<sup style={{fontSize: '20px'}}></sup></h3>							
												</div>						
												<div align="right" className="sparkline" data-type="pie" data-offset="90" data-width="100px" data-height="84px"></div>	
											</div>			
											<a href="#" className="small-box-footer">More info <i className="fa fa-arrow-circle-right"></i></a>
										</div>
									</div>				
									<div className="col-lg-3 col-xs-6">
										<div className="small-box bg-blue-black">
											<div className="inner" style={{height: '104px'}}>							
												<div align="left" className="col-xs-6">
													<p>Healthy Instance</p>
													<h3>{heathyHostCount}<sup style={{fontSize: '20px'}}></sup></h3>							
												</div>	
												<div align="right" className="col-xs-6">
													<Pie data={data} height={84} width={100} options={options}/>
												</div>
											</div>
											<a href="#" className="small-box-footer">More info <i className="fa fa-arrow-circle-right"></i></a>
										</div>	
									</div>	 			
								</div>				
								<div className="row" >			
								   <div className="col-md-3">
										<div className="form-group">							
											<div  className="col-xs-2" >Start</div>
												<div className="input-group date" id="startDate">
												<input type="text" className="form-control" name="start" id="start"/>
												<span className="input-group-addon">
													<span className="glyphicon glyphicon-calendar"></span>
												</span>
											</div>					
										</div>
									</div>
									<div className="col-md-3">
										<div className="form-group">							
											<div  className="col-xs-2" >End</div>
												<div className="input-group date" id="endDate">
												<input type="text" className="form-control" name="end" id="end"/>
												<span className="input-group-addon">
													<span className="glyphicon glyphicon-calendar"></span>
												</span>
											</div>					
										</div>						
									</div>
									<div className="col-md-2 ">								
										<button id="btnGo" type="Submit" className="btn ">Go!</button>
									</div>											
								</div>
								<iframe id="iFrame" style={{borderWidth: '0'}} src="http://35.153.231.229:5601/app/kibana#/dashboard/c9d863c0-ebe5-11e8-a727-d769b362df88?embed=true&_g=(refreshInterval%3A(pause%3A!t%2Cvalue%3A0)%2Ctime%3A(from%3A'2018-10-05T18%3A13%3A33.611Z'%2Cmode%3Aabsolute%2Cto%3A'2018-10-20T08%3A58%3A10.741Z'))" height="480" width='100%' ></iframe>
							</section>								
						</div>
        );
    }
}

function mapStateToProps(state, ownProps) {
	console.log(state)
	return {
		dashboard: state.dashboard
	};
}

function mapDispatchToProps(dispatch) {
	return {
		actions: bindActionCreators(homeActions, dispatch)
	};
}
  
export default connect(mapStateToProps,mapDispatchToProps)(HomeContent);
