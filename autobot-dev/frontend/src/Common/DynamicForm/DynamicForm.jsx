import React from 'react';
import { FormItems } from '../../../static/constants/TemplateConstants';
import _ from 'lodash';
import Select from 'react-select';
import Toggle from "react-toggle-component"
import PropTypes from 'prop-types';

class DynamicForm extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            datasource: this.props.datasource,
            template: this.props.template,
            extradatasource: this.props.extradatasource
        }
    }

    componentWillReceiveProps(newProps) {
        this.setState({
            datasource: newProps.datasource,
            template: newProps.template,
            extradatasource: newProps.extradatasource
        })
    }

    render() {
        let datasource = this.state.datasource
        let template = this.state.template
        let extradatasource = this.state.extradatasource
        console.log(datasource)
        console.log(template)
        console.log(extradatasource)

        var formFields = []

        if (datasource.object_type) {

            let self = this;
            _.forEach(datasource, function (value, key) {
                if (template[key] === undefined || template[key] === null) {
                    return
                }
                if (template[key].type === FormItems.TEXTFIELD) {
                    var patterns = (template[key].pattern != null && template[key].pattern != undefined) ? template[key].pattern : "*"
                    formFields.push(
                        <div>
                            <div>{template[key].label}:</div>
                            <input key={key} type="text" pattern={patterns} value={value == null ? "" : value} onChange={(e) => self.props.handleTextFieldDataChange(key, e)} className="input name-input" />
                        </div>
                    );
                }
                else if (template[key].type === FormItems.CHECKBOX) {
                    formFields.push(
                        <div>
                            <br />
                            <div>{template[key].label}:&nbsp;&nbsp;
                            <Toggle checked={value}
                                    onToggle={(e) => self.props.handleCheckBoxDataChange(key, e)} />
                            </div>
                            <br />
                        </div>
                    );
                }
                else if (template[key].type === FormItems.DROPDOWN) {
                    var selectedOption = template[key].defaultOptions
                    if (selectedOption) {
                        var filteredArray = template[key].defaultOptions.filter(function (itm) {
                            return itm.value == value;
                        });
                        if (filteredArray.length > 0) {
                            formFields.push(
                                <div>
                                    <div>{template[key].label}:</div>
                                    <Select
                                        className="react-select-container"
                                        classNamePrefix="react-select"
                                        value={filteredArray[0]}
                                        options={template[key].defaultOptions}
                                        onChange={e => self.props.handleDropDownDataChange(key, template[key].dropdown_value_key != undefined ? e[template[key].dropdown_value_key] : e)}
                                    />
                                </div>
                            );
                        }
                    }
                    else {
                        formFields.push(
                            <div>
                                <div>{template[key].label}:</div>
                                <Select
                                    className="react-select-container"
                                    classNamePrefix="react-select"
                                    value={value}
                                    options={extradatasource[template[key]["dropdown_datasource_key"]]}
                                    getOptionLabel={(option) => option[template[key]["dropdown_label_key"]]}
                                    getOptionValue={(option) => template[key].dropdown_value_key != undefined ? option[template[key].dropdown_value_key] : option}
                                    onChange={e => self.props.handleDropDownDataChange(key, e)}
                                />
                            </div>
                        );
                    }
                }
                else if (template[key].type === FormItems.DROPDOWNMULTI) {
                    //To be implemented
                }
            });
        }
        return (<div>{formFields}</div>)
    }
}

DynamicForm.propTypes = {
    template: PropTypes.object.isRequired,
    datasource: PropTypes.object.isRequired,
    extradatasource: PropTypes.object,
    handleTextFieldDataChange: PropTypes.func.isRequired,
    handleDropDownDataChange: PropTypes.func.isRequired,
    handleCheckBoxDataChange: PropTypes.func.isRequired
};

export default DynamicForm;
