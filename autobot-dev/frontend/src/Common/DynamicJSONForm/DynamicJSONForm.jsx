import React from 'react';
import { FormItems } from '../../../static/constants/TemplateConstants';
import _ from 'lodash';
import Select from 'react-select';
import Toggle from "react-toggle-component"
import PropTypes from 'prop-types';
const modalRoot = document.getElementById('modal-root');

class DynamicJSONForm extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            datasource: this.props.datasource,
            template: this.props.template,
            extradatasource: this.props.extradatasource,
            validation: {}
        }
        this.handleTextFieldDataChange = this.handleTextFieldDataChange.bind(this);
        this.handleSelectionChange = this.handleSelectionChange.bind(this);
        this.handleCheckBoxChange = this.handleCheckBoxChange.bind(this);
        this.validate = this.validate.bind(this);
    }

    componentWillReceiveProps(newProps) {
        this.setState({
            datasource: newProps.datasource,
            template: newProps.template,
            extradatasource: newProps.extradatasource
        })
    }

    handleTextFieldDataChange(key, e) {
        e.persist();
        const targetValue = (e.target.validity.valid) ? e.target.value : this.state.datasource[key];
        this.setState(({ datasource }) => ({
            datasource: {
                ...datasource,
                [key]: targetValue,
            }
        })
            , () => {
                this.props.onstateChanged(this.state.datasource)
                if (this.state.validated) {
                    this.validate()
                }
            });
    }

    handleSelectionChange(key, updatedSelectedValue, valueKey) {
        if (Array.isArray(this.state.datasource)) {
            this.setState({ datasource: updatedSelectedValue }, () => {
                this.props.onstateChanged(this.state.datasource)
                if (this.state.validated) {
                    this.validate()
                }
            });
        }
        else {
            this.setState(({ datasource }) => ({
                datasource: {
                    ...datasource,
                    [key]: (valueKey == '') ? updatedSelectedValue : updatedSelectedValue[valueKey],
                }
            }), () => {
                this.props.onstateChanged(this.state.datasource)
                if (this.state.validated) {
                    this.validate()
                }
            });
        }

    }

    handleCheckBoxChange(key, e) {
        this.setState(({ datasource }) => ({
            datasource: {
                ...datasource,
                [key]: !this.state.datasource[key],
            }
        }), () => {
            if (typeof (this.props.onstateChanged) === 'function') {
                this.props.onstateChanged(this.state.datasource)
                if (this.state.validated) {
                    this.validate()
                }
            }
        });
    }

    resetValidations() {
        this.setState({
            validated: false,
            validation: {}
        })
    }
    validate() {
        this.setState({
            validated: true
        })
        let self = this
        let template = this.state.template
        let isValid = true
        template.map(function (object) {
            if (object.isrequired === true) {
                //TO-DO : Revisit logic
                if (object.type == FormItems.CHECKBOX) {
                    //By defualt its validated to true, assuming in either case checkbox validation is true
                    self.state.validation[object.key] = true
                }
                else if (Array.isArray(self.state.datasource) && self.state.datasource.length == 0) {
                    self.state.validation[object.key] = false
                    isValid = false
                }
                else if (!Array.isArray(self.state.datasource) && (self.state.datasource[object.key] == null
                    || self.state.datasource[object.key] == undefined || self.state.datasource[object.key] == "")) {
                    self.state.validation[object.key] = false
                    isValid = false
                }
                else {
                    self.state.validation[object.key] = true
                }
            }
        })
        return isValid
    }

    render() {
        let datasource = this.state.datasource
        let template = this.state.template
        let extradatasource = this.state.extradatasource
        var formFields = []
        let self = this;
        template.map(function (object) {
            let error = (self.state.validation[object.key] === false) ? "error" : ""
            if (object.type === FormItems.TEXTFIELD) {
                let value = ""
                if (datasource[object.key] != undefined && datasource[object.key] != null) {
                    value = datasource[object.key]
                }
                var patterns = (object.pattern != null && object.pattern != undefined) ? object.pattern : "*"
                formFields.push(
                    <div>
                        <div>{object.label}:</div>
                        <input key={object.key} type="text" validate={error} pattern={patterns} value={value == null ? "" : value} onChange={(e) => self.handleTextFieldDataChange(object.key, e)} className="input form-input" />
                    </div>
                );
            }
            else if (object.type === FormItems.CHECKBOX) {
                let value = false
                if (datasource[object.key] != undefined && datasource[object.key] != null) {
                    value = datasource[object.key]
                }
                formFields.push(
                    <div>
                        <br />
                        <div>{object.label}:&nbsp;&nbsp;
                        <Toggle checked={value}
                                onToggle={(e) => self.handleCheckBoxChange(object.key, e)} />
                        </div>
                        <br />
                    </div>
                );
            }
            else if (object.type === FormItems.DROPDOWN) {
                let value = ""
                if (datasource[object.key] != undefined && datasource[object.key] != null) {
                    value = datasource[object.key]
                }
                let selectedOption = object.defaultOptions
                if (selectedOption) {
                    var filteredArray = object.defaultOptions.filter(function (itm) {
                        return itm.value == value;
                    });
                    // if (filteredArray.length > 0) {
                    formFields.push(
                        <div>
                            <div>{object.label}:</div>
                            <Select
                                className="react-select-container"
                                classNamePrefix="react-select"
                                styles={{
                                    control: (base, state) => ({
                                        ...base,
                                        border: (self.state.validation[object.key] === false) ? '1px solid #FFBABA' : "1px solid lightgray", // default border color
                                    }),
                                }}
                                menuPortalTarget={(modalRoot != null && modalRoot != undefined) ? modalRoot : null}
                                value={filteredArray[0]}
                                options={object.defaultOptions}
                                getOptionLabel={(option) => object.dropdown_label_key ? option[object.dropdown_label_key] : option}
                                getOptionValue={(option) => object.dropdown_value_key ? option[object.dropdown_value_key] : option}
                                onChange={e => self.handleSelectionChange(object.key, e, (object.dropdown_value_key ? object.dropdown_value_key : ''))}
                            />
                        </div>
                    );
                    // }
                }
                else {
                    formFields.push(
                        <div>
                            <div>{object.label}:</div>
                            <Select
                                className="react-select-container"
                                classNamePrefix="react-select"
                                styles={{
                                    control: (base, state) => ({
                                        ...base,
                                        border: (self.state.validation[object.key] === false) ? '1px solid #FFBABA' : "1px solid lightgray", // default border color
                                    }),
                                }}
                                menuPortalTarget={(modalRoot != null && modalRoot != undefined) ? modalRoot : null}
                                value={value}
                                options={extradatasource[object.dropdown_datasource_key]}
                                getOptionLabel={(option) => object.dropdown_label_key ? option[object.dropdown_label_key] : option}
                                getOptionValue={(option) => object.dropdown_value_key ? option[object.dropdown_value_key] : option}
                                onChange={e => self.handleSelectionChange(object.key, e, (object.dropdown_value_key ? object.dropdown_value_key : ''))}
                            />
                        </div>
                    );
                }
            }
            else if (object.type === FormItems.DROPDOWNMULTI) {
                let value = ""
                formFields.push(
                    <div>
                        <div>{object.label}:</div>
                        <Select
                            className="react-select-container"
                            classNamePrefix="react-select"
                            styles={{
                                control: (base, state) => ({
                                    ...base,
                                    border: (self.state.validation[object.key] === false) ? '1px solid #FFBABA' : "1px solid lightgray", // default border color
                                }),
                            }}
                            value={datasource}
                            options={extradatasource[object.dropdown_datasource_key]}
                            getOptionLabel={(option) => object.dropdown_label_key ? option[object.dropdown_label_key] : option}
                            getOptionValue={(option) => object.dropdown_value_key ? option[object.dropdown_value_key] : option}
                            onChange={e => self.handleSelectionChange(object.key, e, (object.dropdown_value_key ? object.dropdown_value_key : ''))}
                            isMulti={true}
                        />
                    </div>
                )
            }
        })
        return (<div>{formFields}</div>)
    }
}

DynamicJSONForm.propTypes = {
    template: PropTypes.array.isRequired,
    datasource: PropTypes.object.isRequired,
    extradatasource: PropTypes.object,
    onstateChanged: PropTypes.func.isRequired
};

export default DynamicJSONForm;
