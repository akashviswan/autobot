import React from 'react';
import { connect } from 'react-redux';
import {FormItems} from '../../static/constants/TemplateConstants';


class FormCreator extends React.Component {
    componentDidMount() { 
    }

    render() {
        var formItems = []
        switch(this.props.formType) {
            case FormItems.TEXTFIELD : 
                formItems.push("TEXTFIELD")
            case FormItems.CHECKBOX : 
                formItems.push("CHECKBOX")
            case FormItems.DROPDOWN : 
                formItems.push("DROPDOWN")
            case FormItems.DROPDOWNMULTI : 
                formItems.push("DROPDOWNMULTI")
        }
        return ({formItems});
    }
}

export default FormCreator