import React from 'react';
import { connect } from 'react-redux';
import * as d3 from "d3";
import d3Spline from "../../static/css/source/css/d3Spline.css"

class DrawSplinePage extends React.Component {   
	constructor(props){
        super(props);
        //this.state = {points: this.props.points};
		this.state = {type: this.props.type};
    }
	componentDidMount() {
		this.drawSpline();
	}
	
	drawSpline()
	{
		var curveTypes = [	
			{name: 'curveNatural', curve: d3.curveNatural, active: true, lineString: '', clear: true, info: 'Interpolates the points with a cubic spline with zero 2nd derivatives at the endpoints.'}
		];
		var type = this.state.type;	
		//var xScale, yScale;
		console.log("type--"+type);
		var width = 700, height = 500;		
		 /* var width = window.innerWidth,
          height = window.innerHeight,
          margin = { top: 40, right: 20, bottom: 20, left: 40 };
          console.log("width----"+width);
		  console.log("height----"+height); */
		//var width = d3.select("#svg").style("width");console.log("width----"+width);
        //var	height = d3.select("#svg").style("height");console.log("height----"+height);
		var radius = 4;
		var lineGenerator = d3.line();
		var categoryScale = d3.scaleOrdinal(d3.schemeCategory10);
		function colorScale(d) {return d === 0 ? '#777' : categoryScale(d);}
		//var points = this.state.points;	
		//var points = [ [50, 330], [75, 200], [280, 75], [300, 75], [475, 300], [600, 200] ];
		var points = [];	
		var wPoints = [];	
		var mPoints = [];	
		var newPoints = [[3,17],[4,9],[7,5],[10,11],[12,16],[16,13],[16,8],[18,5],[21,8],[21,14],[24,10]]; 
		var numActivePoints = points.length;
		
		var drag = d3.drag()
		.on('drag', function(d, i) {
			points[i][0] = d3.event.x;
			points[i][1] = d3.event.y;
			update();
		});	
		var wdrag = d3.drag()
		.on('drag', function(d, i) {
			wPoints[i][0] = d3.event.x;
			wPoints[i][1] = d3.event.y;
			update();
		});			
		var mdrag = d3.drag()
		.on('drag', function(d, i) {
			mPoints[i][0] = d3.event.x;
			mPoints[i][1] = d3.event.y;
			update();
		});			

		
		// We're passing in a function in d3.max to tell it what we're maxing (x value)
		var xScaleDaily = d3.scaleLinear()
		.domain([0, 24])
		.range([0, 300]);  // Set margins for x specific
		// We're passing in a function in d3.max to tell it what we're maxing (y value)
		var yScaleDaily = d3.scaleLinear()
		.domain([0, 20])
		.range([300, 0]);  // Set margins for y specific	
		
		// We're passing in a function in d3.max to tell it what we're maxing (x value)
		var xScaleWeekly = d3.scaleLinear()
		.domain([0, 7])
		.range([0, 300]);  // Set margins for x specific
		// We're passing in a function in d3.max to tell it what we're maxing (y value)
		var yScaleWeekly = d3.scaleLinear()
		.domain([0, 20])
		.range([300, 0]);  // Set margins for y specific	
	
		// We're passing in a function in d3.max to tell it what we're maxing (x value)
		var xScaleMonthly = d3.scaleLinear()
		.domain([0, 12])
		.range([0, 300]);  // Set margins for x specific
		// We're passing in a function in d3.max to tell it what we're maxing (y value)
		var yScaleMonthly = d3.scaleLinear()
		.domain([0, 20])
		.range([300, 0]);  // Set margins for y specific	
		
		/* // We're passing in a function in d3.max to tell it what we're maxing (x value)
		var xScale = d3.scaleLinear()
		.domain([0, d3.max(points, function(d) { return d[0];})])
		.range([0, width - 100]);  // Set margins for x specific
		// We're passing in a function in d3.max to tell it what we're maxing (y value)
		var yScale = d3.scaleLinear()
		.domain([0, d3.max(points, function(d) { return d[1];})])
		.range([height/2, 0]);  // Set margins for y specific
		
		xScale.domain(d3.extent(newPoints, function(d) { return d[0]; }));
		yScale.domain([0, 20]); */
		
		//////////////////////////////////////////////////////////////////////////////////////////////////////
		
		//set axis label for y axis
		d3.select("#svg").append("text")
            .attr("text-anchor", "middle")  // this makes it easy to centre the text as the transform is applied to the anchor
            .attr("transform", "translate(18,150)rotate(-90)")  // text is drawn off the screen top left, move down and out and rotate
            .text("Data");		
		//set axis label for x axis
		d3.select("#svg").append("text")
            .attr("text-anchor", "middle")  // this makes it easy to centre the text as the transform is applied to the anchor
            .attr("transform", "translate(200,340)")  // centre below axis
            .text("Daily data");
			
		//set axis label for y axis
		d3.select("#week").append("text")
            .attr("text-anchor", "middle")  // this makes it easy to centre the text as the transform is applied to the anchor
            .attr("transform", "translate(18,150)rotate(-90)")  // text is drawn off the screen top left, move down and out and rotate
            .text("Data");		
		//set axis label for x axis
		d3.select("#week").append("text")
            .attr("text-anchor", "middle")  // this makes it easy to centre the text as the transform is applied to the anchor
            .attr("transform", "translate(200,340)")  // centre below axis
            .text("Weekly data");
		
		//set axis label for y axis
		d3.select("#month").append("text")
            .attr("text-anchor", "middle")  // this makes it easy to centre the text as the transform is applied to the anchor
            .attr("transform", "translate(18,150)rotate(-90)")  // text is drawn off the screen top left, move down and out and rotate
            .text("Data");		
		//set axis label for x axis
		d3.select("#month").append("text")
            .attr("text-anchor", "middle")  // this makes it easy to centre the text as the transform is applied to the anchor
            .attr("transform", "translate(200,340)")  // centre below axis
            .text("Monthly data");
						
        //////////////////////////////////////////////////////////////////////////////////////////////////////
		
		var x_axis_daily = d3.axisBottom()
				.scale(xScaleDaily);	  

		var y_axis_daily = d3.axisLeft()
				.scale(yScaleDaily);
				
		var x_axis_weekly = d3.axisBottom()
				.scale(xScaleWeekly);	  

		var y_axis_weekly = d3.axisLeft()
				.scale(yScaleWeekly);
				
		var x_axis_monthly = d3.axisBottom()
				.scale(xScaleMonthly);	  

		var y_axis_monthly = d3.axisLeft()
				.scale(yScaleMonthly);
				
		d3.select("#svg").append("g")
		   .attr("transform", "translate(40, 5)")
		   .call(y_axis_daily);

		var xAxisTranslate = height + 10;
		d3.select("#svg").append("g")
				.attr("transform", "translate(40, 305)")
				.call(x_axis_daily);
				
		d3.select("#week").append("g")
		   .attr("transform", "translate(40, 5)")
		   .call(y_axis_weekly);

		d3.select("#week").append("g")
				.attr("transform", "translate(40, 305)")
				.call(x_axis_weekly);
		
		d3.select("#month").append("g")
		   .attr("transform", "translate(40, 5)")
		   .call(y_axis_monthly);

		d3.select("#month").append("g")
				.attr("transform", "translate(40, 305)")
				.call(x_axis_monthly); 

		//xScale.clamp(true);
				
		var circleAttrs = {
		cx: function(d) { return d[0];},
		cy: function(d) { return d[1];},
		r: radius
		};
 	
		d3.select("#svg").selectAll("circle")
			.data(points)
			.enter()
			.append("circle")
			//.attr(circleAttrs)  // Get attributes from circleAttrs var
			.attr('cx', function(d) { return d[0];})
			.attr('cy', function(d) { return d[1];})
			.on("mouseover", handleMouseOver)
			.on("mouseout", handleMouseOut);
			
		d3.select("#week").selectAll("circle")
			.data(wPoints)
			.enter()
			.append("circle")
			//.attr(circleAttrs)  // Get attributes from circleAttrs var
			.attr('cx', function(d) { return d[0];})
			.attr('cy', function(d) { return d[1];})
			.on("mouseover", handleMouseOver)
			.on("mouseout", handleMouseOut);
			
		d3.select("#month").selectAll("circle")
			.data(mPoints)
			.enter()
			.append("circle")
			//.attr(circleAttrs)  // Get attributes from circleAttrs var
			.attr('cx', function(d) { return d[0];})
			.attr('cy', function(d) { return d[1];})
			.on("mouseover", handleMouseOver)
			.on("mouseout", handleMouseOut);
				
		// On Click, we want to add data to the array and chart
		d3.select("#svg").on("click", function() {
			var coords = d3.mouse(this);
			console.log("coords----"+coords);
			// Normally we go from data to pixels, but here we're doing pixels to data
			var newData= {
			x: Math.round( xScaleDaily.invert(coords[0])),  // Takes the pixel number to convert to number
			y: Math.round( yScaleDaily.invert(coords[1]))
			};			
			console.log("newData----"+JSON.stringify(newData));
			console.log("newData x----"+JSON.stringify(newData.x));
			console.log("newData y----"+JSON.stringify(newData.y));
			//points = [];			
			//newPoints.push([newData.x, newData.y]);   // Push data to our array
			//console.log("newPoints----"+JSON.stringify(newPoints));
			points.push(coords);   // Push data to our array
			console.log("points----"+JSON.stringify(points));
			//drawCircle(coords[0], coords[1]);						
			d3.select("#svg").selectAll("circle")
				.data(points)
				.enter()
				.append("circle")
				//.attr(circleAttrs)  // Get attributes from circleAttrs var
				.attr('cx', function(d) { return d[0];})
				.attr('cy', function(d) { return d[1];})
				.on("mouseover", handleMouseOver)
				.on("mouseout", handleMouseOut);
			numActivePoints++;
			update();			
		}) 
		
		// On Click, we want to add data to the array and chart
		d3.select("#week").on("click", function() {
			var wcoords = d3.mouse(this);
			console.log("wcoords----"+wcoords);
			// Normally we go from data to pixels, but here we're doing pixels to data
			var wLatestPoints = {
			x: Math.round( xScaleWeekly.invert(wcoords[0])),  // Takes the pixel number to convert to number
			y: Math.round( yScaleWeekly.invert(wcoords[1]))
			};			
			console.log("wLatestPoints----"+JSON.stringify(wLatestPoints));
			console.log("wLatestPoints x----"+JSON.stringify(wLatestPoints.x));
			console.log("wLatestPoints y----"+JSON.stringify(wLatestPoints.y));
			//newPoints.push([wLatestPoints.x, wLatestPoints.y]);   // Push data to our array
			//console.log("newPoints----"+JSON.stringify(newPoints));
			wPoints.push(wcoords);   // Push data to our array
			console.log("wPoints----"+JSON.stringify(wPoints));
			d3.select("#week").selectAll("circle")
				.data(wPoints)
				.enter()
				.append("circle")
				.attr('cx', function(d) { return d[0];})
				.attr('cy', function(d) { return d[1];})
				.on("mouseover", handleMouseOver)
				.on("mouseout", handleMouseOut);
			numActivePoints++;
			update();			
		})
		
		// On Click, we want to add data to the array and chart
		d3.select("#month").on("click", function() {
			var mcoords = d3.mouse(this);
			console.log("mcoords----"+mcoords);
			// Normally we go from data to pixels, but here we're doing pixels to data
			var mLatestPoints = {
			x: Math.round( xScaleWeekly.invert(mcoords[0])),  // Takes the pixel number to convert to number
			y: Math.round( yScaleWeekly.invert(mcoords[1]))
			};			
			console.log("mLatestPoints----"+JSON.stringify(mLatestPoints));
			console.log("mLatestPoints x----"+JSON.stringify(mLatestPoints.x));
			console.log("mLatestPoints y----"+JSON.stringify(mLatestPoints.y));
			//newPoints.push([mLatestPoints.x, wLatestPoints.y]);   // Push data to our array
			//console.log("newPoints----"+JSON.stringify(newPoints));
			mPoints.push(mcoords);   // Push data to our array
			console.log("mPoints----"+JSON.stringify(mPoints));
			d3.select("#month").selectAll("circle")
				.data(mPoints)
				.enter()
				.append("circle")
				.attr('cx', function(d) { return d[0];})
				.attr('cy', function(d) { return d[1];})
				.on("mouseover", handleMouseOver)
				.on("mouseout", handleMouseOut);
			numActivePoints++;
			update();			
		})
		
		
		function updateInfo(info) {
			d3.select('.info .default').style('display', info ? 'none' : 'inline');
			d3.select('.info .text').text(info);
		}
		function updateMenu() {
			var u = d3.select('.menus')
			.selectAll('div.item')
			.data(curveTypes);

			u.enter()
			.append('div')
			.classed('item', true)
			.style('clear', function(d) { return d.clear ? 'left' : 'none'; })
			.text(function(d) { return d.name; })
			.on('click', function(d) {
			d.active = !d.active;
			update();
			})
			.on('mouseover', function(d) { updateInfo(d.info); })
			.on('mouseout', function() { updateInfo(''); })
			.merge(u)
			.style('background-color', function(d, i) { return d.active ? colorScale(i) : '#fff'; })
			.style('color', function(d, i) { return d.active ? 'white' : '#444'; });
		}
		

		function updateLines() {	
			console.log("updateLines numActivePoints"+numActivePoints);			
			curveTypes.forEach(function(d) {
			if(!d.active) return;
			lineGenerator.curve(d.curve);
			d.lineString = lineGenerator(points.slice(0, numActivePoints))
			//d.lineString = lineGenerator(wPoints.slice(0, numActivePoints))
			//d.lineString = lineGenerator(mPoints.slice(0, numActivePoints));
			//console.log("------------"+d.lineString);
			});
			
			var u = d3.select('svg g')
			.selectAll('path')
			.data(curveTypes);

			u.enter()
			.append('path')
			.merge(u)
			.style('stroke', function(d, i) { return colorScale(i); })
			.attr('d', function(d) { return d.lineString;	})
			.style('display', function(d) { return d.active ? 'inline' : 'none'; });
			
			var w = d3.select('week g')
			.selectAll('path')
			.data(curveTypes);

			w.enter()
			.append('path')
			.merge(w)
			.style('stroke', function(d, i) { return colorScale(i); })
			.attr('d', function(d) { return d.lineString;	})
			.style('display', function(d) { return d.active ? 'inline' : 'none'; });
			
			var m = d3.select('month g')
			.selectAll('path')
			.data(curveTypes);

			m.enter()
			.append('path')
			.merge(m)
			.style('stroke', function(d, i) { return colorScale(i); })
			.attr('d', function(d) { return d.lineString;	})
			.style('display', function(d) { return d.active ? 'inline' : 'none'; });
		}

		function updatePoints() {
			console.log("updatePoints numActivePoints"+numActivePoints);
			var u = d3.select("g")
			.selectAll('circle')
			.data(points.slice(0, numActivePoints));
			//.data(wPoints.slice(0, numActivePoints))
			//.data(mPoints.slice(0, numActivePoints));

			u.enter()
			.append('circle')
			.attr('r', 4)
			.call(drag)			
			.merge(u)
			.attr('cx', function(d) { return d[0];})
			.attr('cy', function(d) { return d[1];});
			
			u.exit().remove();
			
			{/* var w = d3.select("g")
			.selectAll('circle')
			.data(wPoints.slice(0, numActivePoints));

			w.enter()
			.append('circle')
			.attr('r', 4)
			.call(wdrag)
			.merge(u)
			.attr('cx', function(d) { return d[0];})
			.attr('cy', function(d) { return d[1];});
			
			w.exit().remove();
			
			var m = d3.select("g")
			.selectAll('circle')
			.data(mPoints.slice(0, numActivePoints));

			m.enter()
			.append('circle')
			.attr('r', 4)			
			.call(mdrag)
			.merge(u)
			.attr('cx', function(d) { return d[0];})
			.attr('cy', function(d) { return d[1];});
			
			m.exit().remove();  */}
		}		
		
		/* function drawCircle(x, y) {
			console.log('Drawing circle at', x, y);
			d3.select("#svg").append("circle")
			//.attr('class', 'click-circle')
			.attr("cx", x)
			.attr("cy", y)
			.attr("r", radius);		
					
		} */
		
		// Create Event Handlers for mouse
        function handleMouseOver(d, i) {  // Add interactivity
		console.log("handleMouseOver----"+handleMouseOver);
            // Use D3 to select element, change color and size
            d3.select(this).attr({
              fill: "orange",
              r: radius * 2
            });

            // Specify where to put label of text
            d3.select("#svg").append("text").attr({
               id: "t" + d.x + "-" + d.y + "-" + i,  // Create an id for text so we can select it later for removing on mouseout
                x: function() { return xScale(d.x) - 30; },
                y: function() { return yScale(d.y) - 15; }
            })
            .text(function() {console.log("x----"+d.x);console.log("y----"+d.y);
              return [d.x, d.y];  // Value of the text
            });
          }

        function handleMouseOut(d, i) {console.log("handleMouseOut----"+handleMouseOut);
            // Use D3 to select element, change color back to normal
            d3.select(this).attr({
              fill: "black",
              r: radius
            });

            // Select text by id and then remove
            d3.select("#t" + d.x + "-" + d.y + "-" + i).remove();  // Remove text location
          }
		function update() {
			updateMenu();
			//updatePointsMenu();
			updateLines();
			updatePoints();
		}		
		update();
	}
	render() {
		const { user, users } = this.props;		
        return (
				<div>
					<svg id="svg" width="360" height="350">
						<g>
						{/*<path></path>
						<g className="points-menu" transform="translate(600, 450)">
							<g className="remove-point">
								<rect x="-6" y="-6" width="12" height="12"></rect>
								<line x1="-6" x2="6"></line>
							</g>
							<g className="add-point" transform="translate(20,0)">
								<rect x="-6" y="-6" width="12" height="12"></rect>
								<line x1="-6" x2="6"></line><line y1="-6" y2="6"></line>
							</g>
						</g>*/}
						</g>  
					</svg>
					<svg id="week" width="360" height="350">
					<g>
							
					</g>  
					</svg>
					<svg id="month" width="360" height="350">
					<g>
							
					</g>  
					</svg>																								
				</div>			
        );
    }
}


function mapStateToProps(state) {
    const { users, authentication } = state;
    const { user } = authentication;
    return {
        user,
        users
    };
}

export default DrawSplinePage;
