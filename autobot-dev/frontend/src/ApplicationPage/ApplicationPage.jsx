import React from 'react';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';
import { Header } from '../Header/Header';
import Footer from '../Footer/Footer';
import ASideLeft from '../ASideLeft/ASideLeft';
import AsideRight from '../AsideRight/AsideRight';
import { userActions } from '../_actions';
import AddApplicationPageN from './AddApplications/AddApplicationPageN'
import { Modal, Button, Grid, Col, Row } from 'react-bootstrap/lib'
import { authHeader, handleResponse } from '../_helpers';
import Toggle from "react-toggle-component";
import { toast } from 'react-toastify';

class ApplicationPage extends React.Component {
	constructor(props) {
		super(props);
		this.state = {
			addApplicationOpen: false,
			editenabled: false,
			page_number: 1,
			application_list: {
				count: 0,
				next: null,
				previous: null,
				results: []
			}
		};
		this.loadApplicationData = this.loadApplicationData.bind(this);
		this.handleProductTable = this.handleProductTable.bind(this);
		this.handleRowEdit = this.handleRowEdit.bind(this);
		this.handleAddEvent = this.handleAddEvent.bind(this);
		this.switchEditMode = this.switchEditMode.bind(this);
		this.closeAddApplication = this.closeAddApplication.bind(this);
		this.openAddApplication = this.openAddApplication.bind(this);
		this.enableAutoScalerForIndex = this.enableAutoScalerForIndex.bind(this);
	}

	componentDidMount() {
		this.loadApplicationData("api/user/applications/");
	}

	loadApplicationData(url) {
		const requestOptions = {
			method: 'GET',
			headers: authHeader(),
		};
		var getQueryString = function (field, url) {
			var href = url ? url : window.location.href;
			var reg = new RegExp('[?&]' + field + '=([^&#]*)', 'i');
			var string = reg.exec(href);
			return string ? string[1] : 1;
		};

		fetch(url, requestOptions)
			.then(handleResponse)
			.then(result => this.setState({ application_list: result }))
			.then(this.setState({ page_number: getQueryString('page', url) }))
	}

	switchEditMode(e) {
		this.setState(prevState => ({
			editenabled: !prevState.editenabled
		}));
		console.log("State edit enabled" + this.state.editenabled)
		e.preventDefault();
	}

	openAddApplication(event) {
		this.setState({
			addApplicationOpen: true
		})
	}

	closeAddApplication(event) {
		this.setState({
			addApplicationOpen: false
		}, () => {
			this.loadApplicationData("api/user/applications/");
		})
	}

	handleProductTable(evt) {

	};

	handleRowEdit(product) {

	};

	handleAddEvent(evt) {

	}
	handleDeleteUser(id) {

	}

	enableAutoScalerForIndex(id, value) {
		//First update  the application
		//Reload application list
		let updateIndex = -1
		this.setState(state => {
			const application_list = state.application_list.results.map((application, index) => {
				if (application.id === id) {
					updateIndex = index
					application.enable_autoscalers = value
					return application
				} else {
					return application;
				}
			});

			return {
				application_list: {
					...state.application_list,
					results: application_list
				}
				,
			};
		}, () => {
			if (updateIndex > -1) {
				var applicationBody = {};
				let stringJson = this.state.application_list.results[updateIndex];
				Object.keys(stringJson).forEach(function (key) {
					if (stringJson[key] !== null)
						applicationBody[key] = stringJson[key];
				});
				fetch('api/user/applications/' + this.state.application_list.results[updateIndex].id + '/',
					{
						headers: new Headers(authHeader()),
						method: 'PATCH',
						body: JSON.stringify(applicationBody)
					}
				).then(response => {
					if (response.ok) {
						toast.success('Application changes has been successfully saved.', {
							position: "top-center",
							autoClose: 5000,
							hideProgressBar: true,
							closeOnClick: true,
							pauseOnHover: true,
							draggable: true
						});
						this.loadApplicationData("api/user/applications/");
					}
					else {
						toast.error("We couldn\'t save your changes. Please try again.!!", {
							position: "top-center",
							autoClose: 5000,
							hideProgressBar: true,
							closeOnClick: true,
							pauseOnHover: true,
							draggable: true
						});
						this.loadApplicationData("api/user/applications/");
					}
				})
					.catch(error => {
						toast.error("We couldn't save your changes. Please try again.!!", {
							position: "top-center",
							autoClose: 5000,
							hideProgressBar: true,
							closeOnClick: true,
							pauseOnHover: true,
							draggable: true
						});
						this.loadApplicationData("api/user/applications/");
					});
			}
		});
	};

	render() {
		const customStyles = {
			content: {
				top: '70px',
				left: '50px',
				right: '50px',
				bottom: '50px',
			},
			overlay: {
				zIndex: 9999,
				backgroundColor: 'rgba(255, 255, 255, 0.2)'
			}
		};
		const { user, users } = this.props;
		var applications = [];
		const application_list = this.state.application_list.results;
		if (application_list) {
			application_list.map((application) => {
				var lb = '-';
				if (application.loadbalancer != null && application.loadbalancer != undefined) {
					if (application.loadbalancer.lb != null && application.loadbalancer.lb != undefined)
						lb = application.loadbalancer.lb
				}
				applications.push(
					this.state.editenabled ?
						(<ProductRow onProductTableUpdate={this.handleProductTable}
							product={application}
							onEditEvent={this.rowEdit}
							onDelEvent={this.rowDel}
							key={application.id} />)
						:
						(<tr>
							<td><Link to={{ pathname: '/application/' + application.id, state: { application_object: application } }}> {application.name} </Link></td>
							<td>
								<div className="btn btn-default btn-sm">{lb}</div>
							</td>
							<td>
								{
									application.instances ? (
										application.instances.map((instance, key) => {
											return (<div key={key} className="btn btn-default btn-sm"> {instance.instance_str ? instance.instance_str : instance.name} </div>)
										})) : ("")
								}
							</td>
							{/* <td>
								{application.auto_scalers && application.auto_scalers.length > 0 && <Toggle className="enable_autoscaler"
									id={application.id}
									checked={application.enable_autoscalers}
									onToggle={(value, event) =>
										this.enableAutoScalerForIndex(application.id, value)
									}
								/>}
							</td> */}
							<td><a><div onClick={this.switchEditMode}><i className="fa fa-edit"></i></div></a></td>
						</tr>
						)

				);
			});
		}


		return (
			<div>
				<Header />
				<ASideLeft />
				<div className="content-wrapper">
					<section className="content">
						<div className="box">
							<div className="box-header with-border">
								<h3 className="box-title">Application</h3>&nbsp;
								<div onClick={this.openAddApplication}><i className="fa fa-plus-circle"></i></div>
								<Modal show={this.state.addApplicationOpen} onHide={this.closeAddApplication} dialogClassName="add-application-modal">
									<Modal.Body className="add-app-modal-body">
										<AddApplicationPageN dismissModal={this.closeAddApplication} />
									</Modal.Body>
								</Modal>
							</div>
							<div className="box-body">
								<div className="row">
									<div className="col-xs-8 col-xs-offset-2">
										<form action="/ci/search_application" method="GET">
											<div className="input-group form-group" style={{ zIndex: "0" }}>
												<div className="input-group-btn search-panel">
													<button type="button" className="btn btn-default dropdown-toggle" data-toggle="dropdown">
														Search by <span id="search_concept"></span><span className="caret"></span>
													</button>
													<ul className="dropdown-menu" role="menu" name="options">
														<li><a href="#Application">Application </a></li>
													</ul>
												</div>
												<input type="hidden" name="search_param" value="Application" id="search_param" />
												<input type="text" className="form-control" name="x" placeholder="Search..." />
												<span className="input-group-btn">
													<button type="Submit" className="btn btn-info btn-flat">Go!</button>
												</span>
											</div>
										</form>
									</div>
								</div>
								<div className="col-md-12">
									{application_list ? (
										<table className="table table-striped">
											<tbody>
												<tr>
													<th>Name</th>
													<th>LoadBalancer</th>
													<th>Instances</th>
													{/* <th>AutoScalers</th> */}
													<th></th>
												</tr>
												{applications}
											</tbody>
										</table>
									) 
									: (<p>No Applications configured</p>)}

								</div>
							</div>
							<div className="box-footer">
								<div className="pagination">
									<span className="step-links">
										{this.state.application_list.previous ? (<a onClick={() => this.loadApplicationData(this.state.application_list.previous)}>Previous{"    "}</a>) : ("")}
										<span className="current"> Page {this.state.page_number}
										</span>
										{this.state.application_list.next ? (<a onClick={() => this.loadApplicationData(this.state.application_list.next)} >{"    "}Next</a>) : ("")}
									</span>
								</div>
							</div>
						</div>
					</section>
				</div>
				<AsideRight />
				<Footer />
			</div>
		);
	}
}


class ProductTable extends React.Component {
	render() {
		var onProductTableUpdate = this.props.onProductTableUpdate;
		var rowDel = this.props.onRowDel;
		var rowEdit = this.props.onRowEdit;
		var filterText = this.props.filterText;
		var product = this.props.products.map(function (product) {
			if (product.name.indexOf(filterText) === -1) {
				return;
			}
			return (<ProductRow
				onProductTableUpdate={onProductTableUpdate}
				product={product}
				onEditEvent={rowEdit.bind(this)}
				onDelEvent={rowDel.bind(this)}
				key={product.id} />)
		});
		return (
			<div>
				<table className="table table-striped">
					<thead>
						<tr>
							<th>Name</th>
							<th>LoadBalancer</th>
							<th>Instances</th>
						</tr>
					</thead>
					<tbody>
						{product}
					</tbody>
				</table>
			</div>
		);
	}
}
class ProductRow extends React.Component {
	onDelEvent() {
		this.props.onDelEvent(this.props.product);
	}
	onEditEvent() {
		this.props.onEditEvent(this.props.product);
	}
	render() {
		var lb = '';
		if (this.props.product.loadbalancer != null && this.props.product.loadbalancer != undefined) {
			if (this.props.product.loadbalancer.lb != null && this.props.product.loadbalancer.lb != undefined)
				lb = this.props.product.loadbalancer.lb
		}

		var instances = []
		this.props.product.instances ? (
			this.props.product.instances.map((instance, key) => {
				instances.push(<div key={key} className="btn btn-default btn-sm"> {instance.instanceId ? instance.instanceId : '-'} </div>)
			})) : ("")

		return (

			<tr className="eachRow">
				<EditableCell onProductTableUpdate={this.props.onProductTableUpdate} cellData={{
					"type": "name",
					value: this.props.product.name,
					id: this.props.product.id
				}} />
				<EditableCell onProductTableUpdate={this.props.onProductTableUpdate} cellData={{
					type: "loadbalancer",
					value: lb,
					id: this.props.product.id
				}} />
				<EditableCell onProductTableUpdate={this.props.onProductTableUpdate} cellData={{
					type: "instances",
					value: instances,
					id: this.props.product.id
				}} />
				<td className="del-cell">
					<a><div onClick={this.onEditEvent.bind(this)}><i class="fa fa-edit"></i></div></a>
				</td>
				<td className="del-cell">
					<a><div onClick={this.onDelEvent.bind(this)}><i className="fa fa-remove"></i></div></a>
				</td>
			</tr>
		);
	}
}
class EditableCell extends React.Component {
	render() {
		return (
			<td>
				{
					(Array.isArray(this.props.cellData.value)) ? (this.props.cellData.value.map((value, key) => {
						<input type='text' name={this.props.cellData.type} id={this.props.cellData.id} value={value} onChange={this.props.onProductTableUpdate} />
					}))
						: (<input type='text' name={this.props.cellData.type} id={this.props.cellData.id} value={this.props.cellData.value} onChange={this.props.onProductTableUpdate} />)
				}
			</td>
		);
	}
}

function mapStateToProps(state) {
	const { users, authentication } = state;
	const { user } = authentication;
	return {
		user,
		users
	};
}

const connectedHomePage = connect(mapStateToProps)(ApplicationPage);
export { connectedHomePage as ApplicationPage };
export default ApplicationPage;