import React from 'react';
import { connect } from 'react-redux';
import {AutoScalerTemplate} from '../../static/constants/AutoscalerTemplate.jsx';
import {FormItems} from '../../static/constants/TemplateConstants';
import _ from 'lodash';
import Select from 'react-select';
import AsyncSelect from 'react-select/lib/Async';
import DynamicForm from '../Common/DynamicForm/DynamicForm'
import Toggle from "react-toggle-component"
import "react-toggle-component/styles.css"
import { authHeader,handleResponse } from '../_helpers';

class AutoScalerPage extends React.Component {
    constructor(props, context){
        super(props, context);
        this.state = { auto_scaler : this.props.auto_scaler,
                           metrics : [],
                        monitoring : []
                     } 
        this.handleTextFieldDataChange = this.handleTextFieldDataChange.bind(this);
        this.handleSelectionChange = this.handleSelectionChange.bind(this);
        this.loadOptions = this.loadOptions.bind(this);
        this.handleCheckBoxChange = this.handleCheckBoxChange.bind(this);
    }

    handleTextFieldDataChange(key , e) {
        e.persist();
        const targetValue = (e.target.validity.valid) ? e.target.value : this.state.auto_scaler[key];
        this.setState(({auto_scaler}) => ({auto_scaler: {
            ...auto_scaler,
            [key]: targetValue,
        }}), () => {
            this.props.updateAutoScalerObject(this.state.auto_scaler)
        })
    }

    handleSelectionChange(key , updatedSelectedValue) {
        console.log("Drop Down Selected")
        console.log(updatedSelectedValue)
        this.setState(({auto_scaler}) => ({auto_scaler: {
            ...auto_scaler,
            [key]: updatedSelectedValue,
        }}), () => {
            this.props.updateAutoScalerObject(this.state.auto_scaler)
        })
    }

    handleCheckBoxChange(key , e) {
        const value = 
        this.setState(({auto_scaler}) => ({auto_scaler: {
            ...auto_scaler,
            [key]: !this.state.auto_scaler[key],
        }}), () => {
            this.props.updateAutoScalerObject(this.state.auto_scaler)
        })
    }

    componentDidMount() {
        this.loadOptions("monitorings");
        this.loadOptions("metrics");
    }

    loadOptions(modelName) {  
        fetch('api/user/' + modelName,{
			headers : new Headers(authHeader())
		})
            .then(handleResponse)
            .then(result =>  {
                if (modelName == "monitorings") {
                    this.setState({monitoring : result.results});
                }
                else {
                    this.setState({metrics : result.results});
                }
            })  
    }

    render() {
        let scalerTemplate = AutoScalerTemplate.fields(this.state.auto_scaler.object_type);
        let extradatasoucre = {
            metrics : this.state.metrics ,
            monitoring : this.state.monitoring
        }
        return (<div>
                    <DynamicForm datasource={this.state.auto_scaler} 
                                 template={scalerTemplate}
                                 extradatasource={extradatasoucre}
                                 handleTextFieldDataChange = {this.handleTextFieldDataChange}
                                 handleDropDownDataChange = {this.handleSelectionChange}
                                 handleCheckBoxDataChange = {this.handleCheckBoxChange}
                    />
            </div>);
    }
}

function mapStateToProps(state) {
    const { users, authentication } = state;
    const { user } = authentication;
    return {
        user,
        users
    };
}

const connectedHomePage = connect(mapStateToProps)(AutoScalerPage);
export { connectedHomePage as AutoScalerPage };
export default AutoScalerPage
