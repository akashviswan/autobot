import React from "react";
import { FormItems } from "../../../static/constants/TemplateConstants";
import _ from "lodash";
import Select from "react-select";
import Toggle from "react-toggle-component";
import PropTypes from "prop-types";
import AsyncSelect from "react-select/lib/Async";
import { bind } from "bluebird";
const modalRoot = document.getElementById("modal-root");
import DateTimeField from "react-datetime"
import "react-datetime/css/react-datetime.css"

class DynamicFormItem extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      fielddata: this.props.fielddata,
      data: this.props.datasource,
      extradatasources: this.props.extradatasources,
      validation: {
        isAutoValidationEnabled: false,
        isValid: true,
        isSubFieldValid: true,
        innerFieldTemplates: []
      }
    };
    this.handleDataSourceUpdate = this.handleDataSourceUpdate.bind(this);
    this.validate = this.validate.bind(this);
    this.resetValidations = this.resetValidations.bind(this)
    this.onStateUpdateCompletion = this.onStateUpdateCompletion.bind(this);
  }

  handleDataSourceUpdate(key, updatedValue) {
    let sectiondatakey = this.props.section_data_key
    if (sectiondatakey) {
      this.setState({
        data: {
          ...this.state.data,
          [sectiondatakey]: Array.isArray(updatedValue) ? updatedValue : {
            ...this.state.data[sectiondatakey],
            [key]: updatedValue
          }
        }
      }, () => {
        this.onStateUpdateCompletion()
      });
    }
    else {
      this.setState({
        data: {
          ...this.state.data,
          [key]: updatedValue
        }
      }, () => {
        this.onStateUpdateCompletion()
      });
    }
  }

  onStateUpdateCompletion() {
    this.props.updatedatasource(this.state.data);
    if (this.state.validation.isAutoValidationEnabled) {
      this.validate()
    }
    this.forceUpdate()
  }
  componentWillReceiveProps(newProps) {
    this.setState({
      fielddata: newProps.fielddata,
      data: newProps.datasource,
      extradatasources: newProps.extradatasources,
    });
  }

  resetValidations() {
    let self = this
    self.setState({
      validation: {
        ...self.state.validation,
        isAutoValidationEnabled: false,
        isValid: true,
        isSubFieldValid: true
      }
    })
    let fielddata = this.state.fielddata;
    if (fielddata.fields && fielddata.type === FormItems.DROPDOWN) {
      self.state.validation.innerFieldTemplates.map(function (field) {
        self.refs[fielddata.key + field.key].resetValidations();
      })
    }
  }
  validate() {
    let self = this;
    let isValid = true
    let fielddata = this.state.fielddata;

    if (fielddata.isrequired == true) {
      let sectiondatakey = this.props.section_data_key
      let datasource = sectiondatakey ? self.state.data[sectiondatakey] : self.state.data;
      if (fielddata.type == FormItems.DROPDOWNMULTI && datasource[fielddata.key].length == 0) {
        isValid = false
      } else if (fielddata.type == FormItems.DROPDOWN || fielddata.type == FormItems.TEXTFIELD) {
        if (!datasource[fielddata.key]) {
          isValid = false
        }
      }
    }
    //Validate the sub fields
    let subfieldsValid = true
    if (fielddata.fields && fielddata.type === FormItems.DROPDOWN) {
      subfieldsValid = self.state.validation.innerFieldTemplates.reduce(function (accumulator, currentvalue) {
        return self.refs[fielddata.key + currentvalue.key].validate() && accumulator
      }, true)
    }
    //update validations of self and sub fields
    self.setState({
      validation: {
        ...this.state.validation,
        isAutoValidationEnabled: true,
        isValid: isValid,
        isSubFieldValid: subfieldsValid
      }
    })
    return isValid && subfieldsValid;
  }

  render() {
    let self = this;
    let formFields = [];
    if (this.state.fielddata) {
      let fielddata = this.state.fielddata;
      let sectiondatakey = this.props.section_data_key
      let datasource = sectiondatakey ? self.state.data[sectiondatakey] : self.state.data;
      if (fielddata.type === FormItems.TEXTFIELD) {
        let value = "";
        if (datasource && datasource[fielddata.key]) {
          value = datasource[fielddata.key];
        }
        formFields.push(
          <div>
            <div>{fielddata.label}:</div>
            <input
              key={fielddata.key}
              type="text"
              validate={self.state.validation.isValid ? '' : 'error'}
              {...(fielddata.pattern != null && fielddata.pattern != undefined
                ? { pattern: fielddata.pattern }
                : {})}
              value={value == null ? "" : value}
              onChange={e => {
                if (e.target.validity.valid) {
                  e.persist();
                  self.handleDataSourceUpdate(fielddata.key, e.target.value);
                } else {
                  console.log("inavalid data entered for key" + fielddata.key);
                }
              }}
              className="input form-input"
            />
          </div>
        );
      } else if (fielddata.type === FormItems.CHECKBOX) {
        let value = false;
        if (datasource && datasource[fielddata.key]) {
          value = datasource[fielddata.key];
        }
        formFields.push(
          <div>
            <br />
            <div>
              {fielddata.label}:&nbsp;&nbsp;
              <Toggle
                checked={value}
                onToggle={value =>
                  self.handleDataSourceUpdate(fielddata.key, value)
                }
              />
            </div>
            <br />
          </div>
        );
      } else if (
        fielddata.type === FormItems.DROPDOWN ||
        fielddata.type === FormItems.DROPDOWNMULTI
      ) {
        let isMulti = fielddata.type === FormItems.DROPDOWNMULTI;
        let value = null;
        // Check if values exists
        // if exists , use that values to list the dropdown
        // if not get the list from data sources with  data source key/ model name by default
        let dropdownoptions = [];
        if (fielddata.values) {
          dropdownoptions = fielddata.values;
        } else {
          if (
            self.state.extradatasources &&
            self.state.extradatasources[fielddata.model]
          ) {
            dropdownoptions = self.state.extradatasources[fielddata.model]; //get from extradatasources
          }
        }

        //selected Value
        if (datasource && datasource[fielddata.key]) {
          if (fielddata.dropdown_value_key) {
            let tempValue = datasource[fielddata.key];
            if (isMulti) {
              var selectedOptions = dropdownoptions.filter(
                option =>
                  tempValue.indexOf(option[fielddata.dropdown_value_key]) !== -1
              );
              value = selectedOptions;
            } else {
              let selectedOptions = dropdownoptions.filter(function (itm) {
                return itm[fielddata.dropdown_value_key] == tempValue;
              });
              if (selectedOptions.length > 0) {
                value = selectedOptions[0];
              }
            }
          } else {
            value = datasource[fielddata.key];
          }
        }
        formFields.push(
          <div>
            <div>{fielddata.label}:</div>
            <Select
              className="react-select-container"
              classNamePrefix="react-select"
              styles={{
                control: (base, state) => ({
                  ...base,
                  border: (!self.state.validation.isValid) ? '1px solid #FFBABA' : "1px solid lightgray", // default border color
                }),
              }}
              menuPortalTarget={
                modalRoot != null && modalRoot != undefined ? modalRoot : null
              }
              value={value}
              options={dropdownoptions}
              getOptionLabel={option =>
                fielddata.dropdown_label_key
                  ? option[fielddata.dropdown_label_key]
                  : option["label"]
              }
              getOptionValue={option =>
                fielddata.dropdown_value_key
                  ? option[fielddata.dropdown_value_key]
                  : option
              }
              onChange={selectedOption => {
                if (isMulti) {
                  if (fielddata.dropdown_value_key) {
                    let value = selectedOption.map(function (option) {
                      return option[fielddata.dropdown_value_key];
                    });
                    self.handleDataSourceUpdate(fielddata.key, value);
                  } else {
                    self.handleDataSourceUpdate(fielddata.key, selectedOption);
                  }
                } else {
                  let value = fielddata.dropdown_value_key
                    ? selectedOption[fielddata.dropdown_value_key]
                    : selectedOption;
                  self.resetValidations()
                  setTimeout(function () { //Start the timer
                    self.handleDataSourceUpdate(fielddata.key, value);
                  }.bind(this), 100)
                }
              }}
              isMulti={isMulti}
            />
          </div>
        );
        //get values from datasource with  data source key/ model name by default

        if (fielddata.fields && !isMulti) {
          if (value) {
            let fieldKey = "value";
            //By default we assume value and label exists in data
            if (fielddata.dropdown_value_key) {
              fieldKey = fielddata.dropdown_value_key;
            }
            if (self.state.validation.innerFieldTemplates != fielddata.fields[value[fieldKey]]) {
              this.setState({
                validation: {
                  ...this.state.validation,
                  innerFieldTemplates: fielddata.fields[value[fieldKey]]
                }
              })
            }
            fielddata.fields[value[fieldKey]].map(function (field) {
              formFields.push(
                <DynamicFormItem
                  ref={fielddata.key + field.key}
                  fielddata={field}
                  datasource={datasource}
                  updatedatasource={self.props.updatedatasource}
                  extradatasources={self.props.extradatasources}
                />
              );
            });
          }
        }
      }
      else if (fielddata.type === FormItems.DATETIMEPICKER) {
        let value = null;
        if (datasource && datasource[fielddata.key]) {
          value = datasource[fielddata.key];
        }
        formFields.push(
          <div>
            <br />
            <div>
              {fielddata.label}:&nbsp;&nbsp;
              <DateTimeField
                dateTime={value}
                onChange={value =>
                  self.handleDataSourceUpdate(fielddata.key, value)
                }
              />
            </div>
            <br />
          </div>
        );
      }
      else if (fielddata.type === FormItems.TEXTVIEW) {
        let value = "";
        if (datasource && datasource[fielddata.key]) {
          value = datasource[fielddata.key];
        }
        formFields.push(
          <div>
            <div>{fielddata.label}:</div>
            <textarea
              key={fielddata.key}
              type="text"
              validate={self.state.validation.isValid ? '' : 'error'}
              {...(fielddata.pattern != null && fielddata.pattern != undefined
                ? { pattern: fielddata.pattern }
                : {})}
              value={value == null ? "" : value}
              onChange={e => {
                if (e.target.validity.valid) {
                  e.persist();
                  self.handleDataSourceUpdate(fielddata.key, e.target.value);
                } else {
                  console.log("inavalid data entered for key" + fielddata.key);
                }
              }}
              className="input form-input-textarea input-textarea"
            />
          </div>
        );
      }
    }
    return formFields;
  }
}

DynamicFormItem.propTypes = {
  fielddata: PropTypes.object.isRequired
};

export default DynamicFormItem;
