import React from "react";
import PropTypes from "prop-types";
import DynamicFormItem from "./DynamicFormItem";
import { Button, Table } from "react-bootstrap/lib";
import Loader from "react-loader-spinner";
import DynamicFormComponent from "./DynamicFormComponent"

class AddAppAutoScalers extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      pagedata: this.props.pagedata,
      error: null,
      showLoading: false,
      extradatasources: this.props.extradatasources,
      autoscalers: [],
      selectedAutoScaler: null
    };
    this.handleNext = this.handleNext.bind(this)
    this.handlePrev = this.handlePrev.bind(this)
    this.addNewAutoScaler = this.addNewAutoScaler.bind(this)
    this.switchAddScalerProperty = this.switchAddScalerProperty.bind(this)
    this.saveAutoScaler = this.saveAutoScaler.bind(this)
    this.editExistingAutoScaler = this.editExistingAutoScaler.bind(this);
    this.deleteAutoScaler = this.deleteAutoScaler.bind(this);
  }

  componentWillReceiveProps(newprops) {
    this.setState({
      extradatasources: newprops.extradatasources,
    })
  }
  handleNext() {
    let self = this;
    self.setState({
      showLoading: true,
      error: null
    })
      self.props.onNextButtonClick(self.state.pagedata,{
      autoscalers : self.state.autoscalers
    },function(error ,pagenumber) {
      self.setState({
        showLoading: false
      }, () => {
        if (error) {
          self.setState({ error: error.message});
        } else {
          self.props.dismissModal()
        }
      })
    })
  }

  handlePrev() {
    let self = this
    this.props.onBackButtonClick(self.state.pagedata,function(pagenumber) {
      self.props.jumpToStep(pagenumber);
    })
  }

  editExistingAutoScaler(index) {
    this.setState({
      selectedAutoScaler: {
        index: index,
        autoscaler: this.state.autoscalers[index]
      }
    });
    this.switchAddScalerProperty();
  }

  deleteAutoScaler(index) {
    const autoscalers = this.state.autoscalers;
    if (autoscalers.length > index) {
      autoscalers.splice(index, 1);
    }
    this.setState({
      autoscalers: autoscalers
    });
  }

  addNewAutoScaler() {
    this.switchAddScalerProperty();
  }

  saveAutoScaler(pagedata, formdata, callback) {
    if (!this.state.selectedAutoScaler) {
      this.setState({
        autoscalers: [
          ...this.state.autoscalers,
          formdata
        ]
      });
    } else {
      const autoscalers = this.state.autoscalers;
      autoscalers[
        this.state.selectedAutoScaler.index
      ] = this.state.selectedAutoScaler.autoscaler;
      this.setState({
        autoscalers: autoscalers
      });
    }
    this.setState({
      selectedAutoScaler : null
    },()=> {
      this.switchAddScalerProperty();
    })
  }

  switchAddScalerProperty() {
    //switch back to normal table mode/viceversa
    this.setState({ addScalerEnabled: !this.state.addScalerEnabled });
  }

  render() {
    let self = this;
    let content = [];
    content.push(
      self.state.error ? (
        <div className="error infolabel">{self.state.error.message}</div>
      ) : ("")
    );

    let autoscalerTableData = [];
    _.forEach(self.state.autoscalers, function (value, key) {
      autoscalerTableData.push(
        <tr>
          <td>{key}</td>
          <td>{value.resourcetype}</td>
          <td>{value.priority}</td>
          <td>
            <a>
              <div onClick={() => self.editExistingAutoScaler(key)}>
                <i className="fa fa-edit" />
              </div>
            </a>
          </td>
          <td>
            <a>
              <div onClick={() => self.deleteAutoScaler(key)}>
                <i className="fa fa-trash" />
              </div>
            </a>
          </td>
        </tr>
      );
    });
    return self.state.addScalerEnabled ? (
      <DynamicFormComponent
        nextbutton="Save"
        backbutton="Cancel"
        ref={self.state.pagedata.page_id}
        pagedata={self.state.pagedata}
        onNextButtonClick={self.saveAutoScaler}
        onBackButtonClick={self.switchAddScalerProperty}
        extradatasources={self.state.extradatasources}
        getModelObjects={self.props.getModelObjects} />
    ) : (
        <div className="multistep-page page" id="addapp">
          <div className="add-app-dummy-div" />
          <div className="align-right">
            <Button
              bsStyle="primary"
              className="align-right"
              onClick={self.addNewAutoScaler}
            >
              New Autoscaler
          </Button>
          </div>
          <Table responsive>
            <thead>
              <tr>
                <th>#</th>
                <th>AutoScaler Type</th>
                <th>Priority</th>
                <th>Edit</th>
                <th>Delete</th>
              </tr>
            </thead>
            <tbody>{autoscalerTableData}</tbody>
          </Table>
          <div className="add-app-footer">
            <Button
              bsStyle="primary"
              className="footer-primary"
              onClick={self.handleNext}
            >
              Next &raquo;
          </Button>
            <Button
              bsStyle="default"
              className="footer-secondary"
              onClick={self.handlePrev}
            >
              &laquo; Back
          </Button>
          </div>
          {content}

        </div>
      );
  }
}
export default AddAppAutoScalers;
