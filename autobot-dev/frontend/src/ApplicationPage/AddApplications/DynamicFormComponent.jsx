import React from "react";
import PropTypes from "prop-types";
import DynamicFormItem from "./DynamicFormItem";
import { Button } from "react-bootstrap/lib";
import Loader from "react-loader-spinner";

class DynamicFormComponent extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      pagedata: this.props.pagedata,
      data: this.props.defaultdata ? this.props.defaultdata  : {}, //This is where form data get saved on editing form
      error: null,
      showLoading: false,
      extradatasources : this.props.extradatasources
    };
    this.updatedatasource = this.updatedatasource.bind(this);
    this.handleNext = this.handleNext.bind(this);
    this.handlePrev = this.handlePrev.bind(this);
    this.loadFormModelObjectsData = this.loadFormModelObjectsData.bind(this)
  }

  componentDidMount() {
    this.loadFormModelObjectsData()
  }

  //process page data and load all model objects as required
  loadFormModelObjectsData() {
    //Add logic for showing the loading indicator
    let self = this
    if (self.state.pagedata.models) {
      self.state.pagedata.models.map(function(model) {
        //Fetch all model objects
          self.props.getModelObjects(self.state.pagedata , model,function(modelobjects , error) {
            self.setState((prevState,props)=>({
              extradatasources : {
                ...prevState.extradatasources,
                [model] : modelobjects
            }}))
          })
      })
    } 
  }

  handleNext() {
    //Validate
    let self = this;
    let fields = self.state.pagedata.fields;
    let isValid = true
    if (Array.isArray(fields)) {
      isValid = fields.reduce(function (accumulator, currentvalue) {
        return self.refs[currentvalue.key].validate() && accumulator;
      }, true);
    }
    else {
      console.log("Map objects as sections in the Page");
      for (var key in fields) {
        isValid = isValid && fields[key].fields.reduce(function (accumulator, currentvalue) {
          return self.refs[currentvalue.key].validate() && accumulator;
        }, true);
      }
    }
   
    if (isValid) {
      //Jump to Next page only if validated and service is successful
      self.setState({
        showLoading: true,
        error: null
      }, () => {
        self.props.onNextButtonClick(
          self.state.pagedata,
          self.state.data,
          function (error, pagenumber) {
            self.setState({
              showLoading: false
            }, () => {
              if (error) {
                self.setState({ error: error.message});
              } else {
                self.props.jumpToStep(pagenumber);
              }
            })
          }
        );
      })
    }
    this.forceUpdate();
  }

  handlePrev() {
    let self = this
    this.props.onBackButtonClick(self.state.pagedata,function(pagenumber) {
      self.props.jumpToStep(pagenumber);
    })
  }

  componentWillReceiveProps(newProps) {
    this.setState({
      pagedata: newProps.pagedata,
      extradatasources : newProps.extradatasources
    });
  }

  updatedatasource(newData) {
    this.setState({
      data: newData
    });
  }

  render() {
    let self = this;
    let form = [];
    if (
      self.state.pagedata &&
      self.state.pagedata.fields
    ) {
      let fields = self.state.pagedata.fields;
      if (Array.isArray(fields)) {
        fields.map(function (fielddata) {
          form.push(
            <DynamicFormItem
              ref={fielddata.key}
              fielddata={fielddata}
              datasource={self.state.data}
              updatedatasource={self.updatedatasource}
              extradatasources = {self.state.extradatasources}
            />
          );
        });
      }
      else {
        for (var key in fields) {
            let formfields = []
            let datasourcekey = fields[key].section_datasource_key
            {fields[key].fields.map(function (fielddata) {
              formfields.push(<DynamicFormItem
                  ref={fielddata.key}
                  fielddata={fielddata}
                  section_data_key= {datasourcekey ? datasourcekey : null}
                  datasource={self.state.data}
                  updatedatasource={self.updatedatasource}
                  extradatasources = {self.state.extradatasources}
                />)
              })}
              form.push(
                <div className="pageSection">
                <h5>{fields[key].section_label}</h5>{formfields}</div>
              )
        } 
      }
      
    }
    let content = []
    content.push(self.state.showLoading ? (
      <div className="add-app-loader">
        <Loader class="add-app-loader" type="Oval" color="#4288CC" height="40" width="40" />
      </div>
    ) : (
          <div className="multistep-page page" id="addapp">
            <div className="add-app-dummy-div" />
            {form}
            <div className="add-app-footer">
              <Button
                bsStyle="primary"
                className="footer-primary"
                onClick={self.handleNext}>
                { this.props.nextbutton ? this.props.nextbutton :  "Next"}
              </Button>
              {(self.state.pagedata.pagenumber != 0) && <Button
                bsStyle="default"
                className="footer-secondary"
                onClick={self.handlePrev}>
                { this.props.backbutton ? this.props.backbutton :  "Back"}
              </Button>}
            </div>
            {self.state.error && <div className="error infolabel">{self.state.error}</div>}
          </div>
      ))
    return content
  }
}

DynamicFormComponent.propTypes = {
  pagedata: PropTypes.object.isRequired
};

export default DynamicFormComponent;
