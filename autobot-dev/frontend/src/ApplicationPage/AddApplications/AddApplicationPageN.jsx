//Container - Contains all state handling and data of forms
import React from "react";
import StepZilla from "react-stepzilla";
import "./AddApplication.css";
import { Modal, Button, Grid, Col, Row } from "react-bootstrap/lib";
import DynamicFormComponent from "./DynamicFormComponent";
import { ModelUrl } from "../../../static/constants/TemplateConstants";
import AddAppAutoScalers from "./AddAppAutoScalers";
import { authHeader,handleResponse } from "../../_helpers";

class AddApplicationPageN extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      //Get template data from server 
      template: null,

      //Type of cloud (aws / azure ..)
      cloudtype: null,

      credentials: null,
      application: null,
      monitoring: null,
      metrics: null,
      autoscalerdata: {
        autoscalers: [],
        counter: 0
      }
    };
    this.fetchAddAppTemplate = this.fetchAddAppTemplate.bind(this);
    this.fetch = this.fetch.bind(this);
    this.post_or_patch = this.post_or_patch.bind(this);
    this.processTemplate = this.processTemplate.bind(this);
    this.onNextButtonClick = this.onNextButtonClick.bind(this);
    this.onBackButtonClick = this.onBackButtonClick.bind(this);
    this.getModelObjects = this.getModelObjects.bind(this);
    this.getStepsDefaultData = this.getStepsDefaultData.bind(this);
  }

  componentDidMount() {
    //1. Fetch App template and load form from template using dynamic json form
    this.fetchAddAppTemplate();
  }

  getStepsDefaultData(page_id) {
    if (page_id == "credentials") {
      return this.state.credentials;
    } else if (page_id == "application") {
      return this.state.application ? this.state.application : {};
    } else if (page_id == "scaler") {
      return (this.state.application && this.state.application.scaler) ? this.state.application.scaler : {};
    } else if (page_id == "monitoringandmetrics") {
      return (this.state.application && this.state.application.monitoring && this.state.application.metrics) ? {
        monitoring: this.state.application.monitoring,
        metrics: this.state.application.metrics
      } : {}
    }
  }
  //Common Fetch
  fetch(url, errorMessage) {
    return fetch(url,{
      headers : new Headers(authHeader())
    }).then(handleResponse);
  }

  post_or_patch(url, data, errormessage, method_type) {
    return fetch(url, {
      method: method_type,
      headers : new Headers(authHeader()),
      body: JSON.stringify(data)
    }).then(handleResponse);
  }

  fetchAddAppTemplate() {
    this.fetch("api/createAppTemplate", "Please try again")
      .then(template => this.setState({ template: template }))
      .catch(error => console.log(error));
  }

  getModelObjects(pagedata, model, callback) {
    let self = this;
    //using local data
    if (pagedata.page_id == "autoscaler") {
      if (model == "Monitoring") {
        callback(self.state.extradatasources.application_monitoring, null);
        return;
      } else if (model == "Metric") {
        callback(self.state.extradatasources.application_metrics, null);
        return;
      }
    }

    let url = ModelUrl[model];
    if (url) {
      self.fetch(url, "Please try again")
        .then(response => {
          self.setState({
            extradatasources : {
              ...self.state.extradatasources,
              [model] : response.results
            }
          })
          callback(response.results, null)
        })
        .catch(error => callback(null, error.messgae));
    }
  }
  onBackButtonClick(pagedata, callback) {
    if (pagedata.page_id == "credentials") {
      callback(0);
    } else if (pagedata.page_id == "application") {
      callback(0);
    } else if (pagedata.page_id == "scaler") {
      callback(1);
    } else if (pagedata.page_id == "monitoringandmetrics") {
      callback(2);
    } else if (pagedata.page_id == "autoscaler") {
      callback(3);
    }
  }

  //Called just before moving to next page
  onNextButtonClick(pagedata, formdata, callback) {
    let self = this;
    //Fetch/Post data and jump to next page
    if (pagedata.page_id == "credentials") {
      //Assume as credentials
      //Save credential data to main app object
      self.setState({
        cloudtype: formdata.resourcetype == "AWSCredential" ? "AWS" : "Azure"
      });
      this.setState({ credentials: formdata }, () => {
        this.post_or_patch(
          "api/getLoadBalancers/",
          formdata,
          "Please try again",
          "POST"
        ).then(data =>
          self.setState(
            {
              extradatasources: {
                ...self.state.extradatasources,
                LoadBalancer: data
              }
            },
            () => {
              callback(null, 1);
            }
          )
        )
          .catch(error => {
            callback(error, 1)
            console.log(error)
          } );
      });
    } else if (pagedata.page_id == "application") {
      let app_data = formdata;
      app_data = {
        ...app_data,
        credentials: self.state.credentials
      };
      this.post_or_patch(
        "api/createApplication/",
        app_data,
        "Please try again",
        "POST"
      )
        .then(data =>
          self.setState(
            {
              credentials: data.credentials[0],
              application: {
                ...self.state.application,
                ...data
              }
            },
            () => {
              callback(null, 2);
            }
          )
        )
        .catch(error => callback(error, 2));
    } else if (pagedata.page_id == "scaler") {
      let hasId = formdata.id != null && formdata.id != undefined;
      let url = hasId
        ? "api/user/scalers/" + formdata.id + "/"
        : "api/user/scalers/";
      let method_type = hasId ? "PATCH" : "POST";
      this.post_or_patch(url, formdata, "Please try again", method_type)
        .then(data => {
          //Save appln data to somewhere.
          let app_url =
            "api/user/applications/" + this.state.application.id + "/";
          let app_methodType = "PATCH";
          let scalerdata = {
            scaler: data
          };
          this.post_or_patch(
            app_url,
            scalerdata,
            "Please try again",
            app_methodType
          )
            .then(data => {
              self.setState(
                {
                  application: {
                    ...self.state.application,
                    ...data
                  }
                },
                () => {
                  callback(null, 3);
                }
              );
            })
            .catch(error => callback(error, 3));
        })
        .catch(error => callback(error, 3));
    } else if (pagedata.page_id == "monitoringandmetrics") {
      let hasId =
        formdata.monitoring.id != null && formdata.monitoring.id != undefined;
      let url = hasId
        ? "api/user/monitorings/" + formdata.monitoring.id + "/"
        : "api/user/monitorings/";
      let methodType = hasId ? "PATCH" : "POST";
      let updatedformdata = {
        ...formdata.monitoring,
        resourcetype:
          self.state.cloudtype == "AWS" ? "AWSCloudWatch" : "Monitoring"
      };
      self
        .post_or_patch(url, updatedformdata, "", methodType)
        .then(data => {
          this.setState({ monitoring: data }, () => {
            let app_url =
              "api/user/applications/" + this.state.application.id + "/";
            let app_method_type = "PATCH";
            self
              .post_or_patch(
                app_url,
                {
                  monitoring: [self.state.monitoring],
                  metrics: formdata.metrics
                },
                "",
                app_method_type
              )
              .then(data => {
                self.setState(
                  {
                    extradatasources: {
                      ...self.state.extradatasources,
                      application_metrics: data.metrics,
                      application_monitoring: data.monitoring
                    },
                    application: {
                      ...self.state.application,
                      ...data
                    }
                  },
                  () => {
                    callback(null, 4);
                  }
                );
              })
              .catch(error => callback(error, 4));
          });
        })
        .catch(error => callback(error, 4));
    } else if (pagedata.page_id == "autoscaler") {
      for (let i = 0; i < formdata.autoscalers.length; i++) {
        self.setState({
          autoscalerdata: {
            ...self.state.autoscalerdata,
            autoscalers: formdata.autoscalers,
            counter: i + 1
          }
        });
        let autoscaler = formdata.autoscalers[i];
        let hasId = autoscaler.id != null && autoscaler.id != undefined;
        let url = hasId
          ? "api/user/autoscalers/" + autoscaler.id + "/"
          : "api/user/autoscalers/";
        let methodType = hasId ? "PATCH" : "POST";
        self
          .post_or_patch(url, autoscaler, "", methodType)
          .then(data => {
            const array_auto_scalers = self.state.autoscalerdata.autoscalers
            array_auto_scalers[i] = data
            self.setState({
              autoscalerdata: {
                ...self.state.autoscalerdata,
                autoscalers: array_auto_scalers,
                counter: self.state.autoscalerdata.counter - 1
              }
            }, () => {
              if (self.state.autoscalerdata.counter == 0) {
                //Save to App

                let app_url = 'api/user/applications/' + self.state.application.id + "/";
                let app_method = "PATCH"
                self.post_or_patch(app_url, {
                  auto_scalers: self.state.autoscalerdata.autoscalers
                }, "", app_method)
                  .then(data => {
                    callback(null, 5);
                  })
                  .catch(error => callback(error, 4));
              }
            })


          })
          .catch(error => callback(error, 4));
      }
    }
  }

  processTemplate(template) {
    let self = this;
    //Some How loop and pass data to Dynamic JSON form
    let steps = [];
    template.map(function (pagedata) {
      let defaultdata = self.getStepsDefaultData(pagedata.page_id)
      let step = null;
      if (pagedata.page_id == "autoscaler") {
        step = {
          name: pagedata.page,
          component: (
            <AddAppAutoScalers
              key={pagedata.page_id}
              ref={pagedata.page_id}
              pagedata={pagedata}
              defaultdata={defaultdata}
              extradatasources={self.state.extradatasources}
              onNextButtonClick={self.onNextButtonClick}
              onBackButtonClick={self.onBackButtonClick}
              getModelObjects={self.getModelObjects}
              dismissModal={self.props.dismissModal}
            />
          )
        };
      } else {
        let hidebackbutton = (pagedata.page_id == "scaler")
        step = {
          name: pagedata.page,
          component: (
            <DynamicFormComponent
              key={pagedata.page_id}
              ref={pagedata.page_id}
              hideBackButton={hidebackbutton}
              pagedata={pagedata}
              defaultdata={defaultdata}
              extradatasources={self.state.extradatasources}
              onNextButtonClick={self.onNextButtonClick}
              onBackButtonClick={self.onBackButtonClick}
              getModelObjects={self.getModelObjects}
            />
          )
        };
      }
      steps.push(step);
    });
    return steps;
  }

  render() {
    let self = this;
    let steps = null;
    if (self.state.template) {
      steps = self.processTemplate(self.state.template);
    }
    return (
      <Grid fluid={true} className="add-app-container">
        <Row className="add-app-row">
          {/* <Button><i className="fa fa-close" style={{fontSize:24}}></i></Button>*/}
          <Col
            sm={4}
            xsHidden={true}
            className="add-app-col add-app-header-col"
          >
            <div className="add-app-header-content">
              <img
                src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAaMAAAEcCAYAAABqCdtUAAAgAElEQVR4nO3dB5gdV30o8P/0mdu3qnolWdausYUxxQQS/MAQCCUh8MhLIDgGJ5QkhITOi21qEkroBEgIBIjBSRwg9JJnA7EpxjGxXGRJu6qr7fX26eec981KMrLOarXl3mn3//s+f5jzH++dObf855w5RWCMAULokSZdsLIiqFkJRAEAJIE7ZEUOBWAArEGAmAzMARWKKx2PUKfDZIQ6yqxJnmv7ZK/juppL2R4mSXtFQeihhIAqSzlDkY28Kql5RZTWmoDOx6WM1VxKGq7vOD41XUKrkiQCozAnMf8uXRQPqapqdWeUO3QJxs7zZxBKNUxGKNVOOFApSZArySAl5Tp9BjDrg91kML1HhV3cAQilECYjlBozDfu5Zct7hkvgiaoi7+nPqr3dupSYJHSuqkvpguk2LI9OAqP7cop0T1dWva2gSvu5gxFKOExGKLFGKHw/T+Bp/TJorepSS4opF9yaAMNDClyBn2CUBpiMUKJMNL1Xlk3r8Zbn7MxqxpWX9eQ2deo7aPqMnazYMz6hB7OafOeukvZO7iCEEgKTEYq9YQI/LFF4yiYFFHy3VmZRgHEK43tkuGjFAxGKGUxGKLaOLjgfanruM3py+tC2nKLjO7U6LmHsSNWaopTcsSmvfqlP076bhPNGnQ2TEYqVYQ9+fpEIT8xI0GFPgdrrsAXjewxsLaH4EvG9QXFwdLFxw77Jyt2a6ezFRNR63UA23z9dP/jQbPVDHqUDabs+lHzYMkKROQ7w/rwHb+hVQMZ3IVwmBTbG4L4hCR7XSdeN4gtbRih0NmEXHZqtv9+abV6LiSgaGRGEAcau3Dc297EF072mE+sAxQu2jFBojgD8Q8aHP9oqYwKKo2Ef9g3J2FJC0cCWEQrFwfnm573F5gswEcWXajmD90/XvjPX8J7d6XWBwoctI9RWxx1Y3KVBF9Zysix4QHqwCxWFCFtGqC3GG95r901V7+0CUsAaTp5gYdn9s/WxQwvNT3R6XaBwYMsItVzVB1KU8UYnTUZceHBQxXXwUPvgDwZqmcNl5/MH5ptTmIjSRzKtnQ/M1L5v+zhxFrUHtoxQS7iUUVUUcLJqBzjhQnmnCt2dXg+otfAOFm3I8AK9bazumViLnSNH/fz+mcZtTQd2d3pdoNbBlhFal6MUPrNbhFdg7XW2ozbM7NZhc6fXA9o4bBmhNZuqNZ8p1dzfw5pDmu+W9s817+j4ikAbhi0jtGpTANdmffjnAg5QQMsYBtg3BLiCA1of/FFBq1K2/V+Znaq/CRMROp9NDrniSLnxtvOEEVoRtozQBY2YMDyYgUGsKbQaDQKsKcH/2QTwVawwtFp4l4tW9NBs418Uz8UH1GjVchIIUxO1NzY9HG2HVg9bRmhZRwE+sZ3Ca7QU3640fcpOlM1Z3ydVEAUKDBqCKJiSKEwpkjiviuKkIsGkLgpHsqo0pcvyUe6PnPs3HX+v6dMBl7JtHhN2uD7dTBjbQijrDfa4E4DpiiRmBAEETZL0gYJqcH8kRYZduGNIhael+RpRa2AyQst6aKExdnlPbvtysTSwKbBZH+oDKhSjupzDJoztMmC7nOKpwvvnG5MZRfn8xUXtJi6I0FkwGaFHGAG4bRDg19NQK5bP2FjNLtuOf0wW2d1dmvrjnrx2hyoJ09zBMTBecV9f88g1oiA8Jq9L/dtyih7H81yPkSacGMzCruSdOQoLJiP0sLmac02V0q9fUjISv9K2QwEmXFi4WIdeLpgQR12Y3i7DpjR0lY5WXbPpeN+7rD/7O1wQdTzAZITOOOzC6B4VBpJYIRWHkJmmW3E8/4GCJn9/Z8n4W+6ghJuoOy+vWv5vMgEevymnbe815ETuNTRlM3eLLmhcAHU8TEYIji7Y7zcM6XVbM4qaxNo46sDMbq1zlqQZduHgFhEGkzjna8EmZKpu37W3L3s1F0QdDZNRhxvzwblIhsQkofGG68zVzeOqIP5gaz77xa6sdDd3UAcZmW++3/bo8/MZZWBXUc8k5cqDASRjInxpD8B1XBB1JExGHezogvm23T2ZdyepBnBhzuUNu/DQkAqXLRuMqYoLpKTi1uboFExGHWrEhuODOuyM+9UHz4MmKs7RjCZ/f1dJ/QvuAPQIc03nuZM1+zWlrP60HQUt9i2lGZt5m3Qhkd3DqLUwGXWoqkNJURNj/8xhxIIjgwbs4QJoRcMEvj8kwW+sdEwcVB1CT1atnzy6P/dUfEc7GyajDjPiw+FBGS6J61UTBnBwrn5CkYR/H+rJvZU7AK3JlOW9bKZivnprKfuEfkNW4lp7J5qsvDMr4O6xHQyTUeeJ9Rs+7oC1XYPEPIhPihEHjg5qcHFcT/dE1Wk6Ptw21KO9kAuijoALpXaIEQa3xTURjdYda/9s8z9tDwYwEbXHoLa0aKkwsmj904PTtcm4nd/OopYd6tFeMGzCfVwQdQRMRh1igLBnxPVKbV0b29uffbauwBgXRC012G28wugvfCtYoSKOvGa970TDvgHf9c6D3XQdwCZAdQlitRznoTlzwSf+1/duLryCC6JQ1Fxr95wJ9+6O4fJPJzz4q50KvJ0LoNTCllHKHVyw71bEeCWigJDL7MdEFK2CahwlJeO2OJ5beaZyDVeIUg1bRik2bMLBoQxcGqcr3Hdy9sOPHeh/IxdAkRou2//mE+/pl/fm++LyTvgMgAqwRQWI5SrrqLWwZZRSx214j+LY2+J0dWM+uHsH+j/ABVDkhrr0F8td+fvj9E4E+zydmK+8hAugVMKWUQpNMLh+mwCfi8uVHS5b1T1dRokLoFh6YLL+wyu25mPTTTbiwYlBBfdCSjtsGaXQ9MT8M+N0VWKX8SWuEMXWFVvzTz9pQz0u53exxHZ6hOF6hCmHLaOUOdqE2d1ZiLzf/3jNrlea1g8eu6ULJzEm1OGy+deEwqsu7clE/nlacBnpUQVcVDXFsGWUIpN1//o+mfXE4YqorNmYiJJtT1fmJiGXORCHi5AZFYbLbixH/qHWwJZRSpwEeP0AwIejvpoTVcf0BOnbewry73FBlFj7ZxvH9/bnIl/l/YgJX7gkA9dzAZR42DJKCbvivSoOV+Jq2glMROmT6c/9Rxwuyq43H8sVolTAllFK1D1K8kp0W0LcN1U50ZU3/n5HTvtbLohSY6RqLg4WM11RXs+EC9Y2FdcwTBtsGSVcBeBx+6YqD0R9FUZ36QgmovQTipnvRX2RGfDV6Rq8iAugRMOWUcKdtKExoEM2qqs4ON+cU3T9G5fkpFdyQZRa90/WDj5mayGy1T1qHqMFRZC4AEosbBkl2LTp/l5RJEaUVyBms+OYiDpPcUvhy1FetMgYjFT827kASixsGSXYvM28Xj2auReHF60yiNK395TU67gg6ggeoduOl937B3v1yKYTnPThLQMy4BJTKYAto4QaqfpfAUaiO3nVaGIi6myKJE4IBf0HUVZCeb6Mo+tSAltGCTTJ4PqtEa09t2D7ZLrh3nV5b+ZqLog60vFF9539efntWUWMZKuSEQe+PqgBTrBOOGwZJVB10XxeVGctiiJgIkJn29WtvnOGiouRVYrpPIErQ4mDyShhDvtwy6N6MpEMa214lHapIq4PhjgXa9D74Ez9Pz0afk/LYJe2fcqBa7kAShRMRgkjmjSyFbknFPFurhCh0x69Kf9sIaJNhes1XJkh6TAZJchRBz62uyBGsoLyWKX69CGAX+UCCJ1lzocX75+pj4ddJ4Kq/D5XiBIFk1GC2DXzqijO1qEAF5WKP+ICCJ1jiwq3Kvn8US7QZpREOLIUtQQmo4QY8eC2y/syTw77bO+ZmP9vH+ASLoDQeQxl4GmHFp1f1F1Klz+i9bbltX58P5INk1FC6A6EnogChZ7e+awIod/pomS7tFu7ShCieX6EkgmTUQKMevD2gVy4688dq9jNA3ONW4d0iGwYOUo206Khzf05WbdDf06FWguTUQJUy5VdYZ+lKApwWV/uxVwAoVXqL0jfnHLADefzimumJh0mowS4or/08rDPsktlT+IKEVqjLRpod5+c/W67603x4etcIUoUTEYxN1zxQ9+rKBg9V9T1/VwAoXXo6u6x2l1vuZwW6Rp5aOMwGcVYhcHjhkryo8M8wwNzzZlFH/6ACyC0ToM56XdONPxqO+rPIYw9VLGObNLhm1wQJQomoxibXzSfH/bZKXrG2aLCl7gAQhuwMyeX2lJ/AsDlJWMPV44SB5NRjBm6+pawz25PXtjBFSLUGi0f6328av8HV4gSCZNRTI1a8HYS8qzyox7McYUIxZTPALq6jEi2UkGth8koplQRbhwoaKFtKX7/dO2h3QrgLHbUbsKB0dr7T9Zse72vc7jSqP/i5OS7ZAGETQBtH6mHwoGb68VXqG/McBN+NJSFp3MBhNrgsAVjewzYvp6/fNxhi7s0IbKtzlF7YMsohkZM+EqYZzVa82qYiFCY9hhwkUdgywPj1S8+ONM4Vnb88/ZJB7F7JxZHHphY/HhwA42JKJ1wo7QYspuNrZDJhXZitqLg2nModIoE01dsL14XvO5RF6bBh94uGR5eSoEwgIoPpCLIi4/b1j2E71C6YTddDE00PHtbTtHCOLP7pqr7rtxSfBwXQAihEGE3XcyM1Mg3w7xBMLL5JleIEEIhw5ZRjBwn8P6tArxFC/cWAdf5RwhFDp8ZtVnTJXubLtnddN1tlkv6KWO9FIRen/gyZZADWdgqi5JBPepkFK1H25QJ7dxqBGgBFztGCMUAJqM2qHjgl5RTD2KzqrT0D4Aau/OckuC2AleKEELhw2S0Ti4hA+OV2lUNV36jJEnbGKOirkiFoqFkunU5Ee0N7J9DCMUFPjNagxMOLPbLUMpI6fodn/fAXwB4aEiBK7kgQgiFAJPRCqZrzm8vNJ3n+CA8KavJu3Z1GQUppc2Jik3IaLk5CpT8tD+vfnFLIXsbdxBCCLUJJqNzDLvw814BntCjQMc/2g8WojzmwpFBDXCJfoRQW2EyWlpuxH3iRLn5fI8Kz+3K6EM7S3p4Q9pibqzuOPM164gsCD/Zms98tSevYIsJIdRyHZ2MRlwYHlRhkAug82p4lOYUseNbjQih1uq40XQuhc3HFut/WXOcJ23O5wdADWXVndQIbl0OLzartk8bjNG7dpay7yxo8v5OrxeE0MZ0VMuIMKCSgCOa22HEga8PavDC9F0ZQigMqV+brmr5T9g/XbvZJdTHRNQ+brn+pP85Pvd5y/V2p/UaEULtk+qWUdUHUpRxMdiw2QTYKIHvDanwvM66coTQeqXyh/r+qfn33TtVPoqJKBq6BIK1UN1738T8Jzrx+hFCa5eqltFRC2Z3G9DHBVCkTAJsRoQP7BLgrfhOIISWk4qWg+X519Ucb/FiTESxFCyf1Jiu/+7IbOOdnV4XCKHlJbplZFG4WhfhThyVkBwWYcyQBOw+RaEyKViKAJosgLCe34vgVzL4qVz0wOpVASfFt0FifxRsz39ZpVx9BSaihGEM7pusjh6cbny206sCtUfN9m6tWO6hpkvqhDESfOoyIujKOhMRnF7hXhQAelUwzuSm4J+G61ertnesanv3ffDHw5dy/yFafR0nsWXEABgmoXQYseFbgzo8v9PrAa1Ng0AlI0JRTMAPgUXBMUTQuQB6hES1jMo2+abjUxcTUXqQWvOJD03U/qHT6wFd2Bu+cn/+1vsnPkIYozkpGYkoYIig2T51yqaLK5WsIBEtowrAO4sA78AklF4+BZBFnJSMfumeBvziqhw8Ps1VYhFwDAlbTZCEllHNI28Wbe/P8Vcq3RgwODDbmB2r157Z6XXR6a775x92//v+sS/rdjP1z2B0CbRb9p34zD/9/Mirxqvm57gDOkisW0ZNAmZWWnpgiDrIhAXWNgNHLHWSYw1oXJyDbKfXwxlzLph9amfVR2xbRjXbH8VE1Jn6VKb//PjY+zq9HjrBtx8sf+YXE+ZcXvLxu36WYPj4u767fx8XSLHYtYxGAd6+A+BdXAB1pKMefHa3Aq/Edz89bAqOLoLa6fWwVnfU4aan5uFvknXWqxe7llFlqvYSrhB1rOpM+X+dqDYxGaVEwyUVTETr8+QM/euRudqRJJ77asSmZTTswM+GNHgyF0ghyoAF9e4zRghhHmHMFwSgjJ2e6M0YBQHo0pUzEINCQQjm3Aly8L9H5hszlDEqiqKcN9SebkPJdGlyqndfPdyAE3tysIsLoNizKbi6CAq+U621QMDpSdFIvFgkoxNl+wZREG4aKGmp7Tdmp2Zxt21QoEOBaSleZOfAXGOBMXLL5f3Fv+CCKLaqtjda1JUBfIfa4/hCc2pXT3ZrGq4l8mR03IbFXTp0cYEEIowx26OW65NJQYADiijcntWUv4viSo7PVV7U8Ng1HoVfzRv6xZvzWj6violPV2MmmBdlcNRVnJU9uKtLgSd1ej2EKdhD7L8ceNyzM3BfUq8h0mR0cK72vkf1FVKxrYBDwddi3BVx2IaxLTJsy8nJnli6YPpkomb/9IrNuadyQRS5muV9O6fLzxEFXAw3bN8fnnvo2UN9e5N6/pElo2MmzF+cgR4uEHOEMlp3vAXK2IgiCbflNTVxI/8m694flmu1bZYHz9rRX3hSX0aRuYNiruoyWlSFVD8nSxKXgq+KgO9HTPy4Bp+6ugCvSdI5R5KMjk/VXwmG/NFdJSNRExsbPrBcynaPHfHggT6Ay7qUZP2QVBxCJ+veg5f16ldyQRSqqu1NFXVlM9Z6vNRs7+sFXXlhUs439GR03IKFXQZ0c4EYCka9NRyvMlZufuqintyXC6p8fxLOe71G5hvvbNj+s/KGdtnWglbIKvFfivLYvHnjxb2Z93AB1HbBaFAZu+Nir+zBfV0KPDbu5xlqMipb/o+7DPkpXCCmGgTqOQkKSTnfVqp5jBSU+P/Q/Gz/sRsftXvHD7oM6W4uiNrGo7QiCUJBFNa9RRAKiUeo7/j01pwmXxvnOg8tGdUJVPMx/2H3CCMNxxsTRfFfi7p8A3dAh6lZbO/x2cpbJEW+ZldfdlvMW0r4oxgC3Ess2SoEvlOS4DfjeBFhtoxivVcFbmGwsgS0lPC9azOf0IosicVUX2TKWT6xDFmK5bP6ju7vPVo2mz8ZPvnp0XLjekxEKysogjRd9373f0YX/vPwvFlb8eAIjFS82+J2TmlBGHjBzSQmouQzZGlp2/RZm/pxu5i2JyNCT+1BzwViQNIM9ylDA3+8oyv3hTieX9xszitffvyOnmdTIzMat3OzbZsrQxvnUTYrCZC4of9oZZRQ+Op9U59Z8aCQtTUZEQZUEuPVtROsknDvWPlHi014ys6MkIhRfXEzlIUrgm6x+8amb5xsuF4cTk9TlEdzhWjdKgC3BjeRiij0YS2mz+asLL3oyi2vKDuExuXi2vbMqGp7h4u6cgkXiJhJwMngNr8tM+LB4UEFIn+fD86bM4/qzeBcl9aJ9TNe1Dp1x6vmNaUUdZW2pdVSITARt0TkEWp6hP4KJqLWGlRgj0dhywPjlS/sn65PR3Ue9FR3MNqgJoEmJqLOkteU4FkgC1bRiPLCW56Myj58UKV+rJb5Cb5ZviR+QJHE/+aCaMMUEaav2F66XsnlZ6KqTSH+83MTISvhdu+dKurlnFr6YJICXNElwxtb/Gc3xCP0PYok3oh7GrffUA6u9Ajb/MD44hcfv6Pn18N8bdc0vwL4O7oRqWsN+ZQR2yONedN1CGX3FzX5rt6c9g7uwDWYbzrvcTy6mVLi1S13qCuXeVJ3VtU0OTUDkxllsLR5Ghdps5ZmDdclt+pqrJY4ExQJVysJkyIJ07t39AQrsf9PmK+byWIiWq+K6Xy7lNGSefLLcCkQVQRZFgXIafLSP63Sm9W4yfDTHnzateBZ23XYmYYGelTX0OoBDLG4uyKUuZIopOfblWCWz6ght3fJmLLtky5dxuHHa1Qj8JGCBK9L1EmfhTBGHc//ZEZV/pwLRmzRcm5aaLq/VzLUS/uyLcyGIQq7hdTKZBSLRNTuHVXR2ozYcHhQb+9ou5rLSEEVMBmtEWXMF4VkbsMRLGI868E3NqsQ+1WpRx14aIcGl3GBZAjtt7QlfViMRZ+IghNwCJ3DRBQvgzrsCT7QY3WnLbNS7xmdP1Tz4fVcAJ2XS+Fw8JVJUiIK1sSrWt53Tv84BuNVxCQkosAODS4/c97zpvveYwuN2artxWZ+zwUEz5BC+X3fcMvIImzUkITI97j3GHiKACoXQLEwTOG/hkRo+e6sw4vuoaFu9VFcAJ3X6W7s2O5KfK409nbMEPj7bgH+WEnOI+221/+GqyLqREQZI5ZHv4+JKN6GRHha8IG+69jUd09WW9NKIgwAE9HquRTKwW97EhIRocyjjAWDYIQ09nZskuBPlFPrYQpjZfOWo3O1BZ/GekAju2MR3s2VtlDih5q5TKgaivgcLoBi6ckXb3meKLbmY3dUgNu5QrSs9905lVdESMRCp/TUMmKqKAhP4IIpdFFX5trdfYVeh4IZ56uTa7U/4gpbaN3ddDYDVxcgsjssyhh1Cf1/uixhIkqYJoXdwyfGfqNO6TOfesmOF6z27GebnnditnJIl8RvXjHQcxN3AFrWPYvsF1d1C49fLhYX7FRr6LgsChfjuxiMxnNJt6HGtbHQlpbqRp4ZRdqmdChUNREiX08Jrd9w3fnaUF5bdTKat4jXa0jYHbsGPmUHZVG4NO7nSQC+KQH8NhfoUNM+fGGzDC+L49V/9b6pz77oyi2v5AIbtK5k1PTBysrRrfHmUdZQRCHPBVAi1U1374mFxrUuYY/WDe3K4CNJGfUJo67jWPfkdeW+gZ7Cd/Oquh/f4dVzKcypIvTG9fx8SmuyiHskrWTBdN/nU/aGTTktVs/5fjptDf/aZqOlNznrSkauT6kqRzNP12fAZKGzNwVEaDUoY54oxHP+Fc4HXLNYjW6wfAJzJvniQEG9jguu05p/1IM9iqJKRDahLiYihFZW9uA/T88jil0iIpQ5aR0h12ZCnLbWN2QJBgrqH8y68LdccJ3W0zKKMkPjBxihC4hriwhbQxs3TuHgdhFi8wxwdLFZ3dGdbcmz+zW1MhwS3X4Xnk87YpgnQuvlM/huHFtE9NQdL7aGWmC7CMG8OuHwXH0/adPGqGuxoztbdEhrGiirbhl5DJ4nMvZtKYIeOpOCkxFxUzyEzsck9E0iY+/X5VguU49JqA2qLnhFNTb79Wz4PV71B1cWIJJEVGl6+zARIXR+DQZvz0jiB+KWiEyXfhQTUfsUVVBcQv9womo6MTidDbeO1vLMKJI2ocngKRkBfsoFEEJLarZnFXQlVjdsdR+qeRnnAYZhyoPPbFHgFVGfx4lFa3Rnt7GTC6zSqpKRQ5ivSeGu8BucVdmlP+xWxWdwQYTQ2V+V2Kharl00VNxYOQKTVcvcWjQirfuaD39fkOFPucAqrKpZH8Vw6iBHYiJC6PzKpjty3mBEFEN9G75l0dhaNCLf7rhSN3+fK1ylC7aMfAZUDnk/dBwCitDKyi7c06VCbEaYLjSsQz05A1dQj4HJqm1uLeqRtZAqHvglZe3rlq6mmy70bgCPgaUIEHmWRyiuGo5fyWlyLJbSwZvHeCEMXioJ8KWIT2rNn4cVu9+qLvyCK2wzyyMeJiKEzm+84XpxSUQ4fyh+JAFuCd6XiaoZ2ZYUls/WvJPtislIYv4QV9hmniT9d9iviVBS3Hzv8Zu259RYzC3xQ9qOGq2PaGT+JaqqO71x4Jqct5vOAnjQANjLBdrI8qlryKIW5msilBT3zNgTV23St8bkdLE1lAALpvcuTRbfllOlqN6vVb/ueZORQ4Fo4sotp1ZrALwjB+3d2hahJKo6/kJRk7vjcOoNCmZOhCwXQLG04DCrRxOimoe26mR03mQTdiKqWv4YJiKEeBWPjcclEZmu92ZMRMnSowlG2fYjWVe06dFVPzuKzfIhWV3+Xa4QoQ5nE/pygXix2CCvwcDOqMoHuQCKvS5djmRzPrb6XLR8N53lM8eQhdC2dzY9ZmUUAUfQIcTjv6ARcDzy+5oi/Su+P4kX1efpgt11y7aMRGChLv1DZOFDXCFCHW6hYd0ZhxpYen6MiSgV3DUPuA4Pl4yqBD6vyWJoyahmebN5AXAJEYTO4lDwe3LG1VHXScX279XE2GxTgDZIFUE4Mt8YD7seGy4lXOE5uG66psfMrCKEtpSESeC1GQk+wQUQ6myRd88RtjSBEodwp1Oon6+GS1hOXXmLEy4YZiLyKWOYiBD6pSrAzXFIRIsN+3uYiNLrRNW+J8yLC+Y5NX1YsZOQaxmF+UVwKMxqImziAgh1KJcwokoCd5MYJlxrrmNEcdNz3s/VIz70FYc9wB3RJkGrCBMRQr9U8WAs6kRUNp0KJqKOEfr7PGXBJ7nC0x7xwZcZGeCOaBMWs03BEIqST+E3MwKNfKkfPaO9hStEqRX2j7BlNp/HFZ72iG46xoAJIeVK06V/k1HFm7gAQp0p0puzhuO5OU3BdSE70HjFNLeXMmHuf7Rslnm4ZWRSeFOYi/BiIkLolKpNTkZdFZ6k/JgrRB1BVcNdnGGsTg5whWe3jGoEZgsS9HFHtAEOGUXolCqF6WKEz06DZ7cNh9xRMuRruCDqJGG3zLnf/4eTUZjbi7sMTFXAxRYRirp7zibM1yUhknXLUHxQBkwMt3nAvdrD3XRhJSLL8wkmIoRO/QBEWQ2WRzARoSWiAMJkzXLDqo2xOvneuWWhDyMVoh25ilAsLDrw85DvRDlEkT7AFaKOJUshLklKvF85t2gpMxDGfoM7uE08SsthvRZCcdWtAfdlDJND4NocwA34AUFnlHTlNWFVxtai1nVu2VIy8jzyBO7oNqGifCSs10IojhZq5nejPK0GAUuT4BYugDqaKgmfDfP6qy5709n/f2kAQ9Vy9xcN9XLu6PbAUXSoY1VcOFxS4ZKorr9suce7DPViLodlEv4AABm3SURBVIBQcKNkU6dHF0PZy67mgl9Q4eFnlqe66S68ujdCqAVyMtsdZT1iIkIrobL48RXCLWXI7BFbkywlI1WWQ7lT8yguAYQ610+n3J/LYlhrnDySRyjzGfwWF0DoLH0yvHm8YjfDqBPlnBE8S8lomZW728KkEPqmTgjFQdkhtz95sxLZoIUmE+dlAb7NBRA6l6RUubIQLCWjjBrOmlSCAN/iChHqAF2a9IyIGkVQc/xKSYZ+LoDQMrbnpW18aXuM1dyJM3841Ek/sgjHuUKEUm6+1vyPKK+woMncMFqE4oAS/+GG0JnlgNreTxesgSWLOOMVdZaKDydKMuyI4qIpOzWzngsgdAF1j/l5RWj7LFjLp2DIpx4eiU0CFndEG+A3AnWiYkSJKNAkEMqDaJQ+i0S4O5SLOmu8gqiJEMrzIofQ0NY9QigOPAo0qpuw+Ya1Ly9DjgsgtAo7dPi1habrt7uuDEWCcRu+Efy7GNYCqaIg2FwhQilV9+HjksAi6xDozhm4YyvaECGkxRNlRpa2LwntGQ6lbNkNlRBKo7wMr41i9Fwwn6jqki+IALdzQYTWoGpbt4ZRXz2alIcwk5HHWNubfAjFQbnpRZcIBNEqqtL1XDlCa6RpmhdGnUmn79lCS0aiIODDVJR6NQr/2JVVnhHFdQYjVhUR9wpDrbE1o4VyUyOGnYxAgFCyLEJRkgl5WVQvX6fCPVwhQgkR4rwfIZQh5AhFpezC/RlFCmXF43OVLfehLjnaPZIQ2ogQW0ZCnStDKEU0II+K6mq6DHUvV4hQQsxb5F2hJSMBwOEKEUqJYE5RRpWUsK+GMhYMI/8gF0CoBfyQ9llwKLwal+dBqAUUMZpFRlwqkLwMb+YCCLXAuBXOs36BUT20ZMRACGWlB4TCRlg0+3QFo+d0CWQugFCLmD6cDKMuS7pcwJYRQhtQcdlXhIj2jGxQ4SGuEKEWKqgwHEZ9BsO7w2sZMRbZgpEItUtJFV4UxUoLZdM9WZLh0VwAoRbKSHBvGPWpS0J4WzowxrA7AaVKxXR/EdX1dGVUvLlDbScL4ezqACHPM8IVhFFq1Bh8opRRHx/29RDKoOLS73IBhNqgIMN7wqrXYHO9UDq8qy4dLqripVwAoQSyferpshh6ax83qUQRCCVHhLc2HbAerhChBCp7cHsUiQiWtu7HRITSKbxJr4zh0G6UDp5zVRTX0SSAe4Kh1BLDGpTKWHQbjSHUKoQB7cpohbArdK5uHs1KYHABhNqMhPMkB0SXQij7DIX0aAqhtpJC2hn5XIph4IrcKNVEm8EMDSFT5A0VR9OhRPMIo1Gcf9NjTkkWXsIFEGr3Z8+HP5NCmkcnFiXYzpW2ic3g5WG9FkKtVHXpp6NaaaEpC6/iChEKAQPQw6rnpQEMHqFtXwwvyK02gRdyAYQSoKiKr5IlMfQuurrlfqxfgJu5AEIhaHiwJ4zX8Sg7lYzC6gbXBXgyV4hQzC02nVCWRFlO3lBft0wxQqHwCPxaGK8TLDa8lIwczz/GRdtAE6EXP0IoSSo+3NWd1R4b9ikHKy00fHgXF0AoRHkVBsN4tYpNGsEKDNBwvIM5TQlrdQQc4o0SgwZ3bBF8Ym3CiC4JuJ4jipTtA9Xl9v9mTzTp5KlJr5T+gIsi1OFMH6pRJCI4tYoxJiIUuTASUaCoCB9cSkaiKNa4aJt4hN6IHzEUdw0f/lIRaD6K03QoEK4QoRTLqcJHhLOmGIUybtX2aU2XxSIXQCheIhnHXbM9q6ArGS6AUDTC+h6Ev+gipRSXNEGxttgwb4nq/Iik3MkVItQBHm4ZMQYsxA0rcRADiqWyDw90RbSDqu1TEtVq4Aid62Tdv3cgL7d9JGkwx0gRz9rp1fTB5I5qE8tnI/jOoziKcitvRxL/mStEKCLEd/vCeOVZmy4ts/VwMvIYfI87qk10WQhlVi9Ca+EQRqJqslebzj8UBfgjLoBQRAaKRihLxdUcOgVnJ6OSCr/DHdUm2EeH4qZik3+VIJptTigDKGa1P+ECCEUorJELpYzyX3Du5npeWBtXBP3jHsH1tlAsWJS9uqRLL45i7bngC1f34QtcAKEIjZpwpxzS12GLBtfCucnI8tkCd2SbuJ6HSwOhWKjW6qH1CpzLo8CKClzPBRCKUFGioa8j+ohkJInsvdwRbVLI6M+pAHwnrNdDaDkMgG0uFX59mVAoVDG8rf8RWq2SFs6oTo+Qa8/8+yO+CFlF+jB3dBtJPjwlzNdD6FxRPr+sEZjjChHqIGfvGMHdlYU57TwvQ4ErRCgENQb/GNUqC4GK5VUKEvRzAYQiNrJgHQrrDGRJfHiCOZeMmj6Etk4dQlHRCI10GLWiK3/FFSIUB8QLbXfXs3HJSJHgTykLb1Sd5VGHK0SojYL5RJoscp/9MATfrLJNf5IVINQucYRWY96FDw32F3ZEUVncF1IT4BafhpeMJAEUj8KLuABCbVCxyA+k05tKRiGYat6li1fje4viiPnkNWGd1owF9tn/f9kvpe16n+IK20SVRYEyuDWs10Odq+rDP5UM6emytOzHPhRSWHv8I7QOfRlJC6PegoneWZndcHaZsEKP3HkDbYJfUtRWVcu1ioYaSX94wCTgZiQI5cuO0DqF8rsfJCPxnBuz894iBqt4c4Vt5FFGw3w91HFYlIloseEcxkSE4qzm+KFt6nhktvazc8vOm4yqPpzgCtuJMfAp4C6wqOUWm86RqGu1O6cNcoUIxQRh8FJGw2t/GIZx8tyylbrpwPUpVcNaoOiXsLsOtUyw3I5y3luu9gvGAtVc9tWSJka25BBCF1J3KcmroY4w5X7nV3xxQRB8rrDNCKUvDvs1UTpVTecLUSYiWEqGAsFEhOIu5ES0rBVbRqeF+uxouQdbCK1VjcBCQYLuKCuu6fhuVpPxORGKNcun1AipB6zp+qzu0k9uzqmvPTd2wWwYYjfiaQzqDsE1u9CGRJ2IAr4of40rRChGXArXQ3jTSpfWolsuEcFqklHVgzu5wjYSBQHymtTrUPh0mK+L0qF66nMT2ZpzZ5Qt7xdFBbDLGcWaKsLnDEUKrSdqsWl9nCs87YLJqEuFp3KFITAt+39H8boo2WTfvy4OF9BlKFdxhQh1uHxGnzpfDazqoVXZsu/lCtusK6v3+gxCH0CBkqnB4J1BiyirypHNJQo0HN9bbqQQQnHjknAfwixtr6/J7+cCp60qGXUZ+uO5whDIAkhRvC5KHuZ4b4nDSfuS/E2uEKGYObZo7gu7M3vSgvO2imC1yShge3Q6xOdcZ2OLANhlh1bC8rpirBBvO9sjtGx6d5VkwGHcKNZqHtxwcXfmSlUObzT3galKc3sGtnKBs6z6bHRF3BJV50ORwZe5QoSCL5blxmL/LUEQoSuj/CoXQChmZhZrfxb2GWn6hXvP15QaTdv7HlcYAkkA0SFQjeK1UTzNMrg+aBEVDDUf9Qk6PqWaLGCXMoo9hwDbs6mwJezz3N2l57jCc6wpGWV15blcYUgkoDmXwoeien0UL9kQtzm5EFsUseWOEkGL4JZptU931txpWLH8H5MQN987Q5ZEURXhDQ6Df+aCqGNUKPxoadScpkQ6au6MquPfUhRxPhGKtyqFt0Yx/87xafB0Z1UPeFazHBCHUEalYHZqBFyfuqos4hIrnSvyCa1nmD64GRm3hUDxV7N9p6DLatgnanmMGYqwqkbPuoZTEEqXXc4hDKosqixGP0goHA0CzTgloqrlzWEiQkngU6BRJKLAahMRrDcZqbL0SZdAaBsxnStoklkuWeACKJUqpntvToJMnK6taCj9XCFCMXOiYh+SxWjGQa/1znHdA81VCeQonh2dYahSt+0zlwugVCEMWCmjPjYu19R0fCftKyxM2vA5iwA9/XvC/TNjQXPex8FEcTdlwZd2lvShKE7z+HyjstpnRWdsaNZTwxNmucIQqRIoddO9K8pzQO2zULfHwlvCcXV8Wf5Kmt/yw5MLN9v1xosM6fw/JJsMyJQXa9cdnlq4mQui2KjXas+M6lwUVbO4wgtY1wCGs9kesXRFinRkk+0zT5eFSPpEUeuVfbi9S4ZnxKlqfcJY04fPFDXh1VwwBcoW9boMUV7PlUzU/Mq2gtzFBVAkmgT+LCvB30X1+g2HsJwmrbmhs+H1IHRFinQZFlgaOy8oDdsb4QIocSqWe7fByDVxO+8mESbTmoiOzjSmNImtewaKQD3j8GT5W1wARWJqofruKGt+wZV+whWuwoZbRrD0gNk/UTTkHdEM9v4ldqozH1dMTiAH4CUqwL/E7c3zCGVNx7+3lFGfwAXToWXPfedNz+vNKNhDERGbwJ/oEkQ6GXyyaptbi3qWC6xCS1bKK2XknSwGo26FpQfMXpkLoNizTOef4ngX4TOBpjgRtRQlBA5NlUPdjBP90ny1+baoq0PS9c9xhavUsmVbTSK8I8LBdQ/LakopuNur+oAPVxPAZ6dGbZUyWuTdvWcLPsoVy100ZGFdz1Hi7qQJx1s9b6s/ryuXbum6Ovi7LoNXcAegthi14IdBnW/vzoa+5twZNdtjx+bqX96kwbrnoLakm+6MqguzRRX6uEAEyk27LorSl4uG8kdxOB/0SJSxp08vVF69tbfrd7lgDAQbgYlCqrt823rneGBi4cBl23ou5wKopUbr9OD2rHCpJEb7Ua3ZPivoG9uToqXJCJaWf6COoYix6Tf2KFBFxE364sTywTZiunpB8Iyo2nSO9BaMQS6YEoTBSyUBvhTG1ZxswE8GcnA1F0AtYXuU6Up4+xItZ7pqeX0F43pJgFuWCa9ay69CUMRYjThSRBDLTTvS+VDolIbrv7dquVZcExEs7UskQJoTUWBsvhHaBoAF0Xvi+ELjo1wAbciB8cV9dcenihx9490G5dhGExG0o2V0BmPAoh5dd66GD2ZOhnWN9EAbE6ykELcJrGcLWkSVpn2kr5BJdSIKNF1Ks2r4/To1CjcWRHgPF0CrNm7B17Yb8IK41NiB8cWfXba9+9e4wDq0LRnVPJgrKNDLBSK22HBOdue0HXE7r7SqWO4RXZZ26craJ8GFyaeMyeLqF3VMsoZLaE4N/9ag4fgkp8mpHBASFsenTAtxu/BVaNnnqG1XVVCgr950ItkZdiXdOW1gabSdC9MrHIY2qOFDfWmUnKHujnsiWmw6lU5JRIEoEtHS62py8OyWeRRX3V+LWRf+9sz6gHFJRCcXzeZ03W3pnKa2Xlk+qz03rp86TSC9czXzGBdAG1Ix3W+Um041K8EFtxmOg+Dz2Z3VcCmbEAXP24cnFn7WMRe8AQsN663NWu1P43ZeiqrA5rz6Gi6wAW3rpjtbuW7+d1c+cxUXiAkccbdxNQ8WCwok5kfd8Qmtm87+3kLmMVww5Y7MNWYu6cvFYguMYAj9J8dh02svAhxkdJYxE26/KBOv9RkDpkvYRLl5155NhZY8JzpbKG2+rnzmiVxhjAQj7hqO5yw07f1xPs84WmzYd9Yst5mVWKJaF4QJXicmooAUo17ToL/wWXJl39hC/ZNcsEMdm6ne3K/S2CWiJYIA7UhEEFbL6Azb9W1dlWO/O6ZLgH7ZgqtfmgPsSjiPYPfIqDbt2ohgoELDIUdLhrwnaefeKlXTeX0xo304jufmECCaBB03yGHOhw/kgL3RkOM2BvmXxhcta3u30bZNLsO9RZLkfVxZDKkSiC9QvB/N1pp3277/8iSccxjKTeuz83VrzPWpn8REBKeWrVro5EQUKGa0j3CFMaFJIE1Xrer4YufMTRqdr33arDX/JM6JKCAZxn9whS0UassIllaWZd8RGf0NVZYS9Yym7kMlLyfnmUirVD2YLiqwKenX0bA922fC10uG/BIu2IFGpsrf6i5kn92bVWPdCgmeKc168KnNKrT0YXnUxk34xvYMPD9Bp9z2RBl657EuCc8bs6V/4wIxl5VYqW675lzdvLvuuH+VtPNfi7ppv3u20rizZrr1vMwSn4gCRFG+ionolwa3dP1W2Dei6xFMzTWI98pDkwup2C9ppmK+59BkZSwveL/JBWNq0lqaptF2obeMzqg63ucNSbxWlaXE9g/7DFjDh4dKCjyaCyaMS4GoYvg3J+222LTnurN6LEaOxc3oQv1zO3ry1yftvD3KQBHj3aV1RoPA68s2vPWibGJ7F0Kr58iSUaDiw10lGZ7EBRJkafM1261SEMxg00xDlW8xFOnTcb6CuuW82/M8xfPJllIh91ItwTcEK/EpMDmFCbbFEjcBNei68wll01VzeqAnt5U7IAZMx3vVifn6K0pZ/TFbS5lEbjg4azGr3xDaNmDhXJEmo0DDY+9VBfYmVV7f/vtxFozKu/EQfPgDl8ObozrNo3WYHMjCJqWDfpRN1/ddnx4oZdSOHLq9Voeny9/qzmef05NVEz/XLmg1TZvC9EU5CGVvnwkPvur7cGkGYHefEd8FgNdisekS2/U+sbUr+7owXzfyZHSWVC4RElSvTynzCPUcz29QysoMYEYShaooCLOKJO4XBKEui+KsIotf4/7AWTyfvJAwViSEdgeDWxiDvEvIXgbCNkapJAhCSRCEoiyJGU2RNEkUxaAwGR0arVPx4eclGZ6clusJQ8UmfklP1qCi8/EpC9ZwI03bMy3Xm/N8ekBVxMmioR7ayEhC2/Ovbzpef9Pxtvg+URzP3/qo7X2xWbS0VU7W6OxAQQy9WzFOySjYMryW1ZQ8F0DoAhyPsJrpDPcVM49a+Uh0Pifnax8d6C38xXnCKOXm6rbfl9eVqK4yVsnoNFxEEa2Z6TE3owip6CaJUtUFUlTxOVsnGm3QB3bkxMi6tuP4oRPm62bsVvtG8VQzXcd06ZsxEbVGUV1ao7HDOnY728hUZSF4z6NMRBDTltGS05NMi1wAodOCT64L8PsawL9inbTWqAnHdmRgV5quCS3PJPC6jAQfWzYYotgmozMs17cMVda5AOpYDdtzCbCvFXX1xfgpaK8js/W5S/rzsdskEy2PMAbBFKwLNW2D1ben6/bMxT3ZUEYdrkbsk5EH8EIFoK1rIqHkCOaYiBf+rqHWwue4CUFO/55LFxhCO2tSpz8jxuomP/YPKhWAYLizsGja/141HZM7AHWEStOpVy33i5iIIiFMV8z3jkxX5zvw2hMlSEIrJaJDk5XxhuO/IW6JCJLQMjpbhcBXShK8iAugVCMMwBPgZTrAzfhOR2fShJu3ZuAPOvX604AweJkkxPN7lKghnCUJfmepldQw/yFYhoc7AKUGZQyqpltxfHqtJICAiSh6WzNwXfD9G5+vvnd80bQ7vT6S4uB4edRyvT8M3ru4JiJIWsvobGUfbu+S47ctL2qN09uY92B1xtNojT20oyBc1un1kARzFnywz4huSbLVSmwyOqNiecd0WRyQJVGUE7KSL1pe0/Fc12eHu7Lq3mUPQLFzbL45VTSUPkORxIwq4fcvJh4cm3vw4v7iJ7Ka+o9JOefEJ6MzJuq+tS2PQ8CTyqNAFRFSsTZaJzJdQjEZxUfDhzfnZPhgks45Nct+bMvLRtMlbyw3nVnbI4Q7AMXSXLUZrLYhYCJKtowqiWMLjU8dnCiPewQf54Zttu54B8YWHqyY7v8Nvk9JS0SQppbR2Q7X4Id7CnANF0CxEQw/qQEcKAlwOb4r6eL4jGkyNpLCNNXwm1tyci7J15DKZHRGueneAcCuzBtqQRbxyxGl4GPWdDzH9elwd07DfYY6wKGJhW9pqva0XX25RP9IxlUwoliRxNT0bqU6GZ1R96CaV6DABVBomj6YWRmyWOOdZbQBP9yRw16KVnN8BnUmvLdXgRvSck0dsVR8XllacFWoW+5HZ6vmSZyj1H7Bc7uFunW0ZrqfDOoeE1Fn2pGDp59eBVw4PLX4xZGpyrRLaKdXy7rMVK360eny54O61GRBSFMigk5pGZ2r4sGDJQVw+HCb2D54ugyJ3PcftZ/lM2rIOA1jLco28w1VeI0uQmKGaq9VRyajs5Utd0yTxE2yKEiqLOGmYmvk+pSarmcSwqZURf5GXpdjP7kOxcP4Qv2jDoWXFQ01p0iSmNMkUcJnu0vD5MtNu1Jv2vfs3FS8VVfkz3MHpVDHJ6OzEQYMZ0qsHju1UsJYUYGBpJwziqe9H7p/5v7XX9GPyQhg0fK9bkPuuJ4FTEbLqJjOF1zPf6wiS7tyenDX1tnfkOARm+tT4nh+nTI2oUjSD/OG8ufcgQht0Hzdumm2Zj5LluQ9vXmjrzurpnr+2ULTJQsNa45RdnhLKfu1gqF8hDuoQ2AyWgXKgHVqOgo+HaYPdlYGgwsi1EYn63RmIC/2p7mOTzbYiYGc0PE76gImo7VxPPKShu39IQhwqShALqerhTSM8/cpYz6hzPb8JiF0VhCEB/K6+kVFFr/GHYxQRCbL9XdVms6vCqK0R5PlYimrFbqzauy/f/MNx19sOIuE+MNdGfVHm7ty7+AOQpiMNqLiw6GMCJfIAiS2I88m4OkSjnxDyTPVII0tOSnWUwZmTDAtCvt35uBXuCB6BExGLWZ7/stt13+aT+iAIIoDsij2yJKYUSRRliVBbOfC4sFbSRljHqGUUOa5HmlSxiqM0UlJFI4pknQyoyt3ioJwO/cfI5QSs9XmTZWm8xiPsl2yLG/J6Wp3TlfUoi63pBUVzJOiNBiiTuhMtTkqAhuWJdkuGsr/9OT0v+b+A7QqmIxCdMMwvOvG3fAWTQQt2D67Fa0pspSAgHoU/HEbHhrMweO4gxDqcKMNOKyLbGtGBj2rBDeFa6sPjwKYHiNNX7B8gJmBLFzCHYTWDwD+PxUNROPjl5zoAAAAAElFTkSuQmCC"
                width="100"
                height="50"
              />
              <h3>Autobot</h3>
              <h6>Scale your cloud application as easy as never before</h6>
            </div>
          </Col>
          <Col sm={8} xs={12} className="add-app-col">
            <Modal.Header closeButton>
              <Modal.Title className="add-app-modal-title">
                Add Application
              </Modal.Title>
            </Modal.Header>

            {steps && steps.length > 0 && (
              <StepZilla
                startAtStep={0}
                dontValidate={true}
                steps={steps}
                showNavigation={false}
                preventEnterSubmission={true}
              />
            )}
          </Col>
        </Row>
      </Grid>
    );
  }
}
export default AddApplicationPageN;
