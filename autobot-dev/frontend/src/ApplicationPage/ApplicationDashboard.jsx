import React from 'react';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';
import ReactDOM from 'react-dom';

class ApplicationDashboard extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            render : this.props.dashboard.render,
            iFrameHeight: '600px'
        }
    }

    render() {
        let renderValue = this.state.render
        return (<div dangerouslySetInnerHTML={{ __html: renderValue }}/>);
    }
}

export default ApplicationDashboard
