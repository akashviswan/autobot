import React from 'react';
import { connect } from 'react-redux';
import {ScalerTemplate} from '../../static/constants/ScalerTemplate';
import {FormItems} from '../../static/constants/TemplateConstants';
import _ from 'lodash';
import Select from 'react-select';
import Toggle from "react-toggle-component"
import "react-toggle-component/styles.css"
import DynamicForm from '../Common/DynamicForm/DynamicForm'

class SimpleScalerPage extends React.Component {
    constructor(props, context){
        super(props, context);
        this.state = this.props.scaler;
        this.handleTextFieldDataChange = this.handleTextFieldDataChange.bind(this);
        this.handleSelectionChange = this.handleSelectionChange.bind(this);
        this.handleCheckBoxChange = this.handleCheckBoxChange.bind(this);
    }

    handleTextFieldDataChange(key , e) {
        const targetValue = (e.target.validity.valid) ? e.target.value : this.state[key];
        this.setState({[key] : targetValue}, () => {
            this.props.updateScalerObject(this.state)
        })
    }

    handleSelectionChange(key , updatedSelectedValue) {
        this.setState({[key] : updatedSelectedValue.value}, () => {
            this.props.updateScalerObject(this.state)
        })
    }

    handleCheckBoxChange(key , e) {
        this.setState({[key] : !this.state[key]}, () => {
            this.props.updateScalerObject(this.state)
        })
    }
    
    render() {
        var template = ScalerTemplate.fields(this.state.object_type);
        return (<div>
            <DynamicForm datasource={this.state} 
                         template={template}
                         handleTextFieldDataChange = {this.handleTextFieldDataChange}
                         handleDropDownDataChange = {this.handleSelectionChange}
                         handleCheckBoxDataChange = {this.handleCheckBoxChange}
            />
    </div>);
    }
}

function mapStateToProps(state) {
    const { users, authentication } = state;
    const { user } = authentication;
    return {
        user,
        users
    };
}

const connectedHomePage = connect(mapStateToProps)(SimpleScalerPage);
export { connectedHomePage as SimpleScalerPage };
export default SimpleScalerPage