import React from 'react';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';
import { Header } from '../Header/Header';
import Footer from '../Footer/Footer';
import ASideLeft from '../ASideLeft/ASideLeft';
import AsideRight from '../AsideRight/AsideRight';
import SimpleScalerPage from './SimpleScalerPage';
import AutoScalerPage from './AutoScalerPage';
import ApplicationDashboard from './ApplicationDashboard'
import Toggle from "react-toggle-component";
// import  DrawSplinePage  from './DrawSplinePage';
// import  WeeklyData  from './WeeklyData';
// import  MonthlyData  from './MonthlyData';
import { userActions } from '../_actions';
import ReactTable from "react-table";
import Select from 'react-select';
import _ from 'lodash';
import { authHeader } from '../_helpers';
import RangeComponent from '../_components/RangeComponent/RangeComponent';
import { toast } from 'react-toastify';

class ViewApplicationPage extends React.Component {
	constructor(props) {
		super(props);
		this.instanceRef = React.createRef();
		let appObject = (this.props.location
			&& this.props.location.state
			&& this.props.location.state.application_object)
			? this.props.location.state.application_object : null
		this.state = {
			application: appObject,
			metrics: [],
			loadbalancers: [],
			metricsOptions: [],
			metricstoreOptions: [],
			loadbalancerOptions: [],
		}
		this.handleLoadbalancerUpdate = this.handleLoadbalancerUpdate.bind(this);
		this.fetchAllFormData = this.fetchAllFormData.bind(this);
		this.handleLoadbalancerUpdate = this.handleLoadbalancerUpdate.bind(this);
		this.handleMetricsUpdate = this.handleMetricsUpdate.bind(this);
		this.handleMetricsStoreUpdate = this.handleMetricsStoreUpdate.bind(this);
		this.handleCredentialsUpdate = this.handleCredentialsUpdate.bind(this);
		this.handleSubmit = this.handleSubmit.bind(this);
		this.handleTextFieldDataChange = this.handleTextFieldDataChange.bind(this);
		this.updateScalerObject = this.updateScalerObject.bind(this);
		this.updateAutoScalerObject = this.updateAutoScalerObject.bind(this);
		this.handleAutoScalerSubmit = this.handleAutoScalerSubmit.bind(this);
		this.onValueChange = this.onValueChange.bind(this);
	}

	componentDidMount() {
		const { applicationId } = this.props.match.params
		this.fetchAllFormData()
	}

	handleAutoScalerSubmit(event) {
		//Get data from instance slider and update appln object.
		this.handleSubmit(event)
	}

	handleSubmit(event) {
		event.preventDefault();
		const headers = new Headers({ 'Content-Type': 'application/json' })
		var applicationBody = {};
		let stringJson = this.state.application;
		Object.keys(stringJson).forEach(function (key) {
			if (stringJson[key] !== null)
				applicationBody[key] = stringJson[key];
		});
		fetch('api/user/applications/' + this.props.match.params.applicationId + '/',
			{
				headers: new Headers(authHeader()),
				method: 'PATCH',
				body: JSON.stringify(applicationBody)
			}
		).then(response => {
			if (response.ok) {
				toast.success('Application changes has been successfully saved.', {
					position: "top-center",
					autoClose: 5000,
					hideProgressBar: true,
					closeOnClick: true,
					pauseOnHover: true,
					draggable: true
					});
			}
			else {
				toast.error("We couldn\'t save your changes. Please try again.!!", {
					position: "top-center",
					autoClose: 5000,
					hideProgressBar: true,
					closeOnClick: true,
					pauseOnHover: true,
					draggable: true
					});
			}
		})
		.catch(error => {
			toast.error("We couldn't save your changes. Please try again.!!", {
				position: "top-center",
				autoClose: 5000,
				hideProgressBar: true,
				closeOnClick: true,
				pauseOnHover: true,
				draggable: true
				});
		});

	}

	updateScalerObject(scalerObject) {
		this.setState({
			application:
				{ ...this.state.application, scaler: scalerObject }
		});
	}

	updateAutoScalerObject(autoScalerObject, index) {
		const auto_scalers = this.state.application.auto_scalers;
		auto_scalers[parseInt(index)] = autoScalerObject;
		// update state
		this.setState({
			application:
				{ ...this.state.application, auto_scalers: auto_scalers }
		});
	}


	fetchAllFormData() {
		if (!this.state.application) {
			fetch("api/user/applications/" + this.props.match.params.applicationId + '/', {
				headers: new Headers(authHeader())
			})
				.then(response => response.json())
				.then(result => this.setState({ application: result }))
		}

		fetch("api/user/metrics", {
			headers: new Headers(authHeader())
		})
			.then(response => response.json())
			.then(result => this.setState({ metricsOptions: result.results }))

		fetch("api/user/loadbalancers", {
			headers: new Headers(authHeader())
		})
			.then(response => response.json())
			.then(result => this.setState({ loadbalancerOptions: result.results }))

		fetch("api/user/credentials", {
			headers: new Headers(authHeader())
		})
			.then(response => response.json())
			.then(result => this.setState({ credentialOptions: result.results }))

		fetch("api/user/metricstores", {
			headers: new Headers(authHeader())
		})
			.then(response => response.json())
			.then(result => this.setState({ metricstoreOptions: result.results }))
	}

	handleDeleteUser(id) {
		return (e) => this.props.dispatch(userActions.delete(id));
	}

	handleLoadbalancerUpdate(updatedLoadbalancers) {
		this.setState({ application: { ...this.state.application, loadbalancer: updatedLoadbalancers } });
	}

	handleMetricsUpdate(updatedMetrics) {
		this.setState({ application: { ...this.state.application, metrics: updatedMetrics } });
	}

	handleMetricsStoreUpdate(updatedMetricStore) {
		this.setState({ application: { ...this.state.application, metric_store: updatedMetricStore } });
	}


	handleCredentialsUpdate(updatedCredentials) {
		this.setState({ application: { ...this.state.application, credentials: [updatedCredentials] } });
	}

	handleTextFieldDataChange(e) {
		e.persist();
		this.setState({ application: { ...this.state.application, name: e.target.value } });
	}

	onValueChange(value) {
		this.setState({
			application : {
				...this.state.application,
				scaler : {
					...this.state.application.scaler,
					numInstances : value
				}
			}
		})
	}

	render() {
		const { user, users } = this.props;
		//const axesPoints = [ [50, 330], [75, 200], [280, 75], [300, 75], [475, 300], [600, 200] ];		
		let application = this.state.application;
		let self = this
		//Metric Data 
		const metricColumns = [{
			Header: 'Name',
			accessor: 'name' // String-based value accessors!
		}, {
			Header: 'Metric Type',
			accessor: 'object_type'
		}]
		const instanceColumns = [{
			Header: 'Name',
			accessor: 'instance_str' // String-based value accessors!
		}, {
			Header: 'Status',
			accessor: 'status'
		}]

		let metricsData = (application && application.metrics) ? (<ReactTable
			data={application.metrics}
			columns={metricColumns}
			className="-striped -highlight"
			showPagination={false}
			defaultPageSize={application.metrics.length > 20 ? 20 : application.metrics.length < 3 ? 3 : application.metrics.length}
		/>) : (<p>No Metrics Configured.</p>)

		let instancesData = (application && application.instances) ? (<ReactTable
			data={application.instances}
			columns={instanceColumns}
			className="-striped -highlight"
			showPagination={false}
			defaultPageSize={application.instances.length > 20 ? 20 : application.instances.length < 3 ? 3 : application.instances.length}
		/>) : (<p>No servers are available.</p>)

		let instancesOptions = []
		if (application && application.instances) {
			application.instances.map((instance, key) => {
				instancesOptions.push(instance)
			})
		}

		let autoScalers = []
		if (application.scaler || (application.auto_scalers && application.auto_scalers.length > 0)) {
			autoScalers.push(
				<div className="box-header with-border">
					<input type="submit" value="Save" id="autoscaler_update"/>
					{application.scaler && <RangeComponent ref={this.instanceRef}
						value={application.scaler.numInstances}
						minValue={application.scaler.minInstances}
						maxValue={application.scaler.maxInstances}
						title="Number of Instances" 
						onValueChange={this.onValueChange}/>
					}
					{application.auto_scalers && application.auto_scalers.length > 0 && <div>
						Enable Autoscalers:&nbsp;&nbsp;
              			<Toggle id="enable_autoscaler_view_app"
							checked={application.enable_autoscalers}
							onToggle={(value, event) =>
								this.setState({
									application: {
										...application,
										enable_autoscalers: value,
									}
								}, () => {
									this.forceUpdate();
								})
							}
						/>
					</div>}
				</div>
			)
			application.enable_autoscalers && application.auto_scalers.map((auto_scaler, key) => {
				let id = "collapseTwo" + key;
				let href = "#" + id;
				autoScalers.push(
					<div className="panel panel-default">
						<div className="panel-heading">
							<h4 className="panel-title">
								<a data-toggle="collapse" data-parent="#autoscaler" href={href}>{auto_scaler.object_type}</a>
							</h4>
						</div>

						<div id={id} className="panel-collapse collapse in">
							<div className="panel-body">
								<AutoScalerPage auto_scaler={auto_scaler} updateAutoScalerObject={(autoscalerObject) => this.updateAutoScalerObject(autoscalerObject, key)} />
							</div>
						</div>
					</div>
				);
			})
		}
		let scaler = []
		if (application.scaler) {
			
			scaler.push(
				<div className="panel panel-default">
					<div className="panel-heading">
						<h4 className="panel-title">
							<a data-toggle="collapse" data-parent="#scaler" href="#collapseOne">Scaler</a>
						</h4>
					</div>
					<div id="collapseOne" className="panel-collapse collapse in">
						<div className="panel-body">
							<SimpleScalerPage scaler={application.scaler} updateScalerObject={self.updateScalerObject} />
						</div>
					</div>
				</div>
			)
		}
		let applicationDashboardData = []
		if (application && application.dashboard != null &&
			application.dashboard != undefined &&
			application.dashboard.render != null &&
			application.dashboard.render != undefined) {
			applicationDashboardData = (<ApplicationDashboard dashboard={application.dashboard} />)
		}
		else {
			applicationDashboardData.push(<div style={{
				height: '100px',
				textAlign: 'center',
				lineHeight: '100px'
			}}> No dashboard data available </div>)
		}

		let application_tabs = (<div className="col-md-6"></div>)
		if (application) {
			application_tabs = (
				<div className="col-md-6">
					<div>Name:</div>
					<input type="text" value={application.name} onChange={(e) => self.handleTextFieldDataChange(e)} className="input name-input" />
					<div>Loadbalancer:</div>
					<Select
						classNamePrefix="react-select"
						value={self.state.application.loadbalancer}
						onChange={self.handleLoadbalancerUpdate}
						options={self.state.loadbalancerOptions}
						getOptionLabel={(option) => option.lb}
						getOptionValue={(option) => option.lb}
					/>
					<div>Credentials:</div>
					<Select
						classNamePrefix="react-select"
						value={self.state.application.credentials}
						onChange={self.handleCredentialsUpdate}
						options={self.state.credentialOptions}
						getOptionLabel={(option) => option.name}
						getOptionValue={(option) => option}
					/>
					<div>Metrics:</div>
					<Select
						classNamePrefix="react-select"
						value={self.state.application.metrics}
						onChange={self.handleMetricsUpdate}
						options={self.state.metricsOptions}
						isMulti={true}
						getOptionLabel={(option) => option.name}
						getOptionValue={(option) => option.value ? option.value : option.name}
					/>

					<div>Metric Store:</div>
					<Select
						classNamePrefix="react-select"
						value={self.state.application.metric_store}
						onChange={self.handleMetricsStoreUpdate}
						options={self.state.metricstoreOptions}
						getOptionLabel={(option) => option.object_type}
						getOptionValue={(option) => option}
					/>
					<div>Instances:</div>
					<Select
						classNamePrefix="react-select"
						value={instancesOptions}
						isMulti={true}
						isDisabled={true}
						isClearable={false}
						placeholder={"No instances found."}
						getOptionLabel={(option) => option.instance_str}
						getOptionValue={(option) => option.name}
					/>
					<br/>
					<input type="submit" value="Update" />
				</div>)
		}
		return (
			<div>
				<Header />
				<ASideLeft />
				<div className="content-wrapper">
					<section className="content">
						<div className="col-xs-12">
							<div className="nav-tabs-custom">
								<ul className="nav nav-tabs">
									<li className="active"><a href="#dashboard" data-toggle="tab" aria-expanded="true">Dashboard</a></li>
									<li className=""><a href="#metrics" data-toggle="tab" aria-expanded="false">Metrics</a></li>
									<li className=""><a href="#server" data-toggle="tab" aria-expanded="false">Instances</a></li>
									<li className=""><a href="#settings" data-toggle="tab" aria-expanded="false">Settings</a></li>
									{autoScalers && autoScalers.length > 0 && <li className=""><a href="#autoscalers" data-toggle="tab" aria-expanded="false">AutoScalers</a></li>}
									{/* <li className=""><a href="#deployment" data-toggle="tab" aria-expanded="false">Deployment</a></li> */}
								</ul>
								<div className="tab-content">
									<div className="tab-pane active" id="dashboard">
										{applicationDashboardData}
										<div className="box-body">
											<div className="col-md-12"></div>
										</div>
									</div>
									<div className="tab-pane" id="metrics">
										<div className="box box-info">
											<div className="box-body">
												<div className="col-md-12">
													{metricsData}
												</div>
											</div>
										</div>
									</div>
									<div className="tab-pane" id="server">
										<div className="box box-info">
											<div className="box-body">
												<div className="col-md-12">
													{instancesData}
												</div>
											</div>
										</div>
									</div>
									<div className="tab-pane" id="settings">
										<div className="box box-info">
											<form onSubmit={self.handleSubmit}>
												<div className="box-body">
													{application_tabs}
													<div className="col-md-6">
														<div className="bs-example">
															<div className="panel-group" id="scaler">
																{scaler}
															</div>
														</div>	
															{/* <DrawSplinePage type={"daily"}/> */}
													</div>
												</div>
											</form>
										</div>
									</div>
									<div className="tab-pane" id="autoscalers">
										<form onSubmit={self.handleSubmit}>
											<div className="box-body">
												<div className="col-md-12">
													<div className="panel-group" id="autoscaler">
														{autoScalers}
													</div>
												</div>
											</div>
										</form>
									</div>
									<div className="tab-pane" id="deployment">
										<div className="box-body">
											<div className="col-md-12">
												<div className="panel-group" id="deployment">
													TEST DATA
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</section>
				</div>
				<AsideRight />
				<Footer />
			</div>

		);
	}
}

function mapStateToProps(state) {
	const { users, authentication } = state;
	const { user } = authentication;
	return {
		user,
		users
	};
}

const connectedHomePage = connect(mapStateToProps)(ViewApplicationPage);
export { connectedHomePage as ViewApplicationPage };
