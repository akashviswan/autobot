import { dashboardConstants } from '../_constants';
import { homeService } from '../_services';

function loadedDashboardData(dashboard) {
    return { type: dashboardConstants.LOAD_DASHBOARD_DATA, dashboard };
}

function loadDashboardData() {
    return function(dispatch) {
      return homeService.getHomeDashboardData().then(dashboardData => {
        console.log(dashboardData);
        dispatch(loadedDashboardData(dashboardData));
      }).catch(error => {
        throw(error);
      });
    };
}

export const homeActions = {
  loadDashboardData,
  loadedDashboardData
};


  