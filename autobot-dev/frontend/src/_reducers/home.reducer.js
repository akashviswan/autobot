import {dashboardConstants} from '../_constants';
import initialState from './initialState';

export function home(state = initialState.dashboard, action) {
  switch (action.type) {
    case dashboardConstants.LOAD_DASHBOARD_DATA:
    return Object.assign({},state, action.dashboard)
    default:
      return state;
  }
}