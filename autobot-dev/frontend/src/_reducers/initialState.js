let user = JSON.parse(localStorage.getItem('user'));
const initialState = user ? { loggedIn: true, user } : {};

export default {
    registration: {},
    users: {},
    authentication: {},
    alert: {},
    dashboard:{}
  };