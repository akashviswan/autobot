import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';
import {Header} from '../Header/Header';
import Footer from '../Footer/Footer';
import ASideLeft from '../ASideLeft/ASideLeft';
import AsideRight from '../AsideRight/AsideRight';
import HomeContent from '../HomeContent/HomeContent';
import { userActions,homeActions } from '../_actions';
import { store } from '../_helpers';
class HomePage extends React.Component {
    componentDidMount() {
        this.props.dispatch(userActions.getAll());
        this.props.dispatch(homeActions.loadDashboardData());
    }

    handleDeleteUser(id) {
        return (e) => this.props.dispatch(userActions.delete(id));
    }
    
    render() {
        const { user, users } = this.props;
        return (
            <div>
                <Header />
                <ASideLeft />
                <div className="wrapper" id="mainContent">
                    <HomeContent/>
                </div>
                <AsideRight />
                <Footer />
            </div>
        );
    }
}

function mapStateToProps(state) {
    const { users, authentication } = state;
    const { user } = authentication;
    return {
        user,
        users
    };
}

const connectedHomePage = connect(mapStateToProps)(HomePage);
export { connectedHomePage as HomePage };