import React from 'react';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';
import { userActions } from '../_actions';

class AsideRight extends React.Component {
    componentDidMount() {
        //this.props.dispatch(userActions.getAll());
    }
    render() {
        return (
					<aside className="control-sidebar control-sidebar-dark">   
						<ul className="nav nav-tabs nav-justified control-sidebar-tabs">
							<li><a href="#control-sidebar-home-tab" data-toggle="tab"><i className="fa fa-home"></i></a></li>
							<li><a href="#control-sidebar-settings-tab" data-toggle="tab"><i className="fa fa-gears"></i></a></li>
						</ul> 
						<div className="tab-content">  
							<div className="tab-pane" id="control-sidebar-home-tab">
								<h3 className="control-sidebar-heading">Recent Activity</h3>       
							</div>   
							<div className="tab-pane" id="control-sidebar-stats-tab">Stats Tab Content</div>     
							<div className="tab-pane" id="control-sidebar-settings-tab">
								<form method="post">
								<h3 className="control-sidebar-heading">General Settings</h3>         
								</form>
							</div>
						</div>
					</aside>
					/* <div className="control-sidebar-bg"></div>
					</div> */
        );
    }
}

export default AsideRight;