import React from 'react';
import { Link } from 'react-router-dom';
import ReactDOM from 'react-dom';
import { connect } from 'react-redux';
import { userActions } from '../_actions';

class ASideLeft extends React.Component {
    componentDidMount() {
        //this.props.dispatch(userActions.getAll());
    }

//     handleClick(event) {
//      event.preventDefault();
//      var temp = document.getElementById("mainContent");

//      ReactDOM.render(<ApplicationPage />, temp);
//    }
    

    render() {
        return (
				<aside className="main-sidebar">
					<section className="sidebar">
						<form action="#" method="get" className="sidebar-form">
							<div className="input-group">
								<input type="text" name="q" className="form-control" placeholder="Search..." />
								<span className="input-group-btn">
									<button type="submit" name="search" id="search-btn" className="btn btn-flat">
										<i className="fa fa-search"></i>
									</button>
								</span>
							</div>
						</form>
						<ul className="sidebar-menu">
							<li className="header">MAIN NAVIGATION</li>
							<li>
								<a href="/">
									<i className="fa fa-dashboard"></i> <span>Dashboard</span>
								</a>
							</li>
							<li>
								<a href="/server_index">
									<i className="fa fa-server"></i> <span>Servers</span>
								</a>
							</li>
							<li>
								<Link to="/applications">
									<i className="fa  fa-database"></i> <span>Application</span>
								</Link>
							</li>
							<li>
								<a href="/metrics">
									<i className="fa fa-hourglass-half"></i> <span>Metrics</span>
								</a>
							</li>
							<li>
								<a href="#">
									<i className="fa  fa-wrench"></i> <span>Settings</span>
								</a>
							</li>
						</ul>
					</section>
				</aside>

        );
    }
}

export default ASideLeft;