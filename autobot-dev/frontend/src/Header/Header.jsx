import React from 'react';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';
import {bindActionCreators} from 'redux';
import { userActions } from '../_actions';

class Header extends React.Component {
    constructor(props) {
		super(props);
		this.logout = this.logout.bind(this); 
	}
	
	logout(e) {
		e.preventDefault();
		this.props.dispatch(userActions.logout());
	}

    render() {
        return (        		
				<header className="main-header">		
					<a href="/index" className="logo">			
						<span className="logo-mini"><b>Autobot</b></span>			
						<span className="logo-lg"><b>Autobot</b> </span>
					</a>			
					<nav className="navbar navbar-static-top">			
						<a href="#" className="sidebar-toggle" data-toggle="offcanvas" role="button">
							<span className="sr-only">Toggle navigation</span>
							<span className="icon-bar"></span>
							<span className="icon-bar"></span>
							<span className="icon-bar"></span>
						</a>
						<div className="navbar-custom-menu">
							<ul className="nav navbar-nav">       		
								<li>
									<a href="#" data-toggle="control-sidebar"><i className="fa fa-gears"></i></a>
								</li>
								<li >
									<a href="#" onClick={this.logout} data-toggle="control-sidebar"><i className="fa fa-sign-out"></i></a>
								</li>
							</ul>
						</div>
					</nav>
				</header>
        );
    }
}

function mapStateToProps(state) {
    const { user } = state.authentication;
    return {
        user
    };
}

function mapDispatchToProps(dispatch) {
    let actions = bindActionCreators({ userActions });
    return { ...actions, dispatch };
}

const headerPage = connect(mapStateToProps,mapDispatchToProps)(Header);
export { headerPage as Header }; 
//export default Header