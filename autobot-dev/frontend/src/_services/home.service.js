import { authHeader,handleResponse } from "../_helpers";

export const homeService = {
    getHomeDashboardData,
};

function getHomeDashboardData() {
    const requestOptions = {
        method: 'GET',
        headers : new Headers(authHeader())
    };
    return fetch('api/user/dashboard',requestOptions).
    then(handleResponse)
}
