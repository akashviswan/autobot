import {FormItems} from './TemplateConstants';

const ScalerKeys = {
    AWSScale : {
        amiId : {
            type : FormItems.TEXTFIELD,
            label : "AMI ID"
        },
        instance_type :{             
            type : FormItems.DROPDOWN,
            label : "Instance Type",
            defaultOptions : [
                { value : "t2.nano" ,label : "T2 NANO"},
                { value : "t2.micro" ,label:  "T2 MICRO"},
                { value : "t2.small" ,label:  "T2 SMALL"},
                { value : "t2.medium" ,label:  "T2 MEDIUM"},
                { value : "t2.large" ,label:  "T2 LARGE"},
                { value : "t2.xlarge" ,label:  "T2 XLARGE"},
                { value : "t2.2xlarge" ,label:  "T2 2XLARGE"},
                { value : "m4.large" ,label:  "M4 LARGE"},
                { value : "m4.xlarge" ,label:  "M4 XLARGE"},
                { value : "m4.2xlarge" ,label:  "M4 2XLARGE"},
                { value : "m4.4xlarge" ,label:  "M4 4XLARGE"},
                { value : "m4.10xlarge" ,label:  "M4 10XLARGE"},
                { value : "m4.16xlarge" ,label:  "M4 16XLARGE"},
                { value : "m3.medium" ,label:  "M3 MEDIUM"},
                { value : "m3.large" ,label:  "M3 LARGE"},
                { value : "m3.xlarge" ,label:  "M3 XLARGE"},
                { value : "m3.2xlarge" ,label:  "M3 2XLARGE"}
            ]   
        },
        dryRun : {             
            type : FormItems.CHECKBOX,
            label : "Dry Run",
        },
        SubnetId :{
             type : FormItems.TEXTFIELD ,
             label : "Subnet ID",
        },
        SecurityGroupId :{
             type : FormItems.TEXTFIELD  ,
             label : "Security Group ID",  
        },
        key_pair :{
             type : FormItems.TEXTFIELD ,
             label : "Key Pair",  
        },
    },
    AzureScaleSet : {
        group_name : { 
            type : FormItems.TEXTFIELD ,
            label : "Group Name",  
        },
        vm_scale_set_name : { 
            type : FormItems.TEXTFIELD ,
            label : "VM Scale Set Name",        
        },
        instance_type : { 
            type : FormItems.TEXTFIELD,
            label : "Instance Type",        
        },
        resourceuri : {  
           type : FormItems.TEXTFIELD ,
           label : "Resource URI",        
        },
        rulename :{       
          type : FormItems.TEXTFIELD,
          label : "Rule Name",            
        },
    }
}

export const ScalerTemplate = (function () {
    var instance = {
        cool_off_period :{ 
            type : FormItems.TEXTFIELD,
            label : "Cool Off Period", 
            pattern:"[0-9]*",         
        },
        minInstances :{ 
            type : FormItems.TEXTFIELD,
            label : "Min Instances", 
            pattern:"[0-9]*",                 
        },
        maxInstances :{
            type : FormItems.TEXTFIELD,
            label : "Max Instances", 
            pattern:"[0-9]*",                      
        },
        numInstances :{ 
            type : FormItems.TEXTFIELD,
            label : "Number of Instances", 
            pattern:"[0-9]*",                           
        },
    };

    function addTemplateFieldsBasedOnType(object_type) {
        console.log("object_type");
        console.log(object_type);
        if (object_type === "AWSScale") {
            _.assign(instance, ScalerKeys.AWSScale); // extend
        }
        else {
            _.assign(instance, ScalerKeys.AzureScaleSet); // extend
        }
        return instance
    }

    return {
        fields: function (object_type) {
            instance = addTemplateFieldsBasedOnType(object_type);
            return instance;
        }
    };
})();






