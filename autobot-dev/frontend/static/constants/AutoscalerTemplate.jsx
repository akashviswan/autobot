import {FormItems} from './TemplateConstants';

const AutoScalerKeys = {

    SimpleScaler : {
        metric : {
            type : FormItems.DROPDOWN,
            label : "Metric",
            dropdown_label_key : 'name',
            dropdown_datasource_key : 'metrics'
        },
        monitoring_engine : {
            type : FormItems.DROPDOWN,
            label : "Monitoring Engine",
            dropdown_label_key : 'object_type',
            dropdown_datasource_key : 'monitoring'
        },
        condition :{             
            type : FormItems.DROPDOWN,
            label : "Condition",
            dropdown_value_key : 'value', //if not specified , by default the whole option will be used as value
            defaultOptions : [
                {value:'lt', label: 'Less than'},
                {value : 'gt', label : 'Great than'}
            ]   
        },
        value : {             
            type : FormItems.TEXTFIELD,
            label : "Threshold Value",
            max_digits:5,
            decimal_places:2,
            pattern: "[0-9]+(\.[0-9]{0,2})?",                         
        }
    },
    PredictiveScaler : {
        metrics : { 
            type : FormItems.TEXTFIELD ,
            label : "Metrics", 
            dropdown_label_key : 'name',
            dropdown_datasource_key : 'metrics'
        },
        fwd_time : { 
            type : FormItems.TEXTFIELD ,
            label : "Forward Time",
            pattern:"[0-9]*"                     
        },
        value : { 
            type : FormItems.TEXTFIELD,
            label : "Threshold Value",
            max_digits:5,
            decimal_places:2,
            pattern: "[0-9]+(\.[0-9]{0,2})?",                         
        },
    },
    ProphetScaler : {
        metric : { 
            type : FormItems.DROPDOWN ,
            label : "Metric",
            dropdown_label_key : 'name',
            dropdown_datasource_key : 'metrics' 
        },
        monitoring_engine : {
            type : FormItems.DROPDOWN,
            label : "Monitoring Engine",
            dropdown_label_key : 'object_type',
            dropdown_datasource_key : 'monitoring'
        },
        training_data_period : { //Int
            type : FormItems.TEXTFIELD ,
            label : "Training Data Period",
            pattern:"[0-9]*",                      
        },
        training_data_percent : { //Int
            type : FormItems.TEXTFIELD ,
            label : "Training Data Percentage",
            pattern:"[0-9]*",                     
        },
        fwd_time : { 
            type : FormItems.TEXTFIELD ,
            label : "Forward Time",
            pattern:"[0-9]*",                      
        },
        value : { 
            type : FormItems.TEXTFIELD,
            label : "Threshold Value",
            pattern: "[0-9]+(\.[0-9]{0,2})?",                         
        },
        retrain_period : { //Integer
            type : FormItems.TEXTFIELD,
            label : "Retrain Period",
            pattern:"[0-9]*",                           
        },
        last_train : {  //Date
            type : FormItems.TEXTFIELD,
            label : "Last Train"
        },
    }
}

export const AutoScalerTemplate = (function () {
    var instance = {
        priority :{ 
            type : FormItems.TEXTFIELD,
            label : "Priority",  
            pattern:"[0-9]*",                                
        }
    };

    function addTemplateFieldsBasedOnType(object_type) {
        if (object_type === "SimpleScaler") {
            _.assign(instance, AutoScalerKeys.SimpleScaler); // extend
        }
        else if (object_type === "PredictiveScaler") {
            _.assign(instance, AutoScalerKeys.PredictiveScaler); // extend
        }
        else {
            _.assign(instance, AutoScalerKeys.ProphetScaler); // extend
        }
        return instance
    }

    return {
        fields: function (object_type) {
            instance = addTemplateFieldsBasedOnType(object_type);
            return instance;
        }
    };
})();






