export const FormItems = {
    TEXTVIEW : "TextView",
    TEXTFIELD : 'TextField',
    CHECKBOX : 'CheckBox',
    DROPDOWN : 'Dropdown',
    DROPDOWNMULTI : 'DropdownMulti',
    DATETIMEPICKER : 'DateTimePicker'
}


export const ModelUrl = {
    AzureRegions : "api/azureregions",
    AWSRegions : "api/awsregions",
    Metric : "api/user/metrics"
}