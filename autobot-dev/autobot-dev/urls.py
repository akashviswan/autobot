from django.urls import path,re_path
from django.conf.urls import url,include
from django.contrib import admin


urlpatterns = [
    url(r'^admin/', admin.site.urls),
	re_path(r'^api/', include('core.urls')),
    re_path(r'', include('frontend.urls')),
]