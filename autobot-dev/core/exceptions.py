class CoreError(Exception):
    """
    The base exception class for Core exceptions.
    """
    err = 'An unspecified error occurred'

    def __init__(self, **kwargs):
        msg = self.err.format(**kwargs)
        Exception.__init__(self, msg)
        self.kwargs = kwargs
        
