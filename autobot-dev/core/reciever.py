from django.dispatch import receiver
from core.signals import *
from .management.django_q.django_q_tasks import *
from django_q.tasks import async_task, schedule,Task

def switch_app_autoscaling(sender, **kwargs):
    if kwargs["application"]:
        application = kwargs["application"]
        if application.enable_autoscalers:
            task_id = async_task(start_autoscaling,application)
            application.autoscaler_taskid = task_id
        else:
            task_id = application.autoscaler_taskid
            tasks = Task.objects.filter(id=task_id)
            if tasks.count()>0:
                return tasks.delete()

new_application_added.connect(switch_app_autoscaling, sender=None,
                  dispatch_uid="switch_app_autoscaling")



