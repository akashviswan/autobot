from django.contrib import admin
from django.contrib.auth.models import Group
from core.models import *
# Register your models here.
# admin.site.register(Application)
# admin.site.register(Credential)
admin.site.unregister(Group)
from polymorphic.admin import PolymorphicInlineSupportMixin, StackedPolymorphicInline
from polymorphic.admin import PolymorphicParentModelAdmin, PolymorphicChildModelAdmin, PolymorphicChildModelFilter

class ModelLoadbalancerChildAdmin(PolymorphicChildModelAdmin):
    """ Base admin class for all child models """
    base_model = LoadBalancer 

admin.site.register(ELB_Classic)
class ELB_ClassicAdmin(ModelLoadbalancerChildAdmin):
    base_model = ELB_Classic  # Explicitly set here!
    # define custom features here


admin.site.register(LoadBalancer)
class LoadbalancerParentAdmin(PolymorphicParentModelAdmin):
    """ The parent model admin """
    base_model = LoadBalancer 
    child_models = (ELB_Classic,)
    list_filter = (PolymorphicChildModelFilter,)  # This is optional.


class ModelMonitoringChildAdmin(PolymorphicChildModelAdmin):
    """ Base admin class for all child models """
    base_model = Monitoring 

admin.site.register(AWSCloudWatch)
class AWSCloudWatchAdmin(ModelLoadbalancerChildAdmin):
    base_model = AWSCloudWatch  # Explicitly set here!
    # define custom features here


admin.site.register(CSVFile)
class CSVFilehAdmin(ModelLoadbalancerChildAdmin):
    base_model = CSVFile  # Explicitly set here!
    # define custom features here


admin.site.register(Monitoring)
class LoadbalancerParentAdmin(PolymorphicParentModelAdmin):
    """ The parent model admin """
    base_model = Monitoring 
    child_models = (AWSCloudWatch,CSVFile,)
    list_filter = (PolymorphicChildModelFilter,)  # This is optional.


class ModelScalerChildAdmin(PolymorphicChildModelAdmin):
    """ Base admin class for all child models """
    base_model = Scaler 

admin.site.register(AWSScale)
class AWSScaleAdmin(ModelLoadbalancerChildAdmin):
    base_model = AWSScale 

admin.site.register(AWSAutoScale)
class AWSAutoScaleAdmin(ModelLoadbalancerChildAdmin):
    base_model = AWSAutoScale

admin.site.register(AzureScaleSet)
class AzureScaleSetAdmin(ModelLoadbalancerChildAdmin):
    base_model = AzureScaleSet

admin.site.register(ReplicationControllerScaler)
class ReplicationControllerScalerAdmin(ModelLoadbalancerChildAdmin):
    base_model = ReplicationControllerScaler

admin.site.register(KubernetesDeployment)
class KubernetesDeploymentAdmin(ModelLoadbalancerChildAdmin):
    base_model = KubernetesDeployment


admin.site.register(Scaler)
class ScalerParentAdmin(PolymorphicParentModelAdmin):
    """ The parent model admin """
    base_model = Scaler 
    child_models = (AWSScale,AWSAutoScale,AzureScaleSet,ReplicationControllerScaler,KubernetesDeployment,)
    list_filter = (PolymorphicChildModelFilter,)  # This is optional.

class ModelCredentialChildAdmin(PolymorphicChildModelAdmin):
    """ Base admin class for all child models """
    base_model = Credential 

admin.site.register(SSHCredential)
class SSHCredentialAdmin(ModelLoadbalancerChildAdmin):
    base_model = SSHCredential

admin.site.register(AWSCredential)
class AWSCredentialAdmin(ModelLoadbalancerChildAdmin):
    base_model = AWSCredential

admin.site.register(AzureCredential)
class AzureCredentialAdmin(ModelLoadbalancerChildAdmin):
    base_model = AzureCredential

admin.site.register(KubeClientConfig)
class KubeClientConfigAdmin(ModelLoadbalancerChildAdmin):
    base_model = KubeClientConfig

admin.site.register(Credential)
class CredentialParentAdmin(PolymorphicParentModelAdmin):
    """ The parent model admin """
    base_model = Credential 
    child_models = (SSHCredential,AWSCredential,AzureCredential,KubeClientConfig,)
    list_filter = (PolymorphicChildModelFilter,)  # This is optional.

class ModelDashboardChildAdmin(PolymorphicChildModelAdmin):
    """ Base admin class for all child models """
    base_model = Dashboard  

admin.site.register(KibanaDash)
class KibanaDashAdmin(ModelLoadbalancerChildAdmin):
    base_model = KibanaDash

admin.site.register(Dashboard)
class DashboardParentAdmin(PolymorphicParentModelAdmin):
    """ The parent model admin """
    base_model = Dashboard 
    child_models = (KibanaDash,)
    list_filter = (PolymorphicChildModelFilter,)  # This is optional.

class ModelInstanceChildAdmin(PolymorphicChildModelAdmin):
    """ Base admin class for all child models """
    base_model = Instance  

admin.site.register(EC2)
class EC2Admin(ModelLoadbalancerChildAdmin):
    base_model = EC2

admin.site.register(AzureVM)
class AzureVMAdmin(ModelLoadbalancerChildAdmin):
    base_model = AzureVM

admin.site.register(VirtualMachine)
class VirtualMachineAdmin(ModelLoadbalancerChildAdmin):
    base_model = VirtualMachine

admin.site.register(Instance)
class InstanceParentAdmin(PolymorphicParentModelAdmin):
    """ The parent model admin """
    base_model = Instance 
    child_models = (AzureVM,EC2,VirtualMachine)
    list_filter = (PolymorphicChildModelFilter,)  # This is optional.

class ModelMetricStoreChildAdmin(PolymorphicChildModelAdmin):
    """ Base admin class for all child models """
    base_model = MetricStore  

admin.site.register(ES)
class ESAdmin(ModelLoadbalancerChildAdmin):
    base_model = ES

admin.site.register(MetricStore)
class MetricStoreParentAdmin(PolymorphicParentModelAdmin):
    """ The parent model admin """
    base_model = MetricStore 
    child_models = (ES,)
    list_filter = (PolymorphicChildModelFilter,)  # This is optional.

class ModelMetricChildAdmin(PolymorphicChildModelAdmin):
    """ Base admin class for all child models """
    base_model = Metric  

admin.site.register(EC2CloudWatchMetric)
class ELBCloudWatchMetricAdmin(ModelLoadbalancerChildAdmin):
    base_model = EC2CloudWatchMetric

admin.site.register(ELBCloudWatchMetric)
class ELBCloudWatchMetricAdmin(ModelLoadbalancerChildAdmin):
    base_model = ELBCloudWatchMetric

admin.site.register(ASGCloudWatchMetric)
class ASGCloudWatchMetricAdmin(ModelLoadbalancerChildAdmin):
    base_model = ASGCloudWatchMetric

admin.site.register(AzureScaleSetMetricRule)
class AzureScaleSetMetricRuleAdmin(ModelLoadbalancerChildAdmin):
    base_model = AzureScaleSetMetricRule

admin.site.register(CSVMetric)
class CSVMetricAdmin(ModelLoadbalancerChildAdmin):
    base_model = CSVMetric

admin.site.register(Metric)
class MetricParentAdmin(PolymorphicParentModelAdmin):
    """ The parent model admin """
    base_model = Metric 
    child_models = (EC2CloudWatchMetric,ELBCloudWatchMetric,ASGCloudWatchMetric,AzureScaleSetMetricRule,CSVMetric)
    list_filter = (PolymorphicChildModelFilter,)  # This is optional.

class ModelAutoscalerChildAdmin(PolymorphicChildModelAdmin):
    """ Base admin class for all child models """
    base_model = AutoScaler 

admin.site.register(SimpleScaler)
class SimpleScalerAdmin(ModelAutoscalerChildAdmin):
    base_model = SimpleScaler  

admin.site.register(PredictiveScaler)
class PredictiveScalerAdmin(ModelAutoscalerChildAdmin):
    base_model = PredictiveScaler 

admin.site.register(SQSQueueClearer)
class SQSQueueClearerAdmin(ModelAutoscalerChildAdmin):
    base_model = SQSQueueClearer 

admin.site.register(AzureScaleSetScale)
class AzureScaleSetScaleAdmin(ModelAutoscalerChildAdmin):
    base_model = AzureScaleSetScale  

admin.site.register(ProphetScaler)
class ProphetScalerScaleAdmin(ModelAutoscalerChildAdmin):
    base_model = ProphetScaler  

class AutoScalerParentAdmin(PolymorphicParentModelAdmin):
    """ The parent model admin """
    base_model = AutoScaler 
    child_models = (SimpleScaler,PredictiveScaler,SQSQueueClearer,AzureScaleSetScale,ProphetScaler,)
    list_filter = (PolymorphicChildModelFilter,)  # This is optional.

admin.site.register(Application)
class ApplicationAdmin(PolymorphicInlineSupportMixin, admin.ModelAdmin):
    """
    Admin for orders.
    The inline is polymorphic.
    To make sure the inlines are properly handled,
    the ``PolymorphicInlineSupportMixin`` is needed to
    """
    inlines = (AutoScalerParentAdmin,)