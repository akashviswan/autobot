import django.dispatch

#1. Signal to invoke for start monitoring the application.
new_application_added = django.dispatch.Signal(providing_args=["application"])
