from rest_framework import serializers
from core.models import Credential,SSHCredential,AzureCredential,AWSCredential,KubeClientConfig
from core.serializers.aws import AWSCredentialSerializer
from core.serializers.azureint import AzureCredentialSerializer
from core.serializers.kubernetes import KubeClientConfigSerializer
from rest_polymorphic.serializers import PolymorphicSerializer

class SSHCredentialSerializer(serializers.ModelSerializer):
    object_type = serializers.SerializerMethodField()
    class Meta:
        model = SSHCredential
        fields = '__all__'
        extra_kwargs = {
        'id': {
            'read_only': False, 
            'required': True
            }
        } 

    @staticmethod
    def get_object_type(obj):
        return "SSHCredential"


class CredentialSerializer(serializers.ModelSerializer):
    class Meta:
        model = Credential
        fields = '__all__'
        
    

class CredentialPolymorphicSerializer(PolymorphicSerializer):
    model_serializer_mapping = {
        SSHCredential: SSHCredentialSerializer,
        AzureCredential: AzureCredentialSerializer,
        AWSCredential: AWSCredentialSerializer,
        KubeClientConfig: KubeClientConfigSerializer
    }
