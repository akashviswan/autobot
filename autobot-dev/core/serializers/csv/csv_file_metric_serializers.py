from core.models import CSVMetric
from rest_framework import serializers

class CSVMetricSerializer(serializers.ModelSerializer):
    object_type = serializers.SerializerMethodField()
    
    @staticmethod
    def get_object_type(obj):
        return "CSVMetric"
    class Meta:
        model = CSVMetric
        fields = '__all__'
        extra_kwargs = {
            'id': {
                'read_only': False, 
                'required': True
             }
        } 