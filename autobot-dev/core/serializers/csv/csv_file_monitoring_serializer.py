from core.models import CSVFile
from rest_framework import serializers

class CSVFileSerializer(serializers.ModelSerializer):
    object_type = serializers.SerializerMethodField()
    
    @staticmethod
    def get_object_type(obj):
        return "CSVFile"
    class Meta:
        model = CSVFile
        fields = '__all__'
        extra_kwargs = {
            'id': {
                'read_only': False, 
                'required': False
             },
             'date_created' : {
                'required': False
            },
            'date_modified' : {
                'required': False
            }
        } 
