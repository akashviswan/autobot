from core.models import AWSCloudWatch
from rest_framework import serializers

class AWSCloudWatchSerializer(serializers.ModelSerializer):
    object_type = serializers.SerializerMethodField()
    
    @staticmethod
    def get_object_type(obj):
        return "AWSCloudWatch"

    class Meta:
        model = AWSCloudWatch
        fields = '__all__'
        extra_kwargs = {
            'id': {
                'read_only': False, 
                'required': False
             },
             'date_created' : {
                'required': False
            },
            'date_modified': {
                'required': False
            } 
        }
