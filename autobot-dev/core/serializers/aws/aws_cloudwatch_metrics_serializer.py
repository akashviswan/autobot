from core.models import EC2CloudWatchMetric,ELBCloudWatchMetric,ASGCloudWatchMetric
from rest_framework import serializers

class EC2CloudWatchMetricSerializer(serializers.ModelSerializer):
    object_type = serializers.SerializerMethodField()

    @staticmethod
    def get_object_type(obj):
        return "EC2CloudWatchMetric"
    class Meta:
        model = EC2CloudWatchMetric
        exclude = ('polymorphic_ctype',)
        extra_kwargs = {
            'id': {
                'read_only': False, 
                'required': True
             }
        } 

class ELBCloudWatchMetricSerializer(serializers.ModelSerializer):
    object_type = serializers.SerializerMethodField()

    @staticmethod
    def get_object_type(obj):
        return "ELBCloudWatchMetric"
    class Meta:
        model = ELBCloudWatchMetric
        fields = '__all__'
        extra_kwargs = {
            'id': {
                'read_only': False, 
                'required': True
             }
        } 

class ASGCloudWatchMetricSerializer(serializers.ModelSerializer):
    object_type = serializers.SerializerMethodField()

    @staticmethod
    def get_object_type(obj):
        return "ASGCloudWatchMetric"
    class Meta:
        model = ASGCloudWatchMetric
        fields = '__all__'
        extra_kwargs = {
            'id': {
                'read_only': False, 
                'required': True
             }
        } 
