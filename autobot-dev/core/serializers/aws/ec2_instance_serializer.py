
from core.models import EC2
from rest_framework import serializers

class EC2Serializer(serializers.ModelSerializer):
    object_type = serializers.SerializerMethodField()
    instance_str = serializers.SerializerMethodField()
    def get_instance_str(self, instance):
        return instance.__str__()

    @staticmethod
    def get_object_type(obj):
        return "EC2"

    class Meta:
        model = EC2
        fields = '__all__'


