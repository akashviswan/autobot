from core.models import AWSRegions
from rest_framework import serializers

class AWSRegionsSerializer(serializers.ModelSerializer):
    object_type = serializers.SerializerMethodField()
    
    @staticmethod
    def get_object_type(obj):
        return "AWSRegions"
    class Meta:
        model = AWSRegions
        fields = '__all__'