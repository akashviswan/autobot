from core.models import AWSCredential,AWSRegions
from rest_framework import serializers
from .aws_regions_serializer import AWSRegionsSerializer
class AWSCredentialSerializer(serializers.ModelSerializer):
    object_type = serializers.SerializerMethodField()
    region = AWSRegionsSerializer()
    included_serializers = {
        'loadbalancer': AWSRegionsSerializer,
    }

    @staticmethod
    def get_object_type(obj):
        return "AWSCredential"

    def create(self, validated_data):
        region = validated_data.pop('region')
        try:
            aws_region = AWSRegions.objects.get(code=region.get('code'))
        except AWSRegions.DoesNotExist:
            print("Couldnt find AWS Regions")

        instance = AWSCredential.objects.create(**validated_data)
        instance.region = aws_region
        instance.save()
        return instance

    class Meta:
        model = AWSCredential
        fields = "__all__"
        extra_kwargs = {
            "id": {"read_only": False, "required": False},
            "date_created": {"required": False},
            "date_modified": {"required": False},
        }

