
from core.models import AWSScale , AWSAutoScale
from rest_framework import serializers

class AWSScaleSerializer(serializers.ModelSerializer):
    object_type = serializers.SerializerMethodField()
    
    @staticmethod
    def get_object_type(obj):
        return "AWSScale"
    class Meta:
        model = AWSScale
        fields = '__all__'
        extra_kwargs = {
            'id': {
                'read_only': False, 
                'required': False
             },
             "date_created" : {
                'required': False
             } ,
             "date_modified" : {
                'required': False
             }
        }


class AWSAutoScaleSerializer(serializers.ModelSerializer):
    object_type = serializers.SerializerMethodField()
    
    @staticmethod
    def get_object_type(obj):
        return "AWSAutoScale"
    class Meta:
        model = AWSAutoScale
        fields = '__all__'
        extra_kwargs = {
            'id': {
                'read_only': False, 
                'required': False
             },
             "date_created" : {
                'required': False
             } ,
             "date_modified" : {
                'required': False
             }
        }
