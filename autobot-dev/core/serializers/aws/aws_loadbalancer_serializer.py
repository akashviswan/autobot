from core.models import ELB_Classic
from rest_framework import serializers

class ELB_ClassicSerializer(serializers.ModelSerializer):
    object_type = serializers.SerializerMethodField()

    @staticmethod
    def get_object_type(obj):
        return "ELB_Classic"

    class Meta:
        model = ELB_Classic
        fields = '__all__'
        extra_kwargs = {
            'id': {
                'read_only': False, 
                'required': True
             }
        } 

