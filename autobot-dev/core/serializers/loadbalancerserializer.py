from rest_framework import serializers
from core.models import LoadBalancer,ELB_Classic
from core.serializers.aws.aws_loadbalancer_serializer import ELB_ClassicSerializer
from rest_polymorphic.serializers import PolymorphicSerializer

class LoadBalancerSerializer(serializers.ModelSerializer):

    class Meta:
        model = LoadBalancer
        # fields = '__all__'
        exclude = ('polymorphic_ctype', )
        extra_kwargs = {
            'id': {
                'read_only': False, 
                'required': True
             }
        } 
       

class LoadBalancerPolymorphicSerializer(PolymorphicSerializer):
    model_serializer_mapping = {
        ELB_Classic: ELB_ClassicSerializer
    }