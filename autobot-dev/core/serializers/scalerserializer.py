from rest_framework import serializers
from core.models import Scaler,AzureScaleSet,AWSScale,AWSAutoScale
from core.serializers.azureint import AzureScaleSetSerializer
from core.serializers.aws import AWSScaleSerializer,AWSAutoScaleSerializer
from rest_polymorphic.serializers import PolymorphicSerializer

class ScalerSerializer(serializers.ModelSerializer):

    class Meta:
        model = Scaler
        fields = '__all__'
        exclude = ('polymorphic_ctype', )
        extra_kwargs = {
            'id': {
                'required': False
             },
             "date_created" : {
                'required': False
             } ,
             "date_modified" : {
                'required': False
             } 
        } 

class ScalerPolymorphicSerializer(PolymorphicSerializer):
    model_serializer_mapping = {
        AWSScale: AWSScaleSerializer,
        AzureScaleSet: AzureScaleSetSerializer,
        AWSAutoScale : AWSAutoScaleSerializer
    }
    
