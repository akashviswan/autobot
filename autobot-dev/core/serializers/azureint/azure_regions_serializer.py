from core.models import AzureRegions
from rest_framework import serializers

class AzureRegionsSerializer(serializers.ModelSerializer):
    object_type = serializers.SerializerMethodField()
    
    @staticmethod
    def get_object_type(obj):
        return "AzureRegions"
    class Meta:
        model = AzureRegions
        fields = '__all__'
