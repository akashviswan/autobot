from core.models import AzureScaleSetMetricRule
from rest_framework import serializers

class AzureScaleSetMetricRuleSerializer(serializers.ModelSerializer):
    object_type = serializers.SerializerMethodField()
    
    @staticmethod
    def get_object_type(obj):
        return "AzureScaleSetMetricRule"
    class Meta:
        model = AzureScaleSetMetricRule
        fields = '__all__'
        extra_kwargs = {
            'id': {
                'read_only': False, 
                'required': True
             }
        } 