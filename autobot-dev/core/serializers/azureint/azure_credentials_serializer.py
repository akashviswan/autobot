from core.models import AzureCredential
from rest_framework import serializers


class AzureCredentialSerializer(serializers.ModelSerializer):
    object_type = serializers.SerializerMethodField()

    @staticmethod
    def get_object_type(obj):
        return "AzureCredential"

    class Meta:
        model = AzureCredential
        fields = "__all__"
        extra_kwargs = {
            "id": {"read_only": False, "required": False},
            "date_created": {"required": False},
            "date_modified": {"required": False},
        }

