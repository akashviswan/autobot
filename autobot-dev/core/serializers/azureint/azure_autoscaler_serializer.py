from core.models import AzureScaleSetScale
from rest_framework import serializers

class AzureScaleSetScaleSerializer(serializers.ModelSerializer):
    object_type = serializers.SerializerMethodField()
    
    @staticmethod
    def get_object_type(obj):
        return "AzureScaleSetScale"

    class Meta:
        model = AzureScaleSetScale
        fields = '__all__'