from core.models import AzureVM
from rest_framework import serializers

class AzureVMSerializer(serializers.ModelSerializer):
    object_type = serializers.SerializerMethodField()
    instance_str = serializers.SerializerMethodField()
    
    def get_instance_str(self, instance):
        return instance.__str__()
    @staticmethod
    def get_object_type(obj):
        return "AzureVM"

    class Meta:
        model = AzureVM
        fields = '__all__'