from core.models import AzureScaleSet
from rest_framework import serializers

class AzureScaleSetSerializer(serializers.ModelSerializer):
    object_type = serializers.SerializerMethodField()
    
    @staticmethod
    def get_object_type(obj):
        return "AzureScaleSet"
        
    class Meta:
        model = AzureScaleSet
        fields = '__all__'
        extra_kwargs = {
            'id': {
                'read_only': False, 
                'required': False
             },
             "date_created" : {
                'required': False
             } ,
             "date_modified" : {
                'required': False
             }
        }