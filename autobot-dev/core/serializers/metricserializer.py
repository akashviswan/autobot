from rest_framework import serializers
from core.models import Metric,AzureScaleSetMetricRule,EC2CloudWatchMetric,ELBCloudWatchMetric,CSVMetric,ASGCloudWatchMetric
from core.serializers.azureint import AzureScaleSetMetricRuleSerializer
from core.serializers.aws import EC2CloudWatchMetricSerializer,ELBCloudWatchMetricSerializer,ASGCloudWatchMetricSerializer
from core.serializers.csv import CSVMetricSerializer
from rest_polymorphic.serializers import PolymorphicSerializer

class MetricSerializer(serializers.ModelSerializer):
    polymorphic_serializers = [AzureScaleSetMetricRuleSerializer,EC2CloudWatchMetricSerializer,ELBCloudWatchMetricSerializer,CSVMetricSerializer,ASGCloudWatchMetricSerializer]

    class Meta:
        model = Metric
        fields = '__all__'
        extra_kwargs = {
            'id': {
                'read_only': False, 
                'required': True
             }
        } 

class MetricPolymorphicSerializer(PolymorphicSerializer):
    model_serializer_mapping = {
        AzureScaleSetMetricRule: AzureScaleSetMetricRuleSerializer,
        EC2CloudWatchMetric: EC2CloudWatchMetricSerializer,
        ELBCloudWatchMetric: ELBCloudWatchMetricSerializer,
        CSVMetric : CSVMetricSerializer,
        ASGCloudWatchMetric : ASGCloudWatchMetricSerializer
    }