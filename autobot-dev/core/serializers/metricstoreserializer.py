from rest_framework import serializers
from core.models import MetricStore,ES
from rest_polymorphic.serializers import PolymorphicSerializer
from core.serializers.es.esserializer import ESSerializer

class MetricStoreSerializer(serializers.ModelSerializer):
    object_type = serializers.SerializerMethodField()
    class Meta:
        model = MetricStore
        fields = '__all__'
        extra_kwargs = {
            'id': {
                'read_only': False, 
                'required': True
             }
        } 

    @staticmethod
    def get_object_type(obj):
        return "MetricStore"

class MetricStorePolymorphicSerializer(PolymorphicSerializer):
    model_serializer_mapping = {
        ES: ESSerializer
    }