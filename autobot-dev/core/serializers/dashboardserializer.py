from rest_framework import serializers
from core.models import Dashboard,KibanaDash
from core.serializers.kibana import KibanaDashSerializer
from rest_polymorphic.serializers import PolymorphicSerializer

class DashboardSerializer(serializers.ModelSerializer):
    render = serializers.Field(source='render')

    class Meta:
        model = Dashboard
        fields = 'render'

class DashboardPolymorphicSerializer(PolymorphicSerializer):
    model_serializer_mapping = {
        KibanaDash: KibanaDashSerializer
    }