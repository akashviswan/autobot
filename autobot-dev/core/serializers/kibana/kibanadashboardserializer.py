from core.models import KibanaDash
from rest_framework import serializers

class KibanaDashSerializer(serializers.ModelSerializer):
    
    @staticmethod
    def get_object_type(obj):
        return "KibanaDash"

    class Meta:
        model = KibanaDash
        fields = ["render"]
        extra_kwargs = {
            'id': {
                'read_only': False, 
                'required': True
             }
        } 