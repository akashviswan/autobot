from rest_framework import serializers
from core.models import Monitoring,CSVFile,AWSCloudWatch
from core.serializers.csv import CSVFileSerializer
from core.serializers.aws import AWSCloudWatchSerializer
from rest_polymorphic.serializers import PolymorphicSerializer

class MonitoringSerializer(serializers.ModelSerializer):
    polymorphic_serializers = [CSVFileSerializer,AWSCloudWatchSerializer]

    class Meta:
        model = Monitoring
        fields = '__all__'
        extra_kwargs = {
            'id': {
                'read_only': False, 
                'required': False
             },
             'date_created' : {
                'required': False
            },
            'date_modified': {
                'required': False
            } 
        } 
        

class MonitoringPolymorphicSerializer(PolymorphicSerializer):
    model_serializer_mapping = {
        CSVFile: CSVFileSerializer,
        AWSCloudWatch: AWSCloudWatchSerializer,
        Monitoring : MonitoringSerializer
    }