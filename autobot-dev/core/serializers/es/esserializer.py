from rest_framework import serializers
from core.models import ES
class ESSerializer(serializers.ModelSerializer):
    object_type = serializers.SerializerMethodField()
    class Meta:
        model = ES
        fields = '__all__'
        extra_kwargs = {
            'id': {
                'read_only': False, 
                'required': True
             }
        } 

    @staticmethod
    def get_object_type(obj):
        return "Elastic Search"
