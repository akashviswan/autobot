from rest_framework import serializers
from core.serializers.credentialserializer import CredentialPolymorphicSerializer
from core.serializers.metricserializer import MetricPolymorphicSerializer
from core.serializers.instanceserializer import InstancePolymorphicSerializer
from core.serializers.autoscalerserializer import AutoScalerPolymorphicSerializer
from core.serializers.loadbalancerserializer import LoadBalancerPolymorphicSerializer
from core.serializers.monitoringserializer import MonitoringPolymorphicSerializer
from core.serializers.scalerserializer import ScalerPolymorphicSerializer
from core.serializers.metricstoreserializer import MetricStorePolymorphicSerializer
from core.serializers.dashboardserializer import DashboardPolymorphicSerializer
from core.models import Application,Credential,Metric,Instance,AutoScaler,LoadBalancer,Scaler,Monitoring,MetricStore

class ApplicationSerializer(serializers.ModelSerializer):
    credentials = CredentialPolymorphicSerializer(many=True)
    monitoring = MonitoringPolymorphicSerializer(many=True)
    auto_scalers = AutoScalerPolymorphicSerializer(many=True)
    metrics = MetricPolymorphicSerializer(many=True)
    instances = InstancePolymorphicSerializer(many=True,read_only=True)
    object_type = serializers.SerializerMethodField(read_only=True)
    loadbalancer = LoadBalancerPolymorphicSerializer()
    scaler = ScalerPolymorphicSerializer()
    metric_store = MetricStorePolymorphicSerializer()
    dashboard = DashboardPolymorphicSerializer()
    included_serializers = {
        'loadbalancer': LoadBalancerPolymorphicSerializer,
        'scaler': ScalerPolymorphicSerializer,
        'metric_store' : MetricStorePolymorphicSerializer,
        'dashboard' : DashboardPolymorphicSerializer
    }

    @staticmethod
    def get_object_type(obj):
        return "Application"
    
    class Meta:
        ordering = ['_id']
        model = Application
        exclude = ('polymorphic_ctype', )

    def update(self, instance, validated_data):
        # Update the  instance
        if validated_data.get("name") != None:
            instance.name = validated_data['name']
        if validated_data.get("enable_autoscalers") != None:
            if instance.enable_autoscalers != validated_data['enable_autoscalers'] :
                instance.enable_autoscalers = validated_data['enable_autoscalers']            
                
        # //Monitoring
        if validated_data.get("monitoring") != None:
            monitoring_ids = [item['id'] for item in validated_data['monitoring']]
            for monitoring in instance.monitoring.all():
                if monitoring.id not in monitoring_ids:
                    instance.monitoring.remove(monitoring)
            
            for item in validated_data['monitoring']:
                monitoring = Monitoring.objects.get(pk=item['id'])
                if monitoring:
                    instance.monitoring.add(monitoring)
        
        # LoadBalancer
        if validated_data.get("loadbalancer") != None:
            loadbalancer = validated_data['loadbalancer']
            if instance.loadbalancer == None or loadbalancer['id'] != instance.loadbalancer.id:
                loadbalancer = LoadBalancer.objects.get(pk=loadbalancer['id'])
                if loadbalancer:
                    instance.loadbalancer = loadbalancer

         # //Credentials
        if validated_data.get("credentials") != None:
            credentials_ids = [item['id'] for item in validated_data['credentials']]
            for credential in instance.credentials.all():
                if credential.id not in credentials_ids:
                    instance.credentials.remove(credential)
            
            for item in validated_data['credentials']:
                credential = Credential.objects.get(pk=item['id'])
                if credential:
                    instance.credentials.add(credential)

        if validated_data.get("metrics") != None:
            metrics_ids = [item['id'] for item in validated_data['metrics']]
            for metric in instance.metrics.all():
                if metric.id not in metrics_ids:
                    instance.metrics.remove(metric)
            
            for item in validated_data['metrics']:
                metric = Metric.objects.get(pk=item['id'])
                if metric:
                    instance.metrics.add(metric)

        # Scaler
        if validated_data.get("scaler") != None:
            scaler = validated_data['scaler']
            if instance.scaler == None or scaler['id'] != instance.scaler.id:
                newScaler = Scaler.objects.get(pk=scaler['id'])
                if newScaler:
                    instance.scaler = newScaler
            else:
                for key, value in scaler.items():
                    if key != 'id':
                        setattr(instance.scaler, key, value)
            instance.scaler.save()
        
        # Metric Store
        if validated_data.get("metric_store") != None:
            metric_store = validated_data['metric_store']
            if instance.metric_store == None or metric_store['id'] != instance.metric_store.id:
                metric_store = MetricStore.objects.get(pk=metric_store['id'])
                if metric_store:
                    instance.metric_store = metric_store
                    instance.metric_store.save()

        # AutoScalers
        if validated_data.get("auto_scalers") != None:
            autoscalers_ids = [item['id'] for item in validated_data['auto_scalers']]
            for auto_scaler in instance.auto_scalers.all():
                if auto_scaler.id not in autoscalers_ids:
                    instance.auto_scalers.remove(auto_scaler)
            instance_autoscalers_ids = [item.id for item in instance.auto_scalers.all()]
            for item in validated_data['auto_scalers']:
                if item['id'] not in instance_autoscalers_ids:
                    newAutoScaler = AutoScaler.objects.get(pk=item['id'])
                    if newAutoScaler:
                        instance.auto_scalers.add(newAutoScaler)
                else:
                    newAutoScaler = AutoScaler.objects.get(pk=item['id'])
                    for key, value in item.items():
                        print(key)
                        if key == 'metric':
                            if (not hasattr(newAutoScaler, 'metric')) or  value['id'] != newAutoScaler.metric.id:
                                newMetric = Metric.objects.get(pk=value['id'])
                                if newMetric:
                                    newAutoScaler.metric = newMetric
                            else:
                                newMetric = Metric.objects.get(pk=value['id'])
                                for metrickey, metricvalue in value.items():
                                    setattr(newMetric, metrickey, metricvalue)
                                newAutoScaler.metric = newMetric
                            newAutoScaler.metric.save()
                        elif key == 'metrics':
                            if (not hasattr(newAutoScaler, 'metrics')) or value['id'] != newAutoScaler.metrics.id:
                                newMetric = Metric.objects.get(pk=value['id'])
                                if newMetric:
                                    newAutoScaler.metrics = newMetric
                            else:
                                newMetric = Metric.objects.get(pk=value['id'])
                                for metrickey, metricvalue in value.items():
                                    setattr(newMetric, metrickey, metricvalue)
                                newAutoScaler.metrics = newMetric
                            newAutoScaler.metrics.save()
                        elif key == 'monitoring_engine':
                            if  (not hasattr(newAutoScaler, 'monitoring_engine')) or value['id'] != newAutoScaler.monitoring_engine.id:
                                newMonitoring = Monitoring.objects.get(pk=value['id'])
                                if newMonitoring:
                                    newAutoScaler.monitoring_engine = newMonitoring
                            else:
                                for monitoringkey, monitoringvalue in value.items():
                                    setattr(newAutoScaler.monitoring_engine, monitoringkey, monitoringvalue)
                            newAutoScaler.monitoring_engine.save()
                        elif key != 'id':
                            setattr(newAutoScaler, key, value)
                        else:
                            pass
                    newAutoScaler.save()        
        instance.save()
        return instance