from core.models import AutoScaler,SimpleScaler,PredictiveScaler,ProphetScaler,SQSQueueClearer,AzureScaleSet,Metric,Monitoring
from core.serializers.aws import AWSCredentialSerializer
from core.serializers.azureint import AzureCredentialSerializer
from core.serializers.predictive_scaler import ProphetScalerSerializer
from core.serializers.metricserializer import MetricPolymorphicSerializer
from core.serializers.monitoringserializer import MonitoringPolymorphicSerializer
from rest_framework import serializers
from rest_polymorphic.serializers import PolymorphicSerializer
class AutoScalerSerializer(serializers.ModelSerializer):
    class Meta:
        model = AutoScaler
        fields = '__all__'
        
class SimpleScalerSerializer(serializers.ModelSerializer):
    metric = MetricPolymorphicSerializer()
    monitoring_engine = MonitoringPolymorphicSerializer()
    object_type = serializers.SerializerMethodField()
    class Meta:
        model = SimpleScaler
        fields = '__all__'
        extra_kwargs = {
            'id': {
                'read_only': False, 
                'required': False
             },
             "date_created" : {
                'required': False
             } ,
             "date_modified" : {
                'required': False
             }
        } 
    
    @staticmethod
    def get_object_type(obj):
        return "SimpleScaler"

    def create(self, validated_data):
        metric_data = validated_data.pop('metric')
        monitoring_engine_data = validated_data.pop('monitoring_engine')
        simplescaler = SimpleScaler.objects.create(**validated_data)
        simplescaler.metric = Metric.objects.get(pk=metric_data["id"])
        simplescaler.monitoring_engine = Monitoring.objects.get(pk=monitoring_engine_data["id"])
        return simplescaler

    def update(self, instance, validated_data):
        metric_data = validated_data.pop('metric')
        monitoring_engine_data = validated_data.pop('monitoring_engine')
        instance.priority = validated_data.get('priority', instance.priority)
        instance.metric = Metric.objects.get(pk=metric_data["id"])
        instance.monitoring_engine = Monitoring.objects.get(pk=monitoring_engine_data["id"])
        instance.condition = validated_data.get('condition', instance.condition)
        instance.value = validated_data.get('value', instance.value)
        instance.save()
        return instance

class PredictiveScalerSerializer(serializers.ModelSerializer):
    metrics = MetricPolymorphicSerializer()
    object_type = serializers.SerializerMethodField()
    class Meta:
        model = PredictiveScaler
        fields = '__all__'
        extra_kwargs = {
            'id': {
                'read_only': False, 
                'required': False
             },
            "date_created" : {
                'required': False
             } ,
             "date_modified" : {
                'required': False
             }
        } 

    @staticmethod
    def get_object_type(obj):
        return "PredictiveScaler"
    

    def create(self, validated_data):
        metric_data = validated_data.pop('metrics')
        predictivescaler = PredictiveScaler.objects.create(**validated_data)
        predictivescaler.metrics = Metric.objects.get(pk=metric_data["id"])
        return predictivescaler

    def update(self, instance, validated_data):
        metric_data = validated_data.pop('metric')
        instance.priority = validated_data.get('priority', instance.priority)
        instance.metrics = Metric.objects.get(pk=metric_data["id"])
        instance.fwd_time = validated_data.get('fwd_time', instance.fwd_time)
        instance.value = validated_data.get('value', instance.value)
        instance.save()
        return instance



class AutoScalerPolymorphicSerializer(PolymorphicSerializer):
    model_serializer_mapping = {
        SimpleScaler: SimpleScalerSerializer,
        PredictiveScaler: PredictiveScalerSerializer,
        ProphetScaler: ProphetScalerSerializer
    }
