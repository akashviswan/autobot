from core.models import ProphetScaler,Metric,Monitoring
from rest_framework import serializers
from core.serializers.monitoringserializer import MonitoringPolymorphicSerializer
from core.serializers.metricserializer import MetricPolymorphicSerializer

class ProphetScalerSerializer(serializers.ModelSerializer):
    object_type = serializers.SerializerMethodField()
    metric = MetricPolymorphicSerializer()
    monitoring_engine = MonitoringPolymorphicSerializer()

    @staticmethod
    def get_object_type(obj):
        return "ProphetScaler"

    class Meta:
        model = ProphetScaler
        fields = '__all__'
        extra_kwargs = {
            'id': {
                'read_only': False, 
                'required': False
             },
             "date_created" : {
                'required': False
             } ,
             "date_modified" : {
                'required': False
             }
        } 

    def create(self, validated_data):
        metric_data = validated_data.pop('metric')
        monitoring_engine_data = validated_data.pop('monitoring_engine')
        simplescaler = ProphetScaler.objects.create(**validated_data)
        simplescaler.metric = Metric.objects.get(pk=metric_data["id"])
        simplescaler.monitoring_engine = Monitoring.objects.get(pk=monitoring_engine_data["id"])
        return simplescaler

    def update(self, instance, validated_data):
        metric_data = validated_data.pop('metric')
        monitoring_engine_data = validated_data.pop('monitoring_engine')
        instance.priority = validated_data.get('priority', instance.priority)
        instance.training_data_period = validated_data.get('training_data_period', instance.training_data_period)
        instance.training_data_percent = validated_data.get('training_data_percent', instance.training_data_percent)
        instance.predictive_model = validated_data.get('predictive_model', instance.predictive_model)
        instance.fwd_time = validated_data.get('fwd_time', instance.fwd_time)
        instance.value = validated_data.get('value', instance.value)
        instance.retrain_period = validated_data.get('retrain_period', instance.retrain_period)
        instance.last_train = validated_data.get('last_train', instance.last_train)

        instance.metric = Metric.objects.get(pk=metric_data["id"])
        instance.monitoring_engine = Monitoring.objects.get(pk=monitoring_engine_data["id"])
        instance.save()
        return instance