from rest_framework import serializers
from core.models import Instance,AzureVM,EC2
from core.serializers.azureint import AzureVMSerializer
from core.serializers.aws import EC2Serializer
from rest_polymorphic.serializers import PolymorphicSerializer

class InstanceSerializer(serializers.ModelSerializer):
    class Meta:
        model = Instance
        fields = '__all__'

class InstancePolymorphicSerializer(PolymorphicSerializer):
    model_serializer_mapping = {
        AzureVM: AzureVMSerializer,
        EC2: EC2Serializer
    }