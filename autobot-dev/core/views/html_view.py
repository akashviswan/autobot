from django.http import HttpResponse ,HttpResponseRedirect 
from django.template import loader
from django.shortcuts import render, redirect,get_object_or_404
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from core.models import *
#from core.forms import *
#from core.options import *
from django.forms import inlineformset_factory, modelform_factory
import logging



log = logging.getLogger(__name__)
def index(request):
    template = loader.get_template('core/html/index.html')
    context = {}
    
    return HttpResponse(template.render(context, request,))

def application_index(request):
    template = loader.get_template('core/html/application.html')
    try:
        query = request.GET['x']
        option = request.GET['search_param']
    except:
        return redirect('/applications?search_param=all&x=')
    # if(option == "Server"):
        # application_list = Application.objects.filter(name__startswith=query).order_by('name')
    # elif(option == "SDL"):
        # application_list = Application.objects.filter(Q(sdl__firstname__startswith=query)|Q(sdl__lastname__startswith=query)|Q(sdl__userid__startswith=query)).order_by('code')
    # else:
    application_list = Application.objects.filter(name__startswith=query).order_by('name')
    
    paginator = Paginator(application_list, 25) # Show 25 contacts per page
    page = request.GET.get('page')
    try:
        applications = paginator.page(page)
    except PageNotAnInteger:
        # If page is not an integer, deliver first page.
        applications = paginator.page(1)
    except EmptyPage:
        # If page is out of range (e.g. 9999), deliver last page of results.
        applications = paginator.page(paginator.num_pages)
    context = {
    'search' : "?search_param="+option+"&x="+query , 
    'application_list': applications,
    }
    return HttpResponse(template.render(context, request,)) 
def view_application(request, pk, template_name='core/html/view_application.html'):
    application = get_object_or_404(Application, pk=pk)
    if application.loadbalancer != None:
        application.loadbalancer.update_instances()
    settings = ApplicationForm(instance=application)
    ssf=''
    psf=''
    try:
        ss =SimpleScaler.objects.get(application = application)
        ssf=SimpleScalerForm(instance=ss)
        
        ps =PredictiveScaler.objects.get(application = application)
        psf=PredictiveScalerForm(instance=ps)       
        
    except:
        pass
    #esearch=get_object_or_404(ES, pk=application.elasticSearch_id)
    #es_form=ESForm(instance=esearch)
    if (request.method == 'POST'):
        
        #es=ESForm(request.POST,instance=esearch)
        
        appl=ApplicationForm(request.POST,instance=application)
        #print(es.is_valid())
        if appl.is_valid():
            application=appl.save()
            application.save_base()
            application = get_object_or_404(Application, pk=pk)
        
    
    return render(request, template_name, {'application':application,'settings':settings, 'sform':ssf, 'pform':psf})
def create_application(request,template_name='core/html/add_application.html'):
    #server = get_object_or_404(Server)
    form = AppTypeForm(request.POST or None)
    
    if form.is_valid():
        if (form.cleaned_data.get('appType') == '1'):
            return redirect('/add_aws_application')
        else:
            return redirect('/add_centos7_application')
            
    return render(request, template_name, {'form':form,'tittle':'Add Application'})   

def add_application(request,template_name='core/html/add_application.html'):
    #server = get_object_or_404(Server)
    form = AddApplicationForm(request.POST or None)
    
    if form.is_valid():
        form.save()
        return redirect('/applications/')
            
    return render(request, template_name, {'form':form,'tittle':'Add Application'})     

def create_trigger(request,pk,template_name='core/html/add_application.html'):
    #server = get_object_or_404(Server)
    application = get_object_or_404(Application, pk=pk)
    form = TriggerForm(request.POST or None)
    
    if form.is_valid():
        trig=form.save()
        application.triggers.add(trig)
        return redirect('/application/'+str(application.pk))
            
    return render(request, template_name, {'form':form,'tittle':'Add Trigger'})    

def create_aws_application(request,template_name='core/html/add_aws_application.html'):
    form = ' '
    if request.method == 'GET':
        try:
            step = request.GET['step']
            if (step == '1'):
                cred_form = AWSCredentialForm()
                return render(request, template_name, {'cred_form':cred_form,'tittle':'Provide Application Details'})
            elif (step == '2'):
                pk = request.GET['app']
                app = Application.objects.get(pk=pk)
                form = AWSApplicationMetricForm()
                return render(request, 'core/html/add_application.html', {'form':form,'tittle':'Configure Matrics Collection'})
        except Exception as ex:
            log.error(ex)
            return redirect('/add_aws_application?step=1')
    elif request.method == 'POST':
        try:
            step = request.GET['step']
            if (step == '1'):
            #validate the credentials
            #list the ELB's in AWS Account
            #Select the ELB
                cred_form = AWSCredentialForm(request.POST)
                if cred_form.is_valid():
                    cred=cred_form.save()
                    try:
                        lbs=get_load_balancer_descriptions(cred)
                        slect_app = LBSelect(lbs)

                    except:
                        return redirect('/add_aws_application?step=1')
                    return render(request, 'core/html/add_application.html', {'form':slect_app,'tittle':'Select Application','action':'/add_aws_application/?step=2'})
            elif (step == '2'):
                
                form = LBSelect(request.POST)
                if form.is_valid():
                    log.debug(form.cleaned_data.get('lbsel'))
                    
                    return redirect('/applications')
                
        except Exception as ex:
            log.error(ex)
            return redirect('/add_aws_application?step=1')
    #server = get_object_or_404(Server)
    # form = AppTypeForm(request.POST or None)
    
    # if form.is_valid():
        # if (form.cleaned_data.get('appType') == 1):
            # return redirect('/add_aws_app')
        # else:    
            # return redirect('/add_centos7_app')
            
    return render(request, template_name, {'form':form,'tittle':'Add Application'})    

def delete_application (request,pk,template_name='core/html/confirm_delete.html'):
    app = get_object_or_404(Application, pk=pk)
    if request.method=='POST':
        app.delete()
        return redirect('/applications')
    return render(request, template_name, {'object':app,'tittle':"Delete Application"})

def metrics_index(request):
    template = loader.get_template('core/html/metrics.html')
    try:
        query = request.GET['x']
        option = request.GET['search_param']
    except:
        return redirect('/metrics?search_param=all&x=')
    # if(option == "Server"):
        # application_list = Application.objects.filter(name__startswith=query).order_by('name')
    # elif(option == "SDL"):
        # application_list = Application.objects.filter(Q(sdl__firstname__startswith=query)|Q(sdl__lastname__startswith=query)|Q(sdl__userid__startswith=query)).order_by('code')
    # else:
    object_list = Metric.objects.filter(name__startswith=query).order_by('name')
    
    paginator = Paginator(object_list, 25) # Show 25 contacts per page
    page = request.GET.get('page')
    try:
        objects = paginator.page(page)
    except PageNotAnInteger:
        # If page is not an integer, deliver first page.
        objects = paginator.page(1)
    except EmptyPage:
        # If page is out of range (e.g. 9999), deliver last page of results.
        objects = paginator.page(paginator.num_pages)
    context = {
    'search' : "?search_param="+option+"&x="+query , 
    'object_list': objects,
    }
    return HttpResponse(template.render(context, request,)) 
    
def server_index(request):
    template = loader.get_template('core/html/server.html')
    option=''
    query=''
    try:
        query = request.GET['x']
        option = request.GET['search_param']
        object_list = Server.objects.filter(name__startswith=query).order_by('name')
    except:
        object_list = Server.objects.all().order_by('id')
    # if(option == "Server"):
        # application_list = Application.objects.filter(name__startswith=query).order_by('name')
    # elif(option == "SDL"):
        # application_list = Application.objects.filter(Q(sdl__firstname__startswith=query)|Q(sdl__lastname__startswith=query)|Q(sdl__userid__startswith=query)).order_by('code')
    # else:
    
    
    paginator = Paginator(object_list, 25) # Show 25 contacts per page
    page = request.GET.get('page')
    try:
        objects = paginator.page(page)
    except PageNotAnInteger:
        # If page is not an integer, deliver first page.
        objects = paginator.page(1)
    except EmptyPage:
        # If page is out of range (e.g. 9999), deliver last page of results.
        objects = paginator.page(paginator.num_pages)
    context = {
    'search' : "?search_param="+option+"&x="+query , 
    'object_list': objects,
    }
    return HttpResponse(template.render(context, request,)) 

def add_scaler(request):
    template = loader.get_template('core/html/add_scaler.html')
    context = {}
    
    return HttpResponse(template.render(context, request,))
def add_ss(request, template_name='core/html/add_simple_scaler.html'):
    form = SimpleScalerForm(request.POST or None)
    
    if form.is_valid():
        form.save()
        return redirect('/add_simpleScaler/')
            
    return render(request, template_name, {'form':form,'tittle':'Add Simple Scaler'})
        
    
    #return render(request, template_name, {'application':application,'settings':settings})
	
def add_ps(request, template_name='core/html/add_predictive_scaler.html'):
    form = PredictiveScalerForm(request.POST or None)
    
    if form.is_valid():
        form.save()
        return redirect('/application/')
            
    return render(request, template_name, {'form':form,'tittle':'Add Predictive Scaler'})
    

