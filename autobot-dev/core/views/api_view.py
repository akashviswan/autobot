from restless.modelviews import ListEndpoint, DetailEndpoint, Endpoint
from rest_framework.decorators import api_view, permission_classes
from pprint import pprint
from core.models import *
from restless.models import serialize
from django.shortcuts import render, redirect, get_object_or_404
from django.http import HttpResponse, HttpResponseRedirect
from django.utils.html import escape
from django.utils import timezone
from django.http import JsonResponse
from rest_framework import generics
from rest_framework.views import APIView
from django.db.models import Count
from core.models.azureint import *
from core.models.aws import *
from core.serializers.credentialserializer import CredentialPolymorphicSerializer
from core.serializers.metricserializer import MetricPolymorphicSerializer
from core.serializers.instanceserializer import InstancePolymorphicSerializer
from core.serializers.autoscalerserializer import AutoScalerPolymorphicSerializer
from core.serializers.loadbalancerserializer import LoadBalancerPolymorphicSerializer
from core.serializers.monitoringserializer import MonitoringPolymorphicSerializer
from core.serializers.scalerserializer import ScalerPolymorphicSerializer
from core.serializers.metricstoreserializer import MetricStorePolymorphicSerializer
from core.serializers.azureint.azure_regions_serializer import AzureRegionsSerializer
from core.serializers.aws.aws_regions_serializer import AWSRegionsSerializer
import json
from rest_framework import exceptions, mixins
from rest_framework.viewsets import GenericViewSet

from core.serializers import ApplicationSerializer
from rest_framework.authentication import BasicAuthentication, SessionAuthentication
from rest_framework.permissions import IsAuthenticated
from rest_framework.viewsets import ModelViewSet
from rest_framework.response import Response

import datetime
import pandas as pd
from io import BytesIO as IO


class AWSCredentialList(ListEndpoint):
    model = Credential


class AWSCredentialDetail(DetailEndpoint):
    model = Credential


class GetCredentialData(Endpoint):
    def get(self, request, pk):
        cred = Credential.objects.get(pk=pk)
        return serialize(cred, include=["name"])


class ServerList(ListEndpoint):
    model = Instance


class ServerDetail(DetailEndpoint):
    model = Instance


class GetServerData(Endpoint):
    def get(self, request):
        serv = Instance()
        return serialize(serv)


def GetAppMetrics_old(request, pk):
    application = get_object_or_404(Application, pk=pk)
    with open(
        "/home/autobot/my_env/autobot_dev/tsv/" + application.name, "rb"
    ) as myfile:
        response = HttpResponse(myfile, content_type="text/csv")
        response["Content-Disposition"] = "attachment; filename=application.tsv"
        return response


def GetAppMetrics(request, pk):
    application = get_object_or_404(Application, pk=pk)

    monitoring = application.monitoring_set.all()[0]

    start_time = datetime.datetime.utcnow() - datetime.timedelta(days=5)
    end_time = datetime.datetime.utcnow()
    metric_data = monitoring.collect_all_metrics(start_time, end_time)
    data = []

    for md in metric_data:
        dp = {x["Name"]: x["Value"] for x in md["Metric"]}
        dp["Timestamp"] = md["Timestamp"]
        data.append(dp)
    df = pd.DataFrame(data)
    #    df = pd.from_dict(metric_data,orient='index')
    df["Timestamp"] = pd.to_datetime(df.Timestamp)
    df["date"] = df["Timestamp"].dt.strftime("%Y-%m-%dT%H:%M:%S.000Z")
    df.drop("Timestamp", axis=1, inplace=True)
    df.set_index("date", inplace=True)
    df.reset_index(inplace=True)
    response = HttpResponse(df.to_csv(sep="\t", index=False), content_type="text/csv")
    response["Content-Disposition"] = "attachment; filename=application.tsv"
    return response


def GetAppsCount(request):
    count = Application.objects.count()
    response = JsonResponse({"Applications Count": count})
    return response


class ApplicationList(Endpoint):
    permission_classes = (IsAuthenticated,)
    def get(self, request):
        applications = Application.objects.all()
        return serialize(applications)


class getApplications(generics.ListCreateAPIView):
    permission_classes = (IsAuthenticated,)
    queryset = Application.objects.all()
    serializer_class = ApplicationSerializer


class singleApplication(ModelViewSet):
    permission_classes = (IsAuthenticated,)
    queryset = Application.objects.all()
    serializer_class = ApplicationSerializer


class MetricsAPI(generics.ListCreateAPIView):
    permission_classes = (IsAuthenticated,)
    queryset = Metric.objects.all()
    serializer_class = MetricPolymorphicSerializer


class LoadbalancerAPI(ModelViewSet):
    permission_classes = (IsAuthenticated,)
    queryset = LoadBalancer.objects.all()
    serializer_class = LoadBalancerPolymorphicSerializer


class CredentialsAPI(generics.ListCreateAPIView):
    permission_classes = (IsAuthenticated,)
    queryset = Credential.objects.all()
    serializer_class = CredentialPolymorphicSerializer


class AzureRegionsAPI(generics.ListCreateAPIView):
    permission_classes = (IsAuthenticated,)
    queryset = AzureRegions.objects.all()
    serializer_class = AzureRegionsSerializer

class AWSRegionsAPI(generics.ListCreateAPIView):
    permission_classes = (IsAuthenticated,)
    queryset = AWSRegions.objects.all()
    serializer_class = AWSRegionsSerializer

class getScalerObject(ModelViewSet):
    permission_classes = (IsAuthenticated,)
    queryset = Scaler.objects.all()
    serializer_class = ScalerPolymorphicSerializer


class getAutoScalerObject(ModelViewSet):
    permission_classes = (IsAuthenticated,)
    queryset = AutoScaler.objects.all()
    serializer_class = AutoScalerPolymorphicSerializer


class MetricStoreAPI(ModelViewSet):
    permission_classes = (IsAuthenticated,)
    queryset = MetricStore.objects.all()
    serializer_class = MetricStorePolymorphicSerializer


class MonitoringAPI(ModelViewSet):
    permission_classes = (IsAuthenticated,)
    queryset = Monitoring.objects.all()
    serializer_class = MonitoringPolymorphicSerializer


class DashboardData(APIView):
    permission_classes = (IsAuthenticated,)
    def get(self, request, format=None):
        applicationCount = Application.objects.count()
        metrics_count = Metric.objects.count()
        instance_count = Instance.objects.count()
        value = Instance.objects.values("status").annotate(Count("id"))
        content = {
            "application_count": applicationCount,
            "metrics_count": metrics_count,
            "instance_count": instance_count,
            "instance_status_count": value,
        }
        return Response(content)


# list loadbalancers
def list_loadbalancers(credential):
    return credential.list_load_balancers()


@api_view(["GET"])
@permission_classes((IsAuthenticated, ))
def getCreateAppTemplate(request):
    page1 = {
        "page": "Cloud",
        "pagenumber" : 0,
        "page_id" : "credentials",
        "models" : [
            "AzureRegions",
            "AWSRegions"
        ],
        "fields": [
                {
                    "type": "Dropdown",
                    "key": "resourcetype",
                    "label": "Select Cloud",
                    "values": [
                        {"value": "AWSCredential", "label": "AWS"},
                        {"value": "AzureCredential", "label": "Azure"},
                        {"value": "KubeClientConfig", "label": "Kubernetics"},
                    ],
                    "dropdown_value_key": "value",
                    "isrequired": True,
                    "fields": getFormFields([AWSCredential,AzureCredential,KubeClientConfig],{
                        "exclude" : None,
                        "include" : None,
                        "isarray" : False
                    })
                }
         ]
    }
    page2 = {
        "page": "Application",
        "pagenumber" : 1,
        "page_id" : "application",
        "models" : [],
        "fields":  getFormFields([Application],{
            "include" : [
                "name",
                "loadbalancer"
            ],
            "exclude" : [
                "autoscaler_taskid"
            ],
            "isarray" : True
        })
    }
    page3 = {
        "page": "Scaler",
        "pagenumber" : 2,
        "page_id" : "scaler",
        "models" : [],
        "fields": [{
                    "type": "Dropdown",
                    "key": "resourcetype",
                    "label": "Select Scaler",
                    "values": [
                        {"value": "AWSScale", "label": "AWS Scale"},
                        {"value": "AWSAutoScale", "label": "AWS Auto Scale"},
                        {"value": "AzureScaleSet", "label": "Azure Scale Set"},
                        {"value": "ReplicationControllerScaler", "label": "Replication Controller Scaler"},
                        {"value": "KubernetesDeployment", "label": "Kubernetes Deployment"},
                    ],
                    "dropdown_value_key": "value",
                    "isrequired": True,
                    "fields": getFormFields([AWSScale,
                    AWSAutoScale,
                    AzureScaleSet
                    ,ReplicationControllerScaler,
                    KubernetesDeployment],{
                        "exclude" : None,
                        "include" : None,
                        "isarray" : False
                    })
                }]
    }
    page4 = {
        "page": "Monitoring & Metrics",
        "pagenumber" : 3,
        "page_id" : "monitoringandmetrics",
        "models" : ["Metric"],
        "fields": {
           "Monitoring" : {
            "fields": getFormFields([Monitoring],{
                "exclude" : None,
                "include" : None,
                "isarray" : True
            }),
            "section_label" : "Monitoring Engine",
            "section_datasource_key" : "monitoring",

        },
        "Metrics" : {
            "fields": getFormFields([Application],{
                "exclude" : None,
                "include" : [
                    "metrics"
                ],
                "isarray" : True
            }),
            "section_label" : "",
            }
        }
    }
    page5 = {
        "page": "AutoScaler",
        "pagenumber" : 4,
        "page_id" : "autoscaler",
        "models" : ["Metric","Monitoring"],
        "fields": [{
                    "type": "Dropdown",
                    "key": "resourcetype",
                    "label": "Choose AutoScaler Type",
                    "values": [
                        {"value": "SimpleScaler", "label": "SimpleScaler"},
                        {"value": "PredictiveScaler", "label": "PredictiveScaler"},
                        {"value": "ProphetScaler", "label": "ProphetScaler"},
                        {"value": "AzureScaleSet", "label": "AzureScalesetScaler"},
                        {"value": "SQSQueueClearer", "label": "SQSQueueClearer"},
                    ],
                    "dropdown_value_key": "value",
                    "isrequired": True,
                    "fields": getFormFields([SimpleScaler,PredictiveScaler,ProphetScaler,AzureScaleSet,SQSQueueClearer],{
                        "exclude" : None,
                        "include" : None,
                        "isarray" : False
                    })
                }]
    }
    return Response([page1,page2,page3,page4,page5])

def getFormFields(models,attributes):
    formfields = {}
    formfieldsArray = []
    for model in models:
        modelfields = []
        mapping = {
                "CharField" : "TextField",
                "Choices" : "Dropdown",
                "ForeignKey": "Dropdown",
                "ManyToManyField" : "DropdownMulti",
                "IntegerField" : "TextField",
                "BooleanField" : "CheckBox",
                "DecimalField" : "TextField",
                "TextField" : "TextField",
                "DateTimeField" : "DateTimePicker",
                "BinaryField" : "TextField",
                "EncryptedDictField" : "TextView"
        }
        
        for modelfield in model._meta.get_fields(include_parents=True, include_hidden=False):
            if (modelfield.auto_created is not True and  modelfield.name != "polymorphic_ctype"  and modelfield.name != "date_created" and modelfield.name != "date_modified"):
                if  ((attributes is None) or (attributes["include"] is None) or (attributes["include"] is not None and modelfield.name in attributes["include"])) and ((attributes is None) or (attributes["exclude"] is None) or (attributes["exclude"] is not None and modelfield.name not in attributes["exclude"])):
                        fielddata = {}
                        fielddata["key"] = modelfield.name
                        fielddata["isrequired"] = True
                        fielddata["type"] = mapping[modelfield.get_internal_type()]
                        if (modelfield.get_internal_type() == "IntegerField") :
                            fielddata["pattern"] = "[0-9]*"
                        
                        elif (modelfield.get_internal_type() == "DecimalField"):
                            fielddata["pattern"] = "[0-9]+(\.[0-9]{0,2})?"

                        if modelfield.get_internal_type() == "ForeignKey":
                            fielddata["label"] = modelfield.name.title()
                            fielddata["model"] = modelfield.related_model.__name__
                            if hasattr(modelfield.related_model, 'get_ui_label_keys'):
                                fielddata["dropdown_label_key"] = modelfield.related_model.get_ui_label_keys()
                            if hasattr(modelfield.related_model, 'get_ui_value_keys'):
                                fielddata["dropdown_value_key"] = modelfield.related_model.get_ui_value_keys()
                        elif modelfield.get_internal_type() == "CharField":
                            fielddata["label"] = modelfield.verbose_name.title()
                            if hasattr(modelfield, 'choices') and modelfield.choices:
                                fielddata["type"] = mapping["Choices"]
                                fielddata["values"] = getChoicesAsArray(modelfield.choices)
                                fielddata["dropdown_label_key"] = "label"
                                fielddata["dropdown_value_key"] = "value"
                                fielddata["label"] = modelfield.verbose_name.title()

                        elif modelfield.get_internal_type() == "ManyToManyField":
                            fielddata["label"] = modelfield.name.title()
                            fielddata["model"] = modelfield.related_model.__name__
                            if hasattr(modelfield.related_model, 'get_ui_label_keys'):
                                fielddata["dropdown_label_key"] = modelfield.related_model.get_ui_label_keys()
                            if hasattr(modelfield.related_model, 'get_ui_value_keys'):
                                fielddata["dropdown_value_key"] = modelfield.related_model.get_ui_value_keys()
                        else:
                            fielddata["label"] = modelfield.verbose_name.title()
                        modelfields.append(fielddata)
        formfields[model.__name__] = modelfields
        formfieldsArray.extend(modelfields)
    if attributes is not None and attributes["isarray"] is True:
        return formfieldsArray
    return formfields
        

def getChoicesAsArray(choices):
    choicesArray = [] 
    for value, label in choices:
        choices_dict = {}
        choices_dict["value"] = value
        choices_dict["label"] = label
        choicesArray.append(choices_dict)
    return choicesArray

class GetLoadBalancerFromCredentialViewSet(mixins.CreateModelMixin, GenericViewSet):
    permission_classes = (IsAuthenticated,)
    def create(self, request):
        serializer = CredentialPolymorphicSerializer(data=request.data)
        if (serializer.initial_data["resourcetype"]) == "AWSCredential":
            credentials = AWSCredential(
                name=serializer.initial_data["name"],
                key_id=serializer.initial_data["key_id"],
                key=serializer.initial_data["key"],
                region=AWSRegions.objects.get(id=serializer.initial_data["region"]["id"]),
            )
            loadbalancers = credentials.list_load_balancers()
            lbserializer = LoadBalancerPolymorphicSerializer(loadbalancers, many=True)
            return Response(lbserializer.data)
        elif (serializer.initial_data["resourcetype"]) == "AzureCredential":
            credentials = AzureCredential(
                name=serializer.initial_data["name"],
                subscription_id=serializer.initial_data["subscription_id"],
                client_id=serializer.initial_data["client_id"],
                secret=serializer.initial_data["secret"],
                tenant=serializer.initial_data["tenant"],
                region=serializer.initial_data["region"],
            )
            loadbalancers = credentials.list_load_balancers()
            lbserializer = LoadBalancerPolymorphicSerializer(loadbalancers, many=True)
            return Response(lbserializer.data)
        else:
            return Response(status=404, data={'status':'false',
            'message':"Couldn't find any application corresponding to credentials provided"})

class createApplication(mixins.CreateModelMixin, GenericViewSet):
    permission_classes = (IsAuthenticated,)
    def create(self, request):
        applicationname = request.data["name"]
        requestlb = request.data["loadbalancer"]
        credentials = request.data["credentials"]
        serializer = CredentialPolymorphicSerializer(data=credentials)
        if serializer.is_valid():
            credentialObj = serializer.save()
        else:
            print(serializer.is_valid())
            print(credentials)

        if requestlb["resourcetype"] == "ELB_Classic":
            loadbalancer = ELB_Classic.objects.get_or_create(lb=requestlb["lb"])[0]
            

        app = Application.objects.get_or_create(
            name=applicationname, loadbalancer=loadbalancer
        )[0]
        app.credentials.add(credentialObj)

        applicationSerializer = ApplicationSerializer(app)
        return Response(applicationSerializer.data)