from core.models import *
import logging
log = logging.getLogger(__name__)

def send_metric_to_es (server,start_time,end_time):
    out = {}
    try:
        for metric in server.application.metrics.all():
            dp=server.get_metrics(datetime.datetime.utcnow() - datetime.timedelta(days=1),datetime.datetime.utcnow(),metric)
            doc_type = server.to_class_name()

            for i in dp:
                out = {}
                out.update({'Host':str(server)})
                out.update({'Type':server.to_class_name()+'-'+str(metric)})
                out.update({"Metrics":i})
                out.update({"Timestamp": i['Timestamp']})
                #id = str(server)+'-'+'CPUUtilization'+'-'+i['Timestamp'].strftime('%s')
                server.application.elasticSearch.insert_es(doc_type,data=out)
    except Exception as ex:
        log.error(ex)
        return -1
    else:
        return (0)
    finally:
        pass
def send_bulk_metric_to_es (server,start_time,end_time):
    out = {}
    try:
        for metric in server.application.metrics.all():
            dp=server.get_metrics(start_time,end_time,metric)
            doc_type = server.to_class_name()
            bulk = []
            for i in dp:
                out = {
                "_index":server.application.elasticSearch.index ,
                "_type":server.to_class_name(),
                "_source":{
                            "Host":str(server),
                            "Type":server.to_class_name()+'-'+str(metric),
                            "Metrics":i,
                            "Timestamp":i['Timestamp'],
                            },
                        }
                
                bulk.append(out)
            #print (bulk)
            server.application.elasticSearch.bulk_insert_es(data=bulk)
    except Exception as ex:
        log.error(ex)
        return -1
    else:
        return (0)
    finally:
        pass
def update_bulk_metric_to_es (server):
    out = {}
    start_time = datetime.datetime.utcnow()- datetime.timedelta(days=5)
    end_time = datetime.datetime.utcnow()
    try:
        start_time = ES_Transaction.objects.filter(object_name=str(server)).latest('date_created').date_created
    except:
        pass
    try:
        for metric in server.application.metrics.all():
            dp=server.get_metrics(start_time,end_time,metric)
            doc_type = server.to_class_name()
            bulk = []
            for i in dp:
                out = {
                "_index":server.application.elasticSearch.index ,
                "_type":server.to_class_name(),
                "_source":{
                            "Host":str(server),
                            "Type":server.to_class_name()+'-'+str(metric),
                            "Metrics":i,
                            "Timestamp":i['Timestamp'],
                            },
                        }
                
                bulk.append(out)
            #print (bulk)
            server.application.elasticSearch.bulk_insert_es(data=bulk)
        ES_Transaction.objects.create(object_type=server.to_class_name(),object_name=str(server))
    except Exception as ex:
        log.error(ex)
        return -1
    else:
        return (0)
    finally:
        pass
def send_bulk_lb_metric_to_es (loadbalancer,start_time,end_time):
    out = {}
    try:
        for metric in loadbalancer.metrics.all():
            dp=loadbalancer.get_metrics(start_time,end_time,metric)
            doc_type = loadbalancer.to_class_name()
            bulk = []
            for i in dp:
                out = {
                "_index":loadbalancer.application_set.all()[0].elasticSearch.index ,
                "_type":loadbalancer.to_class_name(),
                "_source":{
                            "LoadBalancer":str(loadbalancer),
                            "Type":loadbalancer.to_class_name()+'-'+str(metric),
                            "Metrics":i,
                            "Timestamp":i['Timestamp'],
                            },
                        }
                
                bulk.append(out)
            #print (bulk)
            loadbalancer.application_set.all()[0].elasticSearch.bulk_insert_es(data=bulk)
    except Exception as ex:
        log.error(ex)
        return -1
    else:
        return (0)
    finally:
        pass
def update_bulk_lb_metric_to_es (loadbalancer,start_time,end_time):
    out = {}
    try:
        start_time = ES_Transaction.objects.filter(object_name=str(server)).latest('date_created').date_created
    except:
        pass
    try:
        for metric in loadbalancer.metrics.all():
            dp=loadbalancer.get_metrics(start_time,end_time,metric)
            doc_type = loadbalancer.to_class_name()
            bulk = []
            for i in dp:
                out = {
                "_index":loadbalancer.application_set.all()[0].elasticSearch.index ,
                "_type":loadbalancer.to_class_name(),
                "_source":{
                            "LoadBalancer":str(loadbalancer),
                            "Type":loadbalancer.to_class_name()+'-'+str(metric),
                            "Metrics":i,
                            "Timestamp":i['Timestamp'],
                            },
                        }
                
                bulk.append(out)
            #print (bulk)
            loadbalancer.application_set.all()[0].elasticSearch.bulk_insert_es(data=bulk)
        ES_Transaction.objects.create(object_type=server.to_class_name(),object_name=str(server))
    except Exception as ex:
        log.error(ex)
        return -1
    else:
        return (0)
    finally:
        pass
def get_latest_status (application):
    try:
        start_time = datetime.datetime.utcnow()- datetime.timedelta(seconds=600)
        end_time = datetime.datetime.utcnow()
        servers=application.server_set.all()
        head=[]
        num= len(servers)
        index=list(range(num))
        for i in application.metrics.all():
            head.append(i.name)

            
        data=pd.DataFrame(index= index ,columns =head)    
        #data = data.fillna(0)    
        for server in servers:
            i =0
            for metric in application.metrics.all():
                dp=server.get_metrics(start_time,end_time,metric)
                try:
                    data[metric.name][i]=dp[0]['Average']
                except:
                    data[metric.name][i]=0.0
            i=i+1

        state=data.mean(axis=0).to_dict()
        for metric in application.loadbalancer.metrics.all():
            dp=application.loadbalancer.get_metrics(start_time,end_time,metric)
            try:
                state.update({metric.name:dp[0]['Average']})
            except:
                state.update({metric.name:0})
    except Exception as ex:
        log.error(ex)
        return -1
    else:
        return (state)
    finally:
        pass
def scale_aws_application(application):
    state=[]
    current_status = application.get_latest_status()
    decision_tree = application.decision_tree_fit()
    for metric in application.metrics.all():
        try:
            state.append(current_status[metric.name])
        except:
            pass
    np.asarray(state)
    state=np.asarray(state)
    scale=int(decision_tree.predict(state.reshape(1, -1)))
    print(scale)
    application.dryRun=False
    if (scale == 1):
        application.set_instance_count(application.server_set.all().count()+1)
    if (scale == 3):
        application.set_instance_count(application.server_set.all().count()-1)
		
def scale_status():
	import datetime
	s=SimpleScaler.objects.all()[0]
	instance_count = s.calculate_instance_count()
	dir='/home/autobot/my_env/autobot_dev/logs/appstatus/'
	with open(dir+str(s.application)+'_scale.dat', 'a+') as f:
		f.write(str(datetime.datetime.now())+','+str(instance_count))
	return instance_count