from django.core.management.base import BaseCommand, CommandError
from core.models import *
import datetime
class Command(BaseCommand):
    args = ''
    help = 'Export data to remote server'

    def handle(self, *args, **options):
        # do something here
        app = Application.objects.get(name= 'AWS Test App 2')
        n,data=app.eval_autoscalers()
        mon = app.monitoring.all()[0]
        for metric in app.metrics.filter(name__in=['Response Time','Healthy Hosts','Instance Count']):
            val=mon.get_latest_metric(metric,app)
            data.append({'Timestamp':datetime.datetime.now(),'Metric':{'Name':metric.name,'Value':val}})
        #auto_scalers = app.auto_scalers.all()


        #app.scaler.set_instance_count(n)
        dir='/home/centos/'
        with open(dir+'scale.log', 'a+') as f:
            f.write(str(data)+'\n')
        print(n,data)