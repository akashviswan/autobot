from django.core.management.base import BaseCommand, CommandError
from core.models import *
import datetime
class Command(BaseCommand):
    args = ''
    help = 'Export data to remote server'

    def handle(self, *args, **options):
        # do something here
        
        s=SimpleScaler.objects.all()[0]
        s_metric_value = s.monitoring_engine.get_latest_metric(s.metric)
        p=PredictiveScaler.objects.all()[0]
        p_metric_value = p.predicted_load()
        instance_count = s.calculate_instance_count()
        p_inst_count = p.calculate_instance_count()
        dir='/home/autobot/my_env/logs/appstatus/'
        with open(dir+str(s.application)+'_scale.dat', 'a+') as f:
            f.write(str(datetime.datetime.now())+','+str(instance_count)+','+str(p_inst_count)+','+str(s_metric_value)+','+str(p_metric_value)+'\n') 
        return(str(instance_count)+','+str(p_inst_count))