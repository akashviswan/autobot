from fernet_fields import EncryptedField
from djongo import models
from ast import literal_eval

class EncryptedChoicesField(EncryptedField, models.char):
    def to_python(self, value):
        if isinstance(value,str):
            dict_value = literal_eval(value)
            if dict_value:
                value = dict_value
        return super().to_python(value)
        
    def get_internal_type(self):
        return "EncryptedDictField"
