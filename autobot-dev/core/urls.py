from django.conf.urls import url
from django.urls import path
from core.views import html_view,auth_view,api_view
from rest_framework import routers

router = routers.SimpleRouter()
router.register(r'user/applications',  api_view.singleApplication)
router.register(r'user/loadbalancers',  api_view.LoadbalancerAPI)
router.register(r'user/scalers',  api_view.getScalerObject)
router.register(r'user/monitorings',  api_view.MonitoringAPI)
router.register(r'user/autoscalers',  api_view.getAutoScalerObject)
router.register(r'user/metricstores',  api_view.MetricStoreAPI)
router.register(r'getLoadBalancers', api_view.GetLoadBalancerFromCredentialViewSet,basename="Credential")
router.register(r'createApplication', api_view.createApplication,basename="Application")

urlpatterns = [
    url(r'^index/', html_view.index),
    url('user/authenticate/', auth_view.login),
    path('user/metrics', api_view.MetricsAPI.as_view()),
    path('user/credentials', api_view.CredentialsAPI.as_view()),
    path('azureregions',api_view.AzureRegionsAPI.as_view()),
    path('awsregions',api_view.AWSRegionsAPI.as_view()),
    path(r'user/dashboard',  api_view.DashboardData.as_view()),
    path(r'createAppTemplate',api_view.getCreateAppTemplate),
]+router.urls