#from kubernetes import client, config.

from kubernetes import client
from kubernetes.client import Configuration
from kubernetes.config import kube_config

#from kubernetes.config import

def _setreplicationcontrollerount():
        configuration = {'apiVersion': 'v1', 'clusters': [{'cluster': {'certificate-authority-data': '<certificate_authority-data>', 'server': 'https://<ip address>:6443'}, 'name': 'kubernetes'}], 'contexts': [{'context': {'cluster': 'kubernetes', 'user': 'kubernetes-admin'}, 'name': 'kubernetes-admin@kubernetes'}], 'current-context': 'kubernetes-admin@kubernetes', 'kind': 'Config', 'preferences': {}, 'users': [{'name': 'kubernetes-admin', 'user': {'client-certificate-data': '<client-certificate-data>', 'client-key-data': '<client-key-data>'}}]}
        #config.load_kube_config(config_file='C:/Users/U45647/Documents/config.yaml')
        #config.load_and_set(client_configuration=configuration)
        #kub=client.KubeConfigLoader(configuration)
        #kub = config.

        a = client.CoreV1Api()
        k8_loader = kube_config.KubeConfigLoader(config_dict=configuration,active_context='kubernetes-admin@kubernetes', config_persister=True)

        #kube_config.ConfigNode('kube-config', configuration)
        #call_config = type.__call__(Configuration)
        #k8_loader.load_and_set(call_config)
        #Configuration.set_default(call_config)
        return a

api_instance = _setreplicationcontrollerount()
name = 'hello-rc'
namespace = 'default'
body = {'spec': {'replicas': 9}}
pretty = 'True'
response = api_instance.patch_namespaced_replication_controller_scale(name, namespace, body)
print(response)