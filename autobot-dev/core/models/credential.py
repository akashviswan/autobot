from djongo import models
from polymorphic.models import PolymorphicModel
from django.utils import timezone
from fernet_fields import EncryptedCharField


class Credential(PolymorphicModel):
    name = models.CharField(max_length=25)
    date_created = models.DateTimeField(editable=False)
    date_modified = models.DateTimeField()

    def save(self, *args, **kwargs):
        if not self.id:
            self.date_created = timezone.now()
            self.date_modified = timezone.now()
        return super(Credential, self).save(*args, **kwargs)

    def __str__(self):  # __unicode__ on Python 2
        return self.name


class SSHCredential(Credential):
    username = EncryptedCharField(max_length=15)
    password = EncryptedCharField(max_length=20)
    sshKey = EncryptedCharField(max_length=200)

    def __init__(self, username, password, sshKey, name):
        super(SSHCredential, self).__init__()
        self.username = username
        self.password = password
        self.sshKey = sshKey
        self.name = name
        self.date_created = timezone.now()
        self.date_modified = timezone.now()



