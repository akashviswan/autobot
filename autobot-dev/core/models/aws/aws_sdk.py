import boto3
import datetime

from botocore.exceptions import ClientError
from .exceptions import *
#from .ec2_instance import EC2
from .aws_loadbalancer import ELB_Classic
import logging

logger = logging.getLogger(__name__)


# low level function for listing LoadBalncers
def _get_load_balancers_descriptions(key_id, key, region):
    lb_names = []
    try:
        elb = boto3.client('elb'
                           , aws_access_key_id=key_id
                           , aws_secret_access_key=key
                           , region_name=region)
        lbd = elb.describe_load_balancers()['LoadBalancerDescriptions']

        for lb in lbd:
            lb_names.append(lb['LoadBalancerName'])

    except Exception as ex:
        logger.exception(ex)
        raise ex

    if len(lb_names) == 0:
        raise NoELBFound
    logger.debug('Listing ELB Classics:', lb_names)
    return (lb_names)

def get_load_balancers_descriptions(credential):
        key_id = credential.key_id
        key = credential.key
        region = credential.region.code
        lbs = _get_load_balancers_descriptions(key_id, key, region)
        elb_classic = []
        for lb in lbs:
            elb_classic.append(ELB_Classic(lb=lb))
        return (elb_classic)

# low level function for creating EC2 Instances
def _create_instances(count, dryRun, amiId, key_pair, SecurityGroupId, instance_type, SubnetId, key_id, key, region):
    try:
        ec2 = boto3.client('ec2'
                           , aws_access_key_id=key_id
                           , aws_secret_access_key=key
                           , region_name=region)
        instances = ec2.run_instances(DryRun=dryRun
                                      , ImageId=amiId
                                      , MinCount=count
                                      , MaxCount=count
                                      , KeyName=key_pair
                                      , SecurityGroupIds=[SecurityGroupId, ]
                                      , InstanceType=instance_type
                                      , Placement={
                'Tenancy': 'default',
            }
                                      , SubnetId=SubnetId
                                      , DisableApiTermination=False
                                      , InstanceInitiatedShutdownBehavior='terminate')
        instance_ids = [x['InstanceId'] for x in instances['Instances']]
    except Exception as ex:
        logger.exception(ex)
        raise ex
    else:
        if len(instance_ids) != count:
            logger.warning("Requested creation of ", count, "instances , only created only ", len(instance_ids),
                           "instances")
        logger.info("Instances Created:" + str(instance_ids))
        logger.debug("Instances Created:" + str(instances))
        return (instances)
    finally:
        pass


# Low level function for terminating instances
def _terminate_instances(instance_ids, key_id, key, region):
    try:
        ec2 = boto3.resource('ec2'
                             , aws_access_key_id=key_id
                             , aws_secret_access_key=key
                             , region_name=region)
        ret = ec2.instances.filter(InstanceIds=[instance for instance in instance_ids]).terminate()
        instance_ids = [x['InstanceId'] for x in ret[0]['TerminatingInstances']]
    except Exception as ex:
        logger.exception(ex)
        raise ex
    else:
        logger.info("Instances Deleted:" + str(instance_ids))
        logger.debug("Instances Deleted:" + str(ret))
        return (ret)
    finally:
        pass
    return (ret)





def get_metric(obj, monitoring, aws_credential, metric,
               start_time=(datetime.datetime.utcnow() - datetime.timedelta(days=1)),
               end_time=datetime.datetime.utcnow()):
    cw = boto3.client('cloudwatch'
                      , aws_access_key_id=aws_credential.key_id
                      , aws_secret_access_key=aws_credential.key
                      , region_name=aws_credential.region.code)
    response = cw.get_metric_statistics(Period=monitoring.period
                                        , StartTime=start_time
                                        , EndTime=end_time
                                        , MetricName=str(metric.value)
                                        , Namespace=metric.namespace
                                        , Statistics=[str(metric.aggragation), ]
                                        , Dimensions=[{'Name': str(metric.dimensions)
                                                          , 'Value': str(obj), }, ], )
    datapoints = response["Datapoints"]
    # reformating returned data to format {Timestamp:<Timestamp>,Metric:{Value:<value>,Unit:<unit>,Name:<MetricName>}}
    out = list(map(lambda data: {'Timestamp': data['Timestamp'],
                                 'Metric': {'Value': data[str(metric.aggragation)],
                                            'Unit': data['Unit'],
                                            'Name': str(metric.name)
                                            }
                                 },
                   datapoints))
    return (sorted(out, key=lambda k: k['Timestamp']))


def get_aggrigated_metric(objs, monitoring, aws_credential, metric,
                          start_time=(datetime.datetime.utcnow() - datetime.timedelta(days=1)),
                          end_time=datetime.datetime.utcnow()):
    from itertools import groupby
    import numpy as np
    dimensions = [{'Name': str(metric.dimensions), 'Value': str(obj)} for obj in objs]
    cw = boto3.client('cloudwatch'
                      , aws_access_key_id=aws_credential.key_id
                      , aws_secret_access_key=aws_credential.key
                      , region_name=aws_credential.region.code)
    datapoints = []
    for dimension in dimensions:
        response = cw.get_metric_statistics(Period=monitoring.period
                                            , StartTime=start_time
                                            , EndTime=end_time
                                            , MetricName=str(metric.value)
                                            , Namespace=metric.namespace
                                            , Statistics=[str(metric.aggragation), ]
                                            , Dimensions=[dimension], )
        datapoints.extend(response["Datapoints"])
    # reformating returned data to format {Timestamp:<Timestamp>,Metric:{Value:<value>,Unit:<unit>,Name:<MetricName>}}
    dp = list(map(lambda data: {'Timestamp': data['Timestamp'],
                                'Metric': {'Value': data[str(metric.aggragation)], 'Unit': data['Unit'],
                                           'Name': str(metric.name)}}, datapoints))
    dps = sorted(dp, key=lambda k: k['Timestamp'])
    gdp = groupby(dps, lambda x: x.pop('Timestamp'))
    out = []
    for timestamp, metricdict in gdp:
        val = [x['Metric']['Value'] for x in metricdict]
        avg = np.mean(val)
        out.append({'Timestamp': timestamp, 'Metric': {'Value': avg, 'Name': str(metric.name)}})

    return (sorted(out, key=lambda k: k['Timestamp']))


def _get_compute_instances(elb_name, key_id, key, region):
    try:
        cw = boto3.client('elb'
                          , aws_access_key_id=key_id
                          , aws_secret_access_key=key
                          , region_name=region)
        instance_healths = cw.describe_instance_health(LoadBalancerName=elb_name)['InstanceStates']
        instances = []
        for instance in instance_healths:
            instances.append({'InstanceId': instance['InstanceId'], 'State': instance['State']})
    except Exception as ex:
        logger.exception(ex)
        raise ex
    else:
        return (instances)
    finally:
        pass


def get_compute_instances(elb_classic, aws_credential):
    from .ec2_instance import EC2
    try:
        aws_access_key_id = aws_credential.key_id
        aws_secret_access_key = aws_credential.key
        region_name = aws_credential.region.code
        load_balancer_names = elb_classic.lb
        instances = _get_compute_instances(load_balancer_names,
                                           aws_access_key_id,
                                           aws_secret_access_key,
                                           region_name)
        ec2 = []
        for instance in instances:
            inst = EC2(instanceId=instance['InstanceId'],
                       status=instance['State'])
            ec2.append(inst)
    except Exception as ex:
        logger.exception(ex)
        raise ex
    else:
        return (ec2)
    finally:
        pass


def get_healthy_compute_instances(aws_credential, elb_classic):
    instances = get_compute_instances(aws_credential, elb_classic)
    healthy_instance = [x['InstanceId'] if x['State'] == 'InService' else None for x in instances]
    return (healthy_instance)


def create_instances(AWSScale, count, awsCredential, ):
    ec2 = boto3.client('ec2'
                       , aws_access_key_id=awsCredential.key_id
                       , aws_secret_access_key=awsCredential.key
                       , region_name=awsCredential.region.code)
    instances = ec2.run_instances(

        DryRun=AWSScale.dryRun,
        ImageId=AWSScale.amiId,
        MinCount=count,
        MaxCount=count,
        KeyName=AWSScale.key_pair,
        SecurityGroupIds=[
            AWSScale.SecurityGroupId,
        ],

        InstanceType=AWSScale.instance_type,
        Placement={
            'Tenancy': 'default',
        },
        SubnetId=AWSScale.SubnetId,
        DisableApiTermination=False,
        InstanceInitiatedShutdownBehavior='terminate',
    )
    instance_ids = [x['InstanceId'] for x in instances['Instances']]
    return (instance_ids)


def terminate_instances(instances, awsCredential):
    ec2 = boto3.resource('ec2'
                         , aws_access_key_id=awsCredential.key_id
                         , aws_secret_access_key=awsCredential.key
                         , region_name=awsCredential.region.code)
    ret = ec2.instances.filter(InstanceIds=[instance.instanceId for instance in instances]).terminate()
    return (ret)


def add_instances_to_elb_classic(instances, elb_classic, awsCredential):
    elb = boto3.client('elb'
                       , aws_access_key_id=awsCredential.key_id
                       , aws_secret_access_key=awsCredential.key
                       , region_name=awsCredential.region.code)
    for instance in instances:
        elb.register_instances_with_load_balancer(LoadBalancerName=elb_classic.lb,
                                                  Instances=[{'InstanceId': instance}, ])


def remove_instances_from_elb_classic(instances, elb_classic, awsCredential):
    elb = boto3.client('elb'
                       , aws_access_key_id=awsCredential.key_id
                       , aws_secret_access_key=awsCredential.key
                       , region_name=awsCredential.region.code)
    inst = elb.deregister_instances_from_load_balancer(LoadBalancerName=elb_classic.lb,
                                                       Instances=[{'InstanceId': instance.instanceId} for instance in
                                                                  instances])
    return (inst['Instances'])


def _get_std_queue_length(key_id, key, region, queue_name):
    try:
        # Get the service resource
        sqs = boto3.resource(service_name='sqs', region_name=region, aws_access_key_id=key_id,
                             aws_secret_access_key=key)
        # Get the queue. This returns an SQS.Queue instance
        queue = sqs.get_queue_by_name(QueueName=queue_name)
        length = int(queue.attributes['ApproximateNumberOfMessages'])

    except Exception as ex:
        logger.exception(ex)
        raise ex
    else:
        return length


def get_queue_length(queue, credential):
    return _get_std_queue_length(credential.key_id, credential.key, credential.region.code, queue.queue_name)

#AWS Autoscale group scaling
def _get_autoscaling_client(key_id=None, key=None, region=None):
    try:
        client = boto3.client('autoscaling', aws_access_key_id=key_id,
                     aws_secret_access_key = key,
                     region_name = region)
    except Exception as ex:
        logger.exception(ex)
        raise ex
    else:
        return client
def _get_autoscaling_groups_description(client=None, group_names=None):

    try:
        out = client.describe_auto_scaling_groups(AutoScalingGroupNames=group_names)

        asgs = out['AutoScalingGroups']

    except Exception as ex:
        logger.exception(ex)
        raise ex
    else:
        return asgs

def _list_autoscaling_groups(client=None):
    try:
        out = client.describe_auto_scaling_groups()

        asgs = out['AutoScalingGroups']

    except Exception as ex:
        logger.exception(ex)
        raise ex
    else:
        return asgs
def _get_instance_count(client=None,group_name=None):
    try:
        asg = _get_autoscaling_groups_description(client=client, group_names=[group_name])[0]
        instance_count = asg['DesiredCapacity']

    except Exception as ex:
        logger.exception(ex)
        raise ex
    else:
        return instance_count

def _set_instance_count(client,group_name,count):
    try:
        response = client.set_desired_capacity(
            AutoScalingGroupName=group_name,
            DesiredCapacity=count,
            HonorCooldown=False
        )

    except Exception as ex:
        logger.exception(ex)
        raise ex
    else:
        return True
def set_autosclaling_instance_count(scaler,count,awsCredential):
    try:
        client = _get_autoscaling_client(key_id=awsCredential.key_id, key=awsCredential.key, region=awsCredential.region.code)
        group_name = scaler.group_name
        _set_instance_count(client,group_name,count)
        if(count !=_get_instance_count(client,group_name)):
            raise UnableToSetInstanceCount
    except Exception as ex:
        logger.exception(ex)
        raise ex
    else:
        return True