from core.models import Scaler

from .aws_credential import AWSCredential
from .ec2_instance import EC2
from .aws_sdk import *
from djongo import models


class AWSScale(Scaler):
    AWS_INSTANCE_CHOICES = (
        ("t2.nano", "T2 NANO"),
        ("t2.micro", "T2 MICRO"),
        ("t2.small", "T2 SMALL"),
        ("t2.medium", "T2 MEDIUM"),
        ("t2.large", "T2 LARGE"),
        ("t2.xlarge", "T2 XLARGE"),
        ("t2.2xlarge", "T2 2XLARGE"),
        ("m4.large", "M4 LARGE"),
        ("m4.xlarge", "M4 XLARGE"),
        ("m4.2xlarge", "M4 2XLARGE"),
        ("m4.4xlarge", "M4 4XLARGE"),
        ("m4.10xlarge", "M4 10XLARGE"),
        ("m4.16xlarge", "M4 16XLARGE"),
        ("m3.medium", "M3 MEDIUM"),
        ("m3.large", "M3 LARGE"),
        ("m3.xlarge", "M3 XLARGE"),
        ("m3.2xlarge", "M3 2XLARGE")
    )
    amiId = models.CharField(max_length=15)
    instance_type = models.CharField(max_length=20, choices=AWS_INSTANCE_CHOICES, default='t2.nano')
    dryRun = models.BooleanField(default=0)
    SubnetId = models.CharField(max_length=20, null=True)
    SecurityGroupId = models.CharField(max_length=20, null=True)
    key_pair = models.CharField(max_length=25, null=True)

    def set_instance_count(self, number,application):
        try:
            if application ==None:
                application = self.application_set.all()[0]
            aws_credential = application.credentials.instance_of(AWSCredential)[0]
            instance_count = len(self.application.loadbalancer.update_instances())
            if (number >= 0):
                if (number > instance_count and number <= self.maxInstances):
                    created_instances = create_instances(self, number - instance_count, aws_credential)
                    add_instances_to_elb_classic(created_instances, self.application.loadbalancer, aws_credential)
                elif (number == instance_count):
                    pass
                elif (number < instance_count and number >= self.minInstances):
                    diff = instance_count - number
                    current = self.application.instances.instance_of(EC2)
                    terminate_instances(current[:diff], AWSCredential)
                    remove_instances_from_elb_classic(current[:diff], self.application.loadbalancer, AWSCredential)
            self.numInstances = len(self.application.loadbalancer.update_instances())
            self.save()
            self.save_base()


        except Exception as ex:
            logger.exception(ex)

        else:
            logger.info("setting instance count to :"+str(self.numInstances))
            return (self.numInstances)
        finally:
            pass

class AWSAutoScale(Scaler):

    group_name = models.CharField(max_length=30)
    def set_instance_count(self, number=None,application=None):
        try:
            if application ==None:
                application = self.application_set.all()[0]
            credential = application.credentials.instance_of(AWSCredential)[0]
            set_autosclaling_instance_count(self,number,credential)
        except Exception as ex:
            logger.exception(ex)
            raise ex
        else:
            return True
    def __str__(self):
        return self.group_name