from core.models import Credential
from .aws_regions import AWSRegions
from djongo import models
from .aws_sdk import get_load_balancers_descriptions
from fernet_fields import EncryptedCharField

class AWSCredential(Credential):
    key_id = EncryptedCharField(max_length=25,verbose_name="Key Id")
    key = EncryptedCharField(max_length=50,verbose_name="Key")
    AWS_REGION_CHOICES = (
        ('us-east-2', 'US East (Ohio)'),
        ('us-east-1', 'US East (N. Virginia)'),
        ('us-west-1', 'US West (N. California)'),
        ('us-west-2', 'US West (Oregon)'),
        ('ap-south-1', 'Asia Pacific (Mumbai)'),
        ('ap-northeast-2', 'Asia Pacific (Seoul)'),
        ('ap-southeast-1', 'Asia Pacific (Singapore)'),
        ('ap-southeast-2', 'Asia Pacific (Sydney)'),
        ('ap-northeast-1', 'Asia Pacific (Tokyo)'),
        ('ca-central-1', 'Canada (Central)'),
        ('eu-central-1', 'EU (Frankfurt)'),
        ('eu-west-1', 'EU (Ireland)'),
        ('eu-west-2', 'EU (London)'),
        ('sa-east-1', 'South America (São Paulo)')
    )
    region = models.ForeignKey(AWSRegions, on_delete=models.CASCADE)

    def list_load_balancers(self):
        lbs = get_load_balancers_descriptions(self)
        return (lbs)