from core.models import Monitoring
from .aws_cloudwatch_metrics import EC2CloudWatchMetric,ELBCloudWatchMetric
from .ec2_instance import EC2
from .aws_credential import AWSCredential
from .aws_sdk import *
from itertools import groupby
import datetime

import logging
logger = logging.getLogger(__name__)


class AWSCloudWatch(Monitoring):

    def to_class_name(self):
        return 'AWS EC2 over Cloud Watch'

    def collect_compute_metric(self, metric,application, start_time, end_time):
        #application = self.application_set.all()[0]
        aws_credential = application.credentials.instance_of(AWSCredential)[0]
        instances = application.instances.instance_of(EC2).get_real_instances()
        return (get_aggrigated_metric(instances, self, aws_credential, metric, start_time, end_time))

    def collect_loadbalancer_metric(self, metric,application, start_time, end_time):
        #application = self.application_set.all()[0]
        aws_credential = application.credentials.instance_of(AWSCredential)[0]
        loadbalancer = application.loadbalancer.get_real_instance()
        return (get_metric(loadbalancer, self, aws_credential, metric, start_time, end_time))
    
    def collect_asg_metric(self, metric,application, start_time, end_time):
        #application = self.application_set.all()[0]
        aws_credential = application.credentials.instance_of(AWSCredential)[0]
        asg = application.scaler.get_real_instance()
        return (get_metric(asg, self, aws_credential, metric, start_time, end_time))

    def collect_all_metrics(self, start_time=(datetime.datetime.utcnow() - datetime.timedelta(days=1)), end_time = datetime.datetime.utcnow()):
        application = self.application_set.all()[0]
        ec2_metrics = application.metrics.instance_of(EC2CloudWatchMetric)
        elb_metrics = application.metrics.instance_of(ELBCloudWatchMetric)
        raw_data = []
        for ec2_metric in ec2_metrics:
            raw_data.extend(self.collect_compute_metric(ec2_metric, start_time, end_time))
        for elb_metric in elb_metrics:
            raw_data.extend(self.collect_loadbalancer_metric(elb_metric, start_time, end_time))
        sorted_data = sorted(raw_data, key=lambda k: k['Timestamp'])
        split_by_time = groupby(sorted_data, lambda x: x.pop('Timestamp'))
        out = []
        for timestamp, metrics in split_by_time:
            metric = [x['Metric'] for x in metrics]
            out.append({'Timestamp': timestamp, "Metric": metric})
        return (out)

    def get_metric(self, metric, start_time, end_time):
        value = 0
        if metric.to_class_name() == 'EC2CloudWatchMetric':
            value = self.collect_compute_metric(metric, start_time, end_time)
        elif metric.to_class_name() == 'ELBCloudWatchMetric':
            value = self.collect_loadbalancer_metric(metric, start_time, end_time)

        out = []
        for v in value:
            out.append(v)
        return out

    def get_latest_metric(self, metric,application=None):
        if application ==None:
            application = self.application_set.all()[0]
            
        time = datetime.datetime.utcnow()
        start_time = time - datetime.timedelta(seconds=self.period * 2)
        end_time = time
        value = 0
        if metric.to_class_name() == 'EC2CloudWatchMetric':
            value = self.collect_compute_metric(metric,application, start_time, end_time)
        elif metric.to_class_name() == 'ELBCloudWatchMetric':
            value = self.collect_loadbalancer_metric(metric,application, start_time, end_time)
        elif metric.to_class_name() == 'ASGCloudWatchMetric':
            value = self.collect_asg_metric(metric,application, start_time, end_time)
        # print(value)
        try:
            out = value[-1]['Metric']['Value']

        except Exception as ex:
            logger.exception(ex)
            out = 0
        try:
            if self.keep_data:
                data = dict(Timestamp=time, Metric={
                    'Name': metric.name,
                    'id'  : metric.pk,
                    'Unit': metric.unit,
                    'Value': out,
                })
                self.save_metric(data,application)
        except Exception as ex:
            logger.exception(ex)
        return (out)

    def to_class_name(self):
        return 'General'

