from core.models import Metric
from djongo import models

class EC2CloudWatchMetric(Metric):
    value = models.CharField(max_length=40)
    namespace = models.CharField(max_length=20,default='AWS/EC2')
    dimensions = models.CharField(max_length=20,default='InstanceId')
    def to_class_name(self):
        return 'EC2CloudWatchMetric'


class ELBCloudWatchMetric(Metric):
    value = models.CharField(max_length=40)
    namespace = models.CharField(max_length=20,default='AWS/ELB')
    dimensions = models.CharField(max_length=20,default='LoadBalancerName')
    def to_class_name(self):
        return 'ELBCloudWatchMetric'


class ASGCloudWatchMetric(Metric):
    value = models.CharField(max_length=40)
    namespace = models.CharField(max_length=20,default='AWS/EC2')
    dimensions = models.CharField(max_length=20,default='AutoScalingGroupName')
    def to_class_name(self):
        return 'ASGCloudWatchMetric'