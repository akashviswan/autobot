from core.models import LoadBalancer
from .ec2_instance import EC2


class ELB_Classic(LoadBalancer):

    def to_class_name(self):
        return 'AWS ElasticLoadBalancer Classic'

    def update_instances(self,application):
        from .aws_sdk import get_compute_instances
        from .aws_credential import AWSCredential
        aws_credential = application.credentials.instance_of(AWSCredential)[0]
        instances = []
        for instance in get_compute_instances(self,aws_credential):
            ec2 = EC2.objects.get_or_create(instanceId=instance.instanceId)[0]
            ec2.status = instance.status
            ec2.save()
            instances.append(ec2.instanceId)
            ec2.save_base()
            application.instances.add(ec2)
            application.save()
            application.save_base()
        EC2.objects.filter(application=application).exclude(instanceId__in=instances).delete()
        return (application.instances.all())

    def list_instances(self):
        from .aws_sdk import get_compute_instances
        from .aws_credential import AWSCredential
        aws_credential = self.application.credentials.instance_of(AWSCredential)[0]
        return get_compute_instances(aws_credential, self)

