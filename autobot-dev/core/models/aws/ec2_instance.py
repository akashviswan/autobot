from core.models import Instance
from djongo import models

class EC2(Instance):

    instanceId = models.CharField(max_length=20)

    def __str__(self):
        return self.instanceId

    def to_class_name(self):
            return 'EC2'