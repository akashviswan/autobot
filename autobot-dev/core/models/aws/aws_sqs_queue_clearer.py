########################################################
#AutoScale based on AWS SQS queue length
#For clearing queues faster
########################################################

from core.models import AutoScaler
from djongo import models
from .aws_credential import AWSCredential
from .aws_sdk import *
import logging
logger = logging.getLogger(__name__)

class SQSQueueClearer(AutoScaler):
    queue_name = models.CharField(max_length=30)
    jobs_per_node = models.DecimalField(max_digits=5, decimal_places=0)

    def calculate_instance_count(self):
        try:
            application = self.application_set.all()[0]
            aws_credential = application.credentials.instance_of(AWSCredential)[0]
            queue_lenght = int(get_queue_length(self,aws_credential))

            instance_required = (queue_lenght // self.jobs_per_node) + 1
        except Exception as ex:
            logger.exception(ex)
            raise ex
        else:
            return (instance_required)