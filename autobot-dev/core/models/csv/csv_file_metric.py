from core.models import Metric

class CSVMetric(Metric):

    def to_class_name(self):
        return 'CSVMetric'