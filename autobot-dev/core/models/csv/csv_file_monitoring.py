from core.models import Monitoring
import pandas as pd
import os

class CSVFile(Monitoring):
    def to_class_name(self):
        return 'Read Metrics from File'

    def _read_df(self):
        df = pd.read_csv(os.path.join('csv/', self.application.name), index_col=0)
        return df

    def collect_all_metrics(self, start_time, end_time):
        #

        #        # Build paths inside the project like this: os.path.join(BASE_DIR, ...)
        #        BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))

        #        #self.application = get_object_or_404(Application, pk=pk)
        #        with open(os.path.join(BASE_DIR, '../tsv/',self.application.name), 'rb') as myfile:
        #            response = HttpResponse(myfile, content_type='text/csv')
        #            response['Content-Disposition'] = 'attachment; filename=application.tsv'
        #            return response
        df = self._read_df()
        data = df.to_dict(orient='index')
        out = list(map(lambda item: {'Timestamp': item[0],
                                     'Metric': list({'Name': key,
                                                      'Value': value} for key, value in item[1].items())},
                       data.items()))
        return out

    def get_metric(self, metric, start_time, end_time):
        df = self._read_df()
        df = pd.DataFrame(df[metric.name].copy())
        data = df.to_dict(orient='index')
        out = list(map(lambda item: {'Timestamp': item[0],
                                     'Metric': {'Name': str(metric),
                                                'Value': item[1][str(metric)]}},
                       data.items()))
        return out
