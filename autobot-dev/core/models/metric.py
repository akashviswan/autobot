from djongo import models
from polymorphic.models import PolymorphicModel
from django.utils import timezone

class Metric(PolymorphicModel):
    name = models.CharField(max_length=20)
    unit = models.CharField(max_length=20,default='Count')
    aggragation = models.CharField(max_length=20,default='Average')
    date_created = models.DateTimeField(editable=False)
    date_modified = models.DateTimeField()
    def save(self, *args, **kwargs):
#On save, update timestamps
        if not self.id:
            self.date_created = timezone.now()
            self.date_modified = timezone.now()
        return super(Metric, self).save(*args, **kwargs)
    def __str__(self):              
        return self.name
    def to_class_name(self):
        return 'General'

    @classmethod
    def get_ui_label_keys(cls):
        return "name"

