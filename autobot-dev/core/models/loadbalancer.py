from polymorphic.models import PolymorphicModel, PolymorphicManager
from djongo import models
from django.utils import timezone

class LoadBalancer(PolymorphicModel):
    lb = models.CharField(max_length=15)
    
    def save(self, *args, **kwargs):
        if not self.id:
            self.date_created = timezone.now()
            self.date_modified = timezone.now()
        return super(LoadBalancer, self).save(*args, **kwargs)

    def __str__(self):  # __unicode__ on Python 2
        return self.lb

    def to_class_name(self):
        return 'Generic LoadBalancer'
    @classmethod
    def get_ui_label_keys(cls):
        return "lb"
    @classmethod
    def get_ui_value_keys(cls):
        return None
