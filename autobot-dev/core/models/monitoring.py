########################################################
# monitoring module contains the Monitoring super class and all the subclasses,
# each monitoring engine should extend the main Monitoring class and overload the methods for collecting the respective metric from Monitoring system
########################################################
from django.utils import timezone
from polymorphic.models import PolymorphicModel
#from core.models import Application
from djongo import models
from core.models import *

class Monitoring(PolymorphicModel):
    #application = models.ForeignKey(Application, on_delete=models.CASCADE)
    period = models.IntegerField()
    keep_data = models.BooleanField(default=False)
    date_created = models.DateTimeField(editable=False)
    date_modified = models.DateTimeField()

    def save(self, *args, **kwargs):
        ''' On save, update timestamps '''
        if not self.id:
            self.date_created = timezone.now()
        self.date_modified = timezone.now()
        return super(Monitoring, self).save(*args, **kwargs)


    def to_class_name(self):
        return 'Generic Monitoring'

    def save_metric(self,data,application):
        #application = self.application_set.all()[0]
        return application.metric_store.insert(data)

    @classmethod
    def get_ui_label_keys(cls):
        return "resourcetype"


