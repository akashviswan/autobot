# -*- coding: utf-8 -*-

from djongo import models
from polymorphic.models import PolymorphicModel
from django.utils import timezone
from core.models import Metric,Monitoring,Action 
class Threshold(PolymorphicModel):
    metric = models.ForignKey(Metric)
    monitoring_engine = models.ForignKey(Monitoring)
    actions = models.ManytoManyField(Action)
    on_failure_continue = models.BooleanField(default = False)
    def _get_metic_value():
        pass
    def evaluate():
        pass
    def execute():
        pass
    
    
class SimpleThreshold(Threshold):
    condition_choices = (('lt','Less than'),('gt','Great than'))
    condition = models.CharField(max_length=2,choices=condition_choices,default='lt')
    value = models.DecimalField(max_digits=5, decimal_places=2)
    reoccurence = models.IntegerField(default=1)
    
    
    