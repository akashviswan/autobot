from djongo import models
from core.models import Metric

class AzureScaleSetMetricRule(Metric):
    metricName = models.CharField(max_length=20, default='Percentage CPU')
    metricResourceUri = models.CharField(max_length=100)
    operator = models.CharField(max_length=20, default='GreaterThan')
    threshold = models.IntegerField()
    timeGrain = models.CharField(max_length=20, default='PT1M')
    timeWindow = models.CharField(max_length=20, default='PT10M')
    statistic =  models.CharField(max_length=20, default='Average')
    timeAggregation = models.CharField(max_length=20, default='Average')

    direction = models.CharField(max_length=20, default='Increase')
    type = models.CharField(max_length=20, default='ChangeCount')
    value = models.IntegerField()
    cool_down = models.CharField(max_length=20, default='PT5M')

    def to_class_name(self):
        return 'Azure Scale Set Metric'