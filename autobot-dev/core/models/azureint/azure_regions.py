from djongo import models
from django.utils import timezone


class AzureRegions(models.Model):
    name = models.CharField(max_length=25)
    code = models.CharField(max_length=25)

    def save(self, *args, **kwargs):
        if not self.name:
            self.date_created = timezone.now()
            self.date_modified = timezone.now()
        return super(AzureRegions, self).save(*args, **kwargs)

    def __str__(self):              # __unicode__ on Python 2
        return self.name
   
    @classmethod
    def get_ui_label_keys(cls):
        return "name"
    @classmethod
    def get_ui_value_keys(cls):
        return None