########################################################
#AutoScaler module contains the AutoScaler super class and all the subclasses,
#each AutoScaler engine should extend the main class and overload the methods for auto scaling
########################################################
from django.utils import timezone
from core.models import Application, AutoScaler
from djongo import models
from .azure_metric import AzureScaleSetMetricRule
from .azure_credential import AzureCredential
from .azure_scaleset import AzureScaleSet
#from core.exceptions import *
from .azuresdk import set_autoscale_state ,get_scaleset_instances
import logging
logger = logging.getLogger(__name__)

class AzureScaleSetScale(AutoScaler):
    name = models.CharField(max_length=20)
    scaler = models.ForeignKey(AzureScaleSet,on_delete=models.CASCADE)
    rules = models.ManyToManyField(AzureScaleSetMetricRule)

    def enable(self):
        try:
            application = self.application_set.all()[0]
            credentials = application.credentials.instance_of(AzureCredential)[0]
            scaleset = self
            self.enabled = True
            metrics = self.rules.all()
            ret=set_autoscale_state(credentials,scaleset,metrics)
        except Exception as ex:
            logger.exception(ex)
            raise ex
        else:
            self.enabled = True
            self.save()
            self.save_base()
            return ret

    def disable(self):
        try:
            application = self.application_set.all()[0]
            credentials = application.credentials.instance_of(AzureCredential)[0]
            scaleset = self
            self.enabled = False
            metrics = self.rules.all()
            ret=set_autoscale_state(credentials,scaleset,metrics)
        except Exception as ex:
            logger.exception(ex)
            raise ex
        else:
            self.enabled = False
            self.save()
            self.save_base()
            return ret

    def calculate_instance_count(self):
        try:
            application = self.application_set.all()[0]
            credentials = application.credentials.instance_of(AzureCredential)[0]
            scaleset = self.scaler
            count=len(get_scaleset_instances(credentials, scaleset))
        except Exception as ex:
            logger.exception(ex)
            raise ex
        else:
            return count

    def save(self, *args, **kwargs):
    #On save, update timestamps
        if not self.id:
            self.date_created = timezone.now()
            self.date_modified = timezone.now()
        return super(AzureScaleSetScale, self).save(*args, **kwargs)
    def __str__(self):
        return str(self.application)
    def to_class_name(self):
        return 'AzureScaleSetScale'