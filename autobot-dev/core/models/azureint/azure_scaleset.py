from core.models import Scaler
from djongo import models
from .azuresdk import set_scaleset_instance_count
from .azure_credential import AzureCredential

import logging
logger = logging.getLogger(__name__)


class AzureScaleSet(Scaler):
    group_name = models.CharField(max_length=25)
    vm_scale_set_name = models.CharField(max_length=25)
    instance_type = models.CharField(max_length=25)
    resourceuri = models.CharField(max_length=25)
    rulename = models.CharField(max_length=25)

    # __unicode__ on Python 2
    def __str__(self):
        return self.group_name

    def set_instance_count(self, number):
        try:
            application = self.application_set.all()[0]
            azure_credential = application.credentials.instance_of(AzureCredential)[0]

            if number >= 0:
                if self.minInstances <= number <= self.maxInstances:
                    created_instances = set_scaleset_instance_count(self, azure_credential, number)
                else:
                    pass
                self.numInstances = len(created_instances)
            self.save()
            self.save_base()
        except Exception as ex:
            logger.exception(ex)
            raise ex
        else:
            logger.debug("setting instance count to :" + str(self.numInstances))
            return self.numInstances
        finally:
            pass
