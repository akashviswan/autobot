from azure.common.credentials import ServicePrincipalCredentials
from azure.mgmt.compute import ComputeManagementClient
from azure.mgmt.monitor import MonitorManagementClient
from .azure_instance import AzureVM

from core.exceptions import *
import logging
import datetime

logger = logging.getLogger(__name__)
#################################
#  This the base function that gets the credentials for Accessing the APIs in Azure.
# This function returns a credential objects. All function uses some versions of get_Credentials in to access the apis
#################################
def _get_credentials(client_id, secret, tenant):
    try:
        credentials = ServicePrincipalCredentials(
            client_id=client_id,
            secret=secret,
            tenant=tenant
        )
    except Exception as ex:
        logger.exception(ex)
        raise ex
    else:
        return credentials
    finally:
        pass
#################################
#  Function to set the VMs in a saleSet
# This is the base function. Returns a list of vms in the scale set after sacling in or out..
#################################
def _set_scaleset_instance_count(subscription_id, client_id, secret, tenant, location, group_name, vm_scale_set_name, instance_type, count):
    vm_names = []

    def _create_vm_parameters():
        return {
            'location': location,
            'sku': {
                'name': instance_type,
                'capacity': count,
            }
        }
    credentials = _get_credentials(client_id, secret, tenant)

    try:
        compute_client = ComputeManagementClient(credentials, subscription_id)
        vm_parameters = _create_vm_parameters()
        scalesetupdate = compute_client.virtual_machine_scale_sets.create_or_update(resource_group_name=group_name,
                                                                                    vm_scale_set_name=vm_scale_set_name,
                                                                                    parameters=vm_parameters)
        scalesetupdate.wait()
        for vm in compute_client.virtual_machine_scale_set_vms.list(resource_group_name=group_name,
                                                                    virtual_machine_scale_set_name=vm_scale_set_name):
            vm_names.append({'name': vm.name,'instanceId': vm.vm_id })
    except Exception as ex:
        print(ex)
    else:
        return vm_names
    finally:
        pass


#################################
#  Function to get the  VMs in a saleSet
#  This is the base function. Returns a list.
#################################
def _get_scaleset_instances(subscription_id, client_id, secret, tenant, group_name, vm_scale_set_name):
    vm_names = []
    credentials = _get_credentials(client_id, secret, tenant)
    try:
        compute_client = ComputeManagementClient(credentials, subscription_id)
        for vm in compute_client.virtual_machine_scale_set_vms.list(resource_group_name=group_name,
                                                                    virtual_machine_scale_set_name=vm_scale_set_name,raw=True):
            vm_names.append({'name': vm.name,'instanceId': vm.vm_id })
    except Exception as ex:
        logger.exception(ex)
        raise ex
    else:
        return vm_names
    finally:
        pass


def get_scaleset_instances(credentials, scaleset):
    azure_vms=[]
    try:
        subscription_id = credentials.subscription_id
        client_id = credentials.client_id
        secret = credentials.secret
        tenant = credentials.tenant
        location = credentials.region
        group_name = scaleset.group_name
        vm_scale_set_name = scaleset.vm_scale_set_name

        vms = _get_scaleset_instances(subscription_id, client_id, secret, tenant, group_name, vm_scale_set_name)
        for vm in vms:
            azure_vms.append(AzureVM.objects.get_or_create(name=vm['name'],instanceId=vm['instanceId']))
    except Exception as ex:
        logger.exception(ex)
        raise ex
    else:
        return azure_vms
    finally:
        pass
#################################
#  Function to set the counts of the number of VMs in a saleSet
# This funciton takes 3 arguments. The credentials object, the scaleset object and
#  the number of VMs (count) to which the scaleSet to scale out or in to.
# This function calls the base function _set_scaleset_instance_count.
#################################


def set_scaleset_instance_count(scaleset, credentials, count):
    azure_vms = []
    try:
        subscription_id = credentials.subscription_id
        client_id = credentials.client_id
        secret = credentials.secret
        tenant = credentials.tenant
        location = credentials.region
        group_name = scaleset.group_name
        vm_scale_set_name = scaleset.vm_scale_set_name
        instance_type = scaleset.instance_type
        count = count
        vms=_set_scaleset_instance_count(subscription_id, client_id, secret, tenant, location, group_name,
                                     vm_scale_set_name, instance_type, count)
        for vm in vms:
            azure_vms.append(AzureVM.objects.get_or_create(name=vm['name'], instanceId=vm['instanceId']))
    except Exception as ex:
        logger.exception(ex)
        raise ex
    else:
        return azure_vms


#################################
#  Base function to Enable Or Disable the Autoscaler funciton.
#  AutoScaler State (Enabled and Disabled) rule can be set using the
#  monitor_client.autoscale_settings.update function.
# This is the base class
#################################
def _set_autoscale_state(subscription_id, client_id, secret, tenant, location, vm_scale_set_name,group_name,
                         enabled,rules,resourceuri,minimum,maximum,default,rulename):

    credentials = _get_credentials(client_id, secret, tenant)
    try:
        monitor_client = MonitorManagementClient(credentials, subscription_id)

        def create_resource():
            return {
                "location": location,
                "properties": {
                    "name": rulename,
                    "enabled": enabled,
                    "targetResourceUri": resourceuri,
                    "profiles": [
                        {
                            "name": vm_scale_set_name,
                            "capacity": {
                                "minimum": minimum,
                                "maximum": maximum,
                                "default": default
                            },
                            "rules": rules

                        }
                    ],
                },
            }
        auto_scale_resource = create_resource()
        logger.debug(str(auto_scale_resource))
        autoscale_state = monitor_client.autoscale_settings.update(resource_group_name=group_name,
                                                                   autoscale_setting_name=rulename,
                                                                   autoscale_setting_resource=auto_scale_resource,
                                                                   subscription_id=subscription_id)

    except Exception as ex:
        logger.exception(ex)
        raise ex
    else:
        return autoscale_state
    finally:
        pass


def set_autoscale_state(credentials,autoscaleset,metrics):

    try:
        #credential
        subscription_id = credentials.subscription_id
        client_id = credentials.client_id
        secret = credentials.secret
        tenant = credentials.tenant
        location = credentials.region

        #auto scaler

        enable = autoscaleset.enabled
        #scaler
        resourceuri = autoscaleset.scaler.resourceuri
        rulename = autoscaleset.scaler.rulename

        group_name = autoscaleset.scaler.group_name
        vm_scale_set_name = autoscaleset.scaler.vm_scale_set_name
        instance_type = autoscaleset.scaler.instance_type
        minimum = autoscaleset.scaler.minInstances
        maximum = autoscaleset.scaler.maxInstances
        default = autoscaleset.scaler.numInstances

        rules=[]
        for metric in metrics:
            rules.append({
                   "scaleAction": {
                            "direction": metric.direction,
                            "type": metric.type,
                            "value": metric.value,
                            "cooldown": metric.cool_down
                            },
                   "metricTrigger": {
                            "metricName": metric.metricName,
                            "metricResourceUri": metric.metricResourceUri,
                            "operator": metric.operator,
                            "statistic": metric.statistic,
                            "threshold": metric.threshold,
                            "timeAggregation": metric.timeAggregation,
                            "timeGrain": metric.timeGrain,
                            "timeWindow": metric.timeWindow
                            }
                    })
        ret= _set_autoscale_state(subscription_id, client_id, secret, tenant, location, vm_scale_set_name,group_name,
                         enable,rules,resourceuri,minimum,maximum,default,rulename)
    except Exception as ex:
        logger.exception(ex)
        raise ex
    else:
        return ret
    finally:
        pass

def _metricdefinitionlist(subscription_id, client_id, secret, tenant, resource_id):
    try:
        credentials = _get_credentials(client_id, secret, tenant)
        monitor_mgmt_client = MonitorManagementClient(credentials, subscription_id)
        resource_id = resource_id
        for metric in monitor_mgmt_client.metric_definitions.list(resource_id):
            print("{}: id={}, unit={}".format(
                metric.name.localized_value,
                metric.name.value,
                metric.unit
            ))
    except Exception as ex:
        logger.exception(ex)
        raise ex
    else:
        return ""
    finally:
        pass

# Valid metrics: Percentage CPU,
# Network In,
# Network Out,
# Disk Read Bytes,
# Disk Write Bytes,
# Disk Read Operations/Sec,
# Disk Write Operations/Sec,
# CPU Credits Remaining,
# CPU Credits Consumed,
# Per Disk Read Bytes/sec,
# Per Disk Write Bytes/sec,
# Per Disk Read Operations/Sec,
# Per Disk Write Operations/Sec,
# Per Disk QD,
# OS Per Disk Read Bytes/sec,
# OS Per Disk Write Bytes/sec,
# OS Per Disk Read Operations/Sec,
# OS Per Disk Write Operations/Sec,
# OS Per Disk QD,
# Inbound Flows,
# Outbound Flows


def _metricdata(subscription_id, client_id, secret, tenant, resource_id, starttime, endtime, metric, interval='PT1H', aggregation='Total'):
    credentials = _get_credentials(client_id, secret, tenant)
    try:
        client = MonitorManagementClient(credentials, subscription_id)
        #today = datetime.datetime.now().date()
        #yesterday = today - datetime.timedelta(days=1)

        starttime = starttime
        endtime = endtime

        metrics_data = client.metrics.list(
            resource_id,
            timespan="{}/{}".format(starttime, endtime),
            interval=interval,
            metric=metric,
            aggregation=aggregation
        )
        for item in metrics_data.value:
            print("{} ({})".format(item.name.localized_value, item.unit.name))
            for timeserie in item.timeseries:
                for data in timeserie.data:
                    print("{}: {}".format(data.time_stamp, data.total))
    except Exception as ex:
        logger.exception(ex)
        raise ex
    finally:
        pass
