from djongo import models
from django.utils import timezone
from core.models import Credential
from .azure_regions import AzureRegions
from fernet_fields import EncryptedCharField


class AzureCredential(Credential):
    subscription_id = EncryptedCharField(max_length=40)
    client_id = EncryptedCharField(max_length=40)
    secret = EncryptedCharField(max_length=50)
    tenant = EncryptedCharField(max_length=40)
    region = models.ForeignKey(AzureRegions, on_delete=models.CASCADE)

    def save(self, *args, **kwargs):
        if not self.id:
            self.date_created = timezone.now()
            self.date_modified = timezone.now()
        return super(AzureCredential, self).save(*args, **kwargs)

    # __unicode__ on Python 2
    def __str__(self):
        return self.name