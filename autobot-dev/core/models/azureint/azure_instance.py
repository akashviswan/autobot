from djongo import models
from core.models import Instance


class AzureVM(Instance):
    instanceId = models.CharField(max_length=40)

    def __str__(self):
        return self.instanceId

    def to_class_name(self):
        return 'Azure Instance'