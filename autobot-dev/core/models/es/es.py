from django.utils import timezone
from django.utils.encoding import python_2_unicode_compatible
from djongo import models
import datetime
import logging

logger = logging.getLogger(__name__)
from elasticsearch import Elasticsearch
from elasticsearch import helpers
from core.models import MetricStore ,Credential


class ES(MetricStore):
    host = models.CharField(max_length=15)
    port = models.BigIntegerField()
    index = models.CharField(max_length=15)
    credential = models.ForeignKey(Credential, on_delete=models.CASCADE, null='true')


    def save(self, *args, **kwargs):

        if not self.id:
            self.date_created = timezone.now()
            self.date_modified = timezone.now()
            return super(ES, self).save(*args, **kwargs)

    def __str__(self):
        return self.host

    def get(self, id):
        try:
            es = Elasticsearch([{'host': self.host, 'port': self.port}])
            data = es.get(self.index, id)
        except Exception as ex:
            logger.exception(ex)
            return -1
        else:
            return data
        finally:
            # es.close()
            pass

    def insert(self, data):
        try:
            dc_type = str(self.application_set.all()[0])
            es = Elasticsearch([{'host': self.host, 'port': self.port}])
            es.index(self.index, dc_type, data)
        except Exception as ex:
            logger.exception(ex)
            raise ex
        else:
            return 0
        finally:
            # es.close()
            pass

    def bulk_insert(self, data):
        try:
            dc_type = str(self.application_set.all()[0])
            es = Elasticsearch([{'host': self.host, 'port': self.port}])
            status = helpers.bulk(es, data)
        except Exception as ex:
            logger.exception(ex)
            raise ex
        else:
            return status
        finally:
            # es.close()
            pass



    def search(self, body):
        try:
            es = Elasticsearch([{'host': self.host, 'port': self.port}])
            data = es.search(self.index, body=body)
        except Exception as ex:
            logger.error(ex)
            raise ex
        else:
            return data
        finally:
            # es.close()
            pass