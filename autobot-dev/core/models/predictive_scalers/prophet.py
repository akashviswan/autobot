########################################################
# Scaler module contains the Scaler super class and all the subclasses,
# each Scaling engine should extend the main class and overload the methods for auto scaling
########################################################
from django.utils import timezone
from polymorphic.models import PolymorphicModel

from djongo import models
from core.models import Metric, Monitoring, AutoScaler
from picklefield.fields import PickledObjectField
import pickle
# from fbprophet import Prophet

import numpy as np
import pandas as pd
import datetime

import logging
logger = logging.getLogger(__name__)


class ProphetScaler(AutoScaler):
    metric = models.ForeignKey(Metric, on_delete=models.PROTECT)
    monitoring_engine = models.ForeignKey(Monitoring, on_delete=models.PROTECT)

    training_data_period = models.IntegerField(default=360)
    training_data_percent = models.IntegerField(default=90)

    predictive_model = PickledObjectField(null=True,blank=True)
    fwd_time = models.IntegerField(default=60)
    value = models.IntegerField(default=22)

    retrain_period = models.IntegerField(default=30)
    last_train = models.DateTimeField(null=True,blank=True)

    def save(self, *args, **kwargs):
        # On save, update timestamps
        if not self.id:
            self.date_created = timezone.now()
            self.date_modified = timezone.now()
        return super(ProphetScaler, self).save(*args, **kwargs)

    def _load_data(self,et=None,period=None):
        # time range for learning
        if et:
            end_time=et.replace(hour=0, minute=0, second=0, microsecond=0)
            start_time = et - datetime.timedelta(days=period)
        else:
            end_time = datetime.datetime.now().replace(hour=0, minute=0, second=0, microsecond=0)
            start_time = end_time - datetime.timedelta(days=self.training_data_period)

        # load data
        metric_value = self.monitoring_engine.get_metric(self.metric, start_time, end_time)

        # flatten JSON and select only

        df = pd.DataFrame(pd.io.json.json_normalize(metric_value))
        data = df[['Timestamp', 'Metric.Value']]

        # rename Timestamp to ds and Metric Value to y for Prophet to work
        data.rename(columns={'Timestamp': 'ds', 'Metric.Value': 'y'}, inplace=True)
        data['ds'] = data['ds'].values.astype('datetime64[s]')

        # Split data from training and testing
        train, test, validate = np.split(data,
                                         [int(self.training_data_percent * len(data)),
                                          int(1 * len(data))])
        return train, test, validate

    def train(self, train):

        # instantiate Prophet
        model = Prophet()

        # fit the model with your dataframe
        model.fit(train);

        self.predictive_model = pickle.dumps(model)
        last_train = datetime.datetime.now()
        self.save()
        self.save_base()
        return self.predictive_model

    def test(self,test):
        #load pickle object
        model = pickle.loads(self.predictive_model)
        # create future data for testing
        future_data = model.make_future_dataframe(periods=len(test), freq='H')
        # forcasted data
        forecast_data = model.predict(future_data)

        #compine predicted and fotcasted data
        final = pd.merge(forecast_data, test, on=['ds'], how='inner')

        #Calculate RMSE(Root Mean Squered Error)

        rmse = ((final.yhat - final.y) ** 2).mean() ** .5

        #Calculate SMAPE

        smape = (abs(final.yhat - final.y)/(abs(final.yhat)+abs(final.y))).mean()*100

        return final, rmse , smape

    def predict(self,time):

        #remove minute , second and microsecond
        time = time.replace(minute=0, second=0, microsecond=0)

        # load pickle object
        model = pickle.loads(self.predictive_model)
        # create future dataframe
        future_data =pd.DataFrame(
            {
                        'ds':[str(time)]
            }
        )
        # forcasted data
        forecast_data = model.predict(future_data)

        return forecast_data.yhat[0]

    def calculate_instance_count(self,application=None):
        #Forward Time
        time_now = datetime.datetime.now()
        time_lookup = time_now + datetime.timedelta(minutes=self.fwd_time)

        #load as per predictive model
        predicted_load = self.predict(time_lookup)

        #instances required to serve the load
        instance_required = int(abs(predicted_load // int(self.value)) + 1)

        return instance_required
    def __str__(self):
        return str('Prophet Scaler_'+str(self.pk))