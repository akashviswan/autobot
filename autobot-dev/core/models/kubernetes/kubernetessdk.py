from kubernetes import client, config
import logging


logger = logging.getLogger(__name__)


def _setreplicationcontrollerount(configuration, replicationcontrollername, replicas, namespace="default"):

    configuration = configuration
    loader = config.kube_config.KubeConfigLoader(configuration)

    con = type.__call__(client.configuration.Configuration)
    loader.load_and_set(con)
    client.configuration.Configuration.set_default(con)

    api_instance = client.CoreV1Api()
    replicationcontrollername = replicationcontrollername
    namespace = namespace
    body = {'spec': {'replicas': replicas}}
    response = api_instance.patch_namespaced_replication_controller_scale(replicationcontrollername, namespace, body)

    return response


def setreplicationcontrollerount(clientconfig, replicationscale):
    try:
        configuration = clientconfig.configuration
        replicationcontrollername = replicationscale.replcationcontrollername
        namespace = replicationscale.namespace
        replicacount = replicationscale.numInstances
        response = _setreplicationcontrollerount(configuration, replicationcontrollername, replicacount, namespace)
    except Exception as ex:
        logger.exception(ex)
        raise ex
    else:
        return response
    finally:
        pass


def _setdeploymentreplicacount(configuration, deploymentname, replicas, namespace="default", sslverify=True):

    body = {'spec': {'replicas': replicas}}
    loader = config.kube_config.KubeConfigLoader(configuration)

    con = type.__call__(client.configuration.Configuration)

    if sslverify is False:
        con.verify_ssl = False

    loader.load_and_set(con)
    client.configuration.Configuration.set_default(con)

    appsv1beta1api = client.AppsV1beta1Api()
    response = appsv1beta1api.patch_namespaced_deployment(name=deploymentname, namespace=namespace, body=body)
    return response


def setdeploymentreplicacount(clientconfig, kubernetesdeployment, count):
    try:
        configuration = clientconfig.configuration
        sslverify = clientconfig.sslverify
        deploymentname = kubernetesdeployment.deploymentname
        namespace = kubernetesdeployment.namespace

        response = _setdeploymentreplicacount(configuration=configuration, deploymentname=deploymentname,
                                              replicas=count, namespace=namespace, sslverify=sslverify)
    except Exception as ex:
        logger.exception(ex)
        raise ex
    else:
        return response
    finally:
        pass
