"""
usage
python3 scrapemetrics.py ipaddress namespace
"""

import sys
import json
import requests

ipaddress = sys.argv[1]
namespace = sys.argv[2]

url = "https://{}:6443/apis/metrics.k8s.io/v1beta1/namespaces/{}/pods/".format(ipaddress, namespace)

metrics_json = requests.get(url, verify=False).content
metrics_dict = json.loads(metrics_json)

podcount = len(metrics_dict['items'])
print(podcount)

for i in range(len(metrics_dict['items'])):
    totalcpu = float(totalcpu) + float(metrics_dict['items'][i]['containers'][0]['usage']['cpu'])
    totalmemory = float(totalmemory) + float(metrics_dict['items'][i]['containers'][0]['usage']['memory'])

averagecpu = totalcpu / float(podcount)
averagememory = totalmemory / float(podcount)

