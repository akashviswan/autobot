from djongo import models
from core.models import Credential
from core.models import Scaler
from django.utils import timezone
from core.management.custom_fields.encrypted_dict_fields import EncryptedDictField

class KubeClientConfig(Credential):
    sslverify = models.BooleanField()
    configuration = EncryptedDictField()

    def save(self, *args, **kwargs):
        if not self.name:
            self.date_created = timezone.now()
            self.date_modified = timezone.now()
        return super(KubeClientConfig, self).save(*args, **kwargs)

    def __str__(self):              # __unicode__ on Python 2
        return self.name


class ReplicationControllerScaler(Scaler):
    replicationcontrollername = models.CharField(max_length=25)
    namespace = models.CharField(max_length=25)

    def save(self, *args, **kwargs):
        if not self.name:
            self.date_created = timezone.now()
            self.date_modified = timezone.now()
        return super(ReplicationControllerScaler, self).save(*args, **kwargs)

    def __str__(self):              # __unicode__ on Python 2
        return self.name


class KubernetesDeployment(Scaler):
    deploymentname = models.CharField(max_length=25)
    namespace = models.CharField(max_length=25)

    def save(self, *args, **kwargs):
        if not self.deploymentname:
            self.date_created = timezone.now()
            self.date_modified = timezone.now()
        return super(KubernetesDeployment, self).save(*args, **kwargs)

    def __str__(self):              # __unicode__ on Python 2
        return self.deploymentname
