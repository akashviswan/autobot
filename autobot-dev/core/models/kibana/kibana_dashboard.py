from djongo import models
from polymorphic.models import PolymorphicModel
from django.utils import timezone
from core.models import Dashboard
from datetime import datetime, timedelta

class KibanaDash(Dashboard):
    host = models.CharField(max_length=30)
    port = models.IntegerField(default=5601)
    use_ssl = models.BooleanField(default=True)
    dashboard_id = models.CharField(max_length=100)
    dashboard_url = models.TextField()
    
    @property
    def render(self):
        historyDate = datetime.today() - timedelta(days=self.history)
        futureDate = datetime.today()
        if self.show_future:
            futureDate = futureDate + timedelta(days=self.future)
        # %Y-%m-%dT%H:%M:%S.%fZ
        temp_url = self.dashboard_url % (self.dashboard_id, historyDate.strftime("%Y-%m-%dT%H:%M:%S.%fZ"), futureDate.strftime("%Y-%m-%dT%H:%M:%S.%fZ"))
        url =  ('https://' if self.use_ssl else 'http://') + self.host + ':' + str(self.port) + temp_url
        render = "<iframe class=\"app-dashboard\" src=" + url + " height=\"700px\" width=\"100%\" ></iframe>"
        return render