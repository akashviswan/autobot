from djongo import models
from polymorphic.models import PolymorphicModel
from django.utils import timezone

class MetricStore(PolymorphicModel):

    append_exsisting = models.BooleanField(default=False)
    date_created = models.DateTimeField(editable=False)
    date_modified = models.DateTimeField()

    def save(self, *args, **kwargs):

        if not self.id:
            self.date_created = timezone.now()
            self.date_modified = timezone.now()
            return super(MetricStore, self).save(*args, **kwargs)

    def insert(self,data):
        #data format
        #{Timestamp:<Timestamp>,Metric:{Name:<MetricName>,Unit:<unit>,Value:<value>}}
        pass

    def bulk_insert(self, data):
        pass

    def get(self,id):
        pass

    def search(self, body):
        pass