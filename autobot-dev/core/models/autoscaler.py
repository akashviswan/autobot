########################################################
# AutoScaler module contains the AutoScaler super class and all the subclasses,
# each AutoScaler engine should extend the main class and overload the methods for auto scaling
########################################################
from django.utils import timezone
from polymorphic.models import PolymorphicModel
from djongo import models
from core.models import Metric, Monitoring
import datetime


class AutoScaler(PolymorphicModel):
    #application = models.ForeignKey(Application, on_delete=models.CASCADE)
    priority = models.IntegerField(default=10)
    date_created = models.DateTimeField(editable=False)
    date_modified = models.DateTimeField()

    def save(self, *args, **kwargs):
        # On save, update timestamps
        if not self.id:
            self.date_created = timezone.now()
            self.date_modified = timezone.now()
        return super(AutoScaler, self).save(*args, **kwargs)

    def __str__(self):
        return str('Auto Scaler_'+str(self.pk))

    def to_class_name(self):
        return 'General'


class SimpleScaler(AutoScaler):
    metric = models.ForeignKey(Metric, on_delete=models.PROTECT)
    monitoring_engine = models.ForeignKey(Monitoring, on_delete=models.PROTECT)
    condition_choices = (('lt', 'Less than'), ('gt', 'Great than'))
    condition = models.CharField(max_length=2, choices=condition_choices, default='lt')
    value = models.IntegerField()

    def calculate_instance_count(self,application=None):
        if application ==None:
            application = self.application_set.all()[0]
        total_instance_count = application.instances.count()
        healthy_instance_count = application.instances.count()
        metric_value = self.monitoring_engine.get_latest_metric(self.metric,application)
        instance_required = int(metric_value // int(self.value) + 1)
        return (instance_required)

    def save(self, *args, **kwargs):
        # On save, update timestamps
        if not self.id:
            self.date_created = timezone.now()
            self.date_modified = timezone.now()
        return super(SimpleScaler, self).save(*args, **kwargs)

    def __str__(self):
        return str('Simple Scaler_'+str(self.pk))


class PredictiveScaler(AutoScaler):
    metrics = models.ForeignKey(Metric, on_delete=models.PROTECT)
    fwd_time = models.IntegerField(default=60)
    value = models.DecimalField(max_digits=5, decimal_places=2)

    def calculate_instance_count(self):
        time_now = datetime.datetime.now()
        time_lookup = time_now + datetime.timedelta(minutes=self.fwd_time)
        func = lambda p, x: (
                    p[0] + p[1] * x + p[2] * pow(x, 2) + p[3] * pow(x, 3) + p[4] * pow(x, 4) + p[5] * pow(x, 5) + p[
                6] * pow(x, 6) + p[7] * pow(x, 7) + p[8] * pow(x, 8))
        lookup = {1: [7.10698885e+01, 1.74855763e+01, -2.83196302e+01, 5.20095029e+00, 9.27782040e-02, -6.32372033e-02,
                      4.30858686e-03, -1.13296773e-04, 1.02555815e-06]
            , 0: [5.70396099e+01, -6.18233851e+00, -1.37662633e+01, 4.79910927e+00, -5.33064696e-01, 2.53196483e-02,
                  -4.09866398e-04, -5.17200454e-06, 1.73045119e-07]}

        if time_lookup.strftime('%w') == '6' or time_lookup.strftime('%w') == '0':
            predicted_load = func(lookup[0], int(time_lookup.strftime('%H'))) / 5
        else:
            predicted_load = func(lookup[1], int(time_lookup.strftime('%H'))) / 5
        instance_required = int(abs(predicted_load // int(self.value)) + 1)
        return (instance_required)

    def predicted_load(self):
        time_now = datetime.datetime.now()
        time_lookup = time_now + datetime.timedelta(minutes=self.fwd_time)
        func = lambda p, x: (
                    p[0] + p[1] * x + p[2] * pow(x, 2) + p[3] * pow(x, 3) + p[4] * pow(x, 4) + p[5] * pow(x, 5) + p[
                6] * pow(x, 6) + p[7] * pow(x, 7) + p[8] * pow(x, 8))
        lookup = {1: [7.10698885e+01, 1.74855763e+01, -2.83196302e+01, 5.20095029e+00, 9.27782040e-02, -6.32372033e-02,
                      4.30858686e-03, -1.13296773e-04, 1.02555815e-06]
            , 0: [5.70396099e+01, -6.18233851e+00, -1.37662633e+01, 4.79910927e+00, -5.33064696e-01, 2.53196483e-02,
                  -4.09866398e-04, -5.17200454e-06, 1.73045119e-07]}

        if time_lookup.strftime('%w') == '6' or time_lookup.strftime('%w') == '0':
            p_load = func(lookup[0], int(time_lookup.strftime('%H'))) / 5
        else:
            p_load = func(lookup[1], int(time_lookup.strftime('%H'))) / 5
        # instance_required = int(abs(predicted_load//int(self.value)) + 1 )
        return (p_load)

    def save(self, *args, **kwargs):
        # On save, update timestamps
        if not self.id:
            self.date_created = timezone.now()
            self.date_modified = timezone.now()
        return super(PredictiveScaler, self).save(*args, **kwargs)

    def __str__(self):
        return str('Simple Scaler_'+str(self.pk))

    def to_class_name(self):
        return 'General'
