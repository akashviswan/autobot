from djongo import models
from polymorphic.models import PolymorphicModel
from core.models import LoadBalancer,Credential,Monitoring,Scaler,AutoScaler,Metric,MetricStore,Instance
from django.utils import timezone
from core.models import Dashboard
import datetime
from django.db.models.signals import pre_save
from core.signals import *
from  core.reciever import *

class Application(PolymorphicModel):
    name = models.CharField(max_length=50,unique=True)
    loadbalancer = models.ForeignKey(LoadBalancer,on_delete=models.CASCADE,blank=True,null=True)
    credentials = models.ManyToManyField(Credential)
    monitoring = models.ManyToManyField(Monitoring)
    scaler = models.ForeignKey(Scaler,on_delete=models.PROTECT,blank=True,null=True)
    dashboard = models.ForeignKey(Dashboard,on_delete=models.PROTECT,blank=True,null=True)
    auto_scalers = models.ManyToManyField(AutoScaler)
    metrics = models.ManyToManyField(Metric)
    metric_store = models.ForeignKey(MetricStore,on_delete=models.PROTECT,blank=True,null=True)
    instances = models.ManyToManyField(Instance)
    date_created = models.DateTimeField(editable=False)
    date_modified = models.DateTimeField()
    enable_autoscalers = models.BooleanField(default=False)
    autoscaler_taskid = models.CharField(max_length=100,null=True)
    
    def save(self, *args, **kwargs):
        ''' On save, update timestamps '''
        if not self.id:
            self.date_created = timezone.now()
        self.date_modified = timezone.now()
        return super(Application, self).save(*args, **kwargs)
    def __str__(self):              # __unicode__ on Python 2
        return self.name
    def to_class_name(self):
        return 'Generic Application'

    def eval_autoscalers(self,keep_data=True):
        time = datetime.datetime.utcnow()
        data=[ ]
        instance_count = []
        for autoscaler in self.auto_scalers.all():
            count = autoscaler.calculate_instance_count(self)
            instance_count.append(count)
            data.append(dict(Timestamp=time,Metric={
                    'Name': str(autoscaler),
                    'id'  : autoscaler.pk,
                    'Unit': 'count',
                    'Value': count,
                }))
        max_count = max(instance_count)
        data.append(dict(Timestamp=time, Metric={
                'Name': 'Max_Instance_Count',
                'Unit': 'count',
                'Value': max_count,
            }))
        if keep_data:
            for d in data:
                self.metric_store.insert(d)
        return max_count,data

@receiver(pre_save, sender=Application)
def check_for_autoscaling(sender, instance=None, created=False, **kwargs):
    old_list = Application.objects.filter(id=instance.id)
    if old_list.count() > 0:
        old = old_list[0]
        enabled_autoscalers = old.enable_autoscalers
        if old.enable_autoscalers != instance.enable_autoscalers:
            new_application_added.send(sender=sender, application=instance)