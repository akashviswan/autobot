from djongo import models
from polymorphic.models import PolymorphicModel
from django.utils import timezone

class Dashboard(PolymorphicModel):
    history = models.IntegerField(default=180)
    future = models.IntegerField(default=30)
    show_future = models.BooleanField(default=False)
    date_created = models.DateTimeField(editable=False)
    date_modified = models.DateTimeField()

    def save(self, *args, **kwargs):
        # On save, update timestamps
        if not self.id:
            self.date_created = timezone.now()
            self.date_modified = timezone.now()
        return super(Dashboard, self).save(*args, **kwargs)
    
    def render(self):
        return print("Implement render method in child class")

     
