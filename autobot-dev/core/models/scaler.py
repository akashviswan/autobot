########################################################
#Scaler module contains the Scaler super class and all the subclasses, 
#each Scaling engine should extend the main class and overload the methods for auto scaling
########################################################
from django.utils import timezone
from polymorphic.models import PolymorphicModel
from core.models import *
from djongo import models

import logging
logger = logging.getLogger(__name__)

class Scaler(PolymorphicModel):
    cool_off_period=models.IntegerField()
    minInstances=models.IntegerField(default=1)
    maxInstances=models.IntegerField(default=5)
    numInstances=models.IntegerField(default=1)
    date_created = models.DateTimeField(editable=False)
    date_modified = models.DateTimeField()
    
    def save(self, *args, **kwargs):
    #On save, update timestamps
        if not self.id:
            self.date_created = timezone.now()
            self.date_modified = timezone.now()
        return super(Scaler, self).save(*args, **kwargs)

    @staticmethod
    def to_class_name():
        return 'General'
    
