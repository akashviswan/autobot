#!/usr/bin/python
# -*- coding: utf-8 -*-
from polymorphic.models import PolymorphicModel
from djongo import models
from django.utils import timezone


class Instance(PolymorphicModel):

    name = models.CharField(max_length=15, null=True)
    status = models.CharField(max_length=15, null=True)
    date_created = models.DateTimeField(editable=False)
    date_modified = models.DateTimeField()

    def save(self, *args, **kwargs):
        ''' On save, update timestamps '''

        if not self.id:
            self.date_created = timezone.now()
            self.date_modified = timezone.now()
        return super(Instance, self).save(*args, **kwargs)

    def __str__(self):
        return self.name

    


class VirtualMachine(Instance):

    VMId = models.CharField(max_length=20)
    




    
