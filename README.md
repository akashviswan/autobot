## Development

* Install Python dependencies: `pipenv install`
* Install Javascript dependencies: `npm i`
* Make the bundle: `npm run dev`
* Migrate: `pipenv run python ./autobot_dev/manage.py migrate`
* Populate the database: `pipenv run python ./autobot_dev/manage.py db_init
* Run locally: `pipenv run python ./autobot_dev/manage.py runserver`
* Head over http://127.0.0.1:8000/

## Test

* Unit: `cd project && pipenv run python manage.py test`
* E2E: `npm run e2e`
* Coverage: `cd project && pipenv run coverage run manage.py test`

## Production

* Make the bundle: `npm run build`
* ... TODO

## TODO

* Authentication
* React routing
* Production



Setup for Linux, 

1. Configure EPEL repo  : 
	sudo yum install epel-release
2. Install Python 3.6 : 
	sudo yum -y install python36 python36-devel
	python3.6 -m ensurepip
	sudo /usr/local/bin/pip3 install pipenv
3. Install dev tool kits  : 
	sudo yum -y groupinstall development
4. Install MongoDB : 
	yum install https://repo.mongodb.org/yum/redhat/7/mongodb-org/4.0/x86_64/RPMS/mongodb-org-server-4.0.5-1.el7.x86_64.rpm
	service mongod start
5. Install and config Node: 
	curl https://raw.githubusercontent.com/creationix/nvm/v0.13.1/install.sh | bash
	nvm install v10.15.0
	nvm use v10.15.0
6. Git clone : 
	git clone https://akashviswan@bitbucket.org/akashviswan/autobot.git
7. Install python dipendencies : 
	pipenv install

8. Install node packages 
	npm i
	npm run dev
9. Create and populate database for testing
	python manage.py makemigrations core
	python manage.py migrate
	python manage.py db_init
	python manage.py demo_init

Setup For Windows

1. Install Anaconda
2. Create Conda environment with python 3.6
3. Install Git bash
4. Git clone : 
	git clone https://akashviswan@bitbucket.org/akashviswan/autobot_dev.git
5. Install dipendencies : 
	pip install pipenv
	pipenv install

Install Mongo DB without Admin rights on windows machine

1. Download MongoDB MSI package
     https://www.mongodb.com/download-center#community
2. Install with following command
	 msiexec /a mongodb-win32-x64-3.2.5.msi /qb TARGETDIR="C:\MongoDB"
3. Add bineries to PATH
     C:\MongoDB\MongoDB\Server\3.2\bin

