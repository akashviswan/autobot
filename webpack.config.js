var webpack = require("webpack");

module.exports = {
     resolve: {
        extensions: ['.js', '.jsx']
    },
    module: {
    rules: [
      {
        test: /\.js$/,
        exclude: /node_modules/,
        use: {
          loader: "babel-loader",
          query: {
                    presets: ['react', 'es2015', 'stage-3']
                }
        }
      },
       {
        test: /\.(gif|jpe?g|png|ico)$/,
        loader: 'url-loader?limit=10000'
      },
      {
        test: /\.(otf|eot|svg|ttf|woff|woff2).*$/,
        loader: 'url-loader?limit=10000'
      },
      {
        test: /\.jsx$/,
        exclude: /node_modules/,
        use: {
          loader: "babel-loader",
          query: {
                    presets: ['react', 'es2015', 'stage-3']
                }
        }
      },
    {
      test: /\.css$/,
      use: [ 'style-loader', 'css-loader' ]
    }
    ]
  },
  plugins: [
    new webpack.ProvidePlugin({
  $: "jquery",
  jquery: "jquery",
  "window.jQuery": "jquery",
  jQuery:"jquery"
})
  ],
  externals: {
        // global app config object
        config: JSON.stringify({
            staticfiles: 'http://localhost:4000'
        })
    }
};

